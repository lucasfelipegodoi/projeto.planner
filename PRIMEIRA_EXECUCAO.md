Nesse documento está descrita a forma de se utilizar o projeto Skeleton, 
explicando os passos para o download, instalação e execução dos testes.

Visão geral
========

Esse Skeleton foi testado em ambientes com as seguintes características:

  - **Sistema Operacional**: Windows e Linux;
  - **PHP**: 5.4 ou superior;
  - **Provedor da máquina virtual**: Puphpet [[https://puphpet.com]];

  
Dados da máquina virtual
========
  
  - **Virtualizador**: VMWare Workstation ou VirtualBox;
  - **I.P. privado do servidor**: 192.168.57.101 (VMWare) 192.168.56.101 (VirtualBox);
  - **Endereço do servidor**: [[http://local.server.vmware]] ou [[http://local.server.virtualbox]];
  - **Memória**: 512 MB;
  - **CPUs**: 2;
  - **Diretório mapeado**: /vhosts;
  - **Portas mapeadas**: 
		
		- MySQL 3306 -> 6366 (VirtualBox) ou 6369 (VMWare)
		- HTTP 80 -> 6365 (VirtualBox) ou 6368 (VMWare)
		- SSH -> 6364 (VirtualBox) ou 6367 (VMWare)
		
  - **Servidor Web**: Apache ou Nginx
  - **Virtual host**:
  
		- Nome do virtual host: http://projeto.local.dev;
		- Apelido do virtual host: www.projeto.local.dev;
		- Diretório: /var/www/projeto/public -> ./vhosts/projeto/public;
		- 
		
  - **PHP**: 5.6;
  - **MySQL**: 5.6;
  - **PostgreSQL**: 9.3;
  
  - **Gerenciador de banco de dados**: Adminer http://192.168.57.101/adminer;
  
  - **Usuário root**: root (MySQL)
  - **Usuário**: user.admin (MySQL e PostgreSQL)
  - **Senhas**: 123 (todos os bancos)
  
  - **Banco de desenvolvimento**: db.projeto
  - **Banco de teste**: db.projeto.testes
  
  
  
IMPORTANT: Esse Skeleton não foi testado na versão 7 do PHP.


Requisitos de instalação
=========================

Você precisará de um máquina com os seguintes requisitos:

  - **Sistema Operacional**: Windows, Linux ou iOS;
  - **Banco de dados**: SQL Server 2008 ou superior, MySQL 5 ou superior;
  - **PHP**: 5.4 ou superior;
  - **Vagrant**: 1.7.2 ou superior;
  - **Virtual Box**: 4.3.28 ou superior;

NOTE: Você poderá rodar a aplicação na máquina virtual, através do Vagrant,
ou em sua máquina local.

Como instalar
==================



Como rodar a primeira vez
==============================

= Rotinas obrigatórias =


= Rotinas opcionais =

