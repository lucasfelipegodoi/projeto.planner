# PROJETO PLANNER
	
##Instruções para primeira execução

**É necessário ter o PHP.exe na sua variável de ambiente do Windows**

### Código-fonte ZendFramework
####Após o download do código-fonte do projeto Planner, é necessário realizar o download dos códigos-fonte do ZF2. 
1. Realizar o download do Composer.phar, neste [link](https://getcomposer.org/composer.phar).
2. Adicionar o arquivo baixado em vhosts/projeto/.
3. No bash do sistema, executar os seguintes comandos

```
> php composer.phar self-update

> php composer.phar install

```

### Base de dados
1. Executar o arquivo vhosts/projeto/db/base_de_dados.sql em sua base local
2. Alterar o arquivo vhosts/projeto/config/autoload/local.php com as informações de conexão

### Executando o sistema
1. Com o bash do sistema, acessar a pasta vhosts/projeto/httpdocs/.
2. Executar o seguinte comando:
```
> php -s localhost:8081

```
O comando é responsável por subir uma instancia do apache.