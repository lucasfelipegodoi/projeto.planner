<?php

use Admin\Form\AutenticacaoForm;
use Admin\Form\UsuarioForm;
use Admin\Form\AlterarSenhaForm;
use Admin\Form\UsuarioFormFilter;
use Admin\Form\RecuperarSenhaForm;
use Admin\Model\UsuarioModel;
use Admin\Model\NivelDeAcessoModel;
use Admin\Service\UsuarioService;
use Admin\Service\NivelDeAcessoService;
use Admin\Parameter\UsuarioParameter;
use Admin\Parameter\NivelDeAcessoParameter;

return [
    'controllers' => [
        'invokables' => [
            'Admin\Controller\Autenticacao' => 'Admin\Controller\AutenticacaoController',
            'Admin\Controller\Index' => 'Admin\Controller\IndexController',
            'Admin\Controller\Usuario' => 'Admin\Controller\UsuarioController',
        ],
    ],
    'service_manager' => [
        'factories' => [

            // -------------------------- Factories Forms ---------------------------- //

            'Admin\Form\AutenticacaoForm' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new AutenticacaoForm($options);
            },
            'Admin\Form\UsuarioForm' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new UsuarioForm($options);
            },
            'Admin\Form\UsuarioFormFilter' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new UsuarioFormFilter($options);
            },
                    
             'Admin\Form\RecuperarSenhaForm' => function ($serviceManager) {
                 $options = [
                    'serviceManager' => $serviceManager
                 ];

                 return new RecuperarSenhaForm($options);
             },
             'Admin\Form\AlterarSenhaForm' => function ($serviceManager) {
                 $options = [
                    'serviceManager' => $serviceManager
                 ];

                 return new AlterarSenhaForm($options);
             },
        
            // -------------------------- Factories Models ---------------------------- //

            'Admin\Model\UsuarioModel' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager,
                ];
                
                return new UsuarioModel($options);
            },
            'Admin\Model\NivelDeAcessoModel' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager,
                ];
                
                return new NivelDeAcessoModel($options);
            },
            // -------------------------- Factories Parameters  ---------------------------- //
            'Admin\Parameter\UsuarioParameter'  => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager,
                ];

                return new UsuarioParameter($options);
            },
            'Admin\Parameter\NivelDeAcessoParameter'  => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager,
                ];

                return new NivelDeAcessoParameter($options);
            },
            
            // -------------------------- Factories Services ---------------------------- //

            'Admin\Service\UsuarioService'  => function ($serviceManager) {
                $options = [];

                return new UsuarioService($options);
            },
            'Admin\Service\NivelDeAcessoService'  => function ($serviceManager) {
                $options = [];

                return new NivelDeAcessoService($options);
            },


                    
        ],
    ],
    'router' => [
        'routes' => [
            'home' => [
                'type' => 'Segment',
                'options' => [
                    'route'    => '/[page/:page]',
                    'defaults' => [
                        'controller' => 'Admin\Controller\Autenticacao',
                        'action'     => 'index',
                        'module'     => 'admin',
                    ],
                ],
            ],
            'admin' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/admin',
                    'defaults' => [
                        'controller'    => 'Autenticacao',
                        'action'        => 'index',
                        '__NAMESPACE__' => 'Admin\Controller',
                        'module'     => 'admin'
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults' => [
                            ],
                        ],
                        'child_routes' => [ //permite mandar dados pela url
                            'wildcard' => [
                                'type' => 'Wildcard'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'display_exceptions'       => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'partials/paginator/control' => __DIR__ . '/../../Core/view/partials/paginator/control.phtml',
            'layout/layout'              => __DIR__ . '/../../Core/view/layout/layout.phtml',
            'layout/layout_email'        => __DIR__ . '/../../Core/view/layout/layout_email.phtml',
            'error/404'                  => __DIR__ . '/../../Core/view/error/404.phtml',
            'error/index'                => __DIR__ . '/../../Core/view/error/index.phtml',
            'layout/layout_autenticacao' => __DIR__ . '/../view/layout/layout_autenticacao.phtml',
        ],
        'template_path_stack' => [
            'admin' => __DIR__ . '/../view',
        ],
    ],
];
