<?php

namespace Admin;

use Core\Service\AutenticacaoService;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

/**
 * Configurações do módulo
 *
 * @name       Module
 * @category   Application
 * @package    Admin
 * @subpackage AdminModule
 * @author     Author <e-mail>
 */
class Module
{
    public function init($m)
    {
        $event = $m->getEventManager()->getSharedManager();
        $event->attach('Zend\Mvc\Application', 'bootstrap', function ($e) {
            //'executed on bootstrap app process <br />';
        });
    }
    
    /**
     * Configurações de boot do módulo
     *
     * @name         onBootstrap
     *
     * @param  array $event
     *
     * @access public
     * @return void
     */
    public function onBootstrap($event)
    {
        $event->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $moduleManager = $event->getApplication()
            ->getServiceManager()
            ->get('modulemanager');

        $sharedEvents = $moduleManager->getEventManager()
            ->getSharedManager();
        
        $sharedEvents->attach(
            'Zend\Mvc\Controller\AbstractActionController',
            MvcEvent::EVENT_DISPATCH,
            array(
                $this,
                'mvcPreDispatch'
            ),
            100
        );
    }

    /**
     * Ação executada antes do dispatch da view
     *
     * @name         mvcPreDispatch
     *
     * @param  array $event
     *
     * @access public
     * @return boolean
     */
    public function mvcPreDispatch($event)
    {
        $serviceLocator = $event->getTarget()
            ->getServiceLocator();

        $redirect = $event->getTarget()
            ->redirect();

        $statusReturn = true;
        $autenticacaoService = $serviceLocator->get(AutenticacaoService::class);
        $routeMatch          = $event->getRouteMatch();
        $rota                = array(
            'module'     => $routeMatch->getParam('module'),
            'controller' => $routeMatch->getParam('controller'),
            'action'     => $routeMatch->getParam('action'),
        );

        if ($rota['controller'] == 'Admin\Controller\Autenticacao') {
            return true;
        }
        
        if ($autenticacaoService->hasIdentity() === false) {
            $redirect->toUrl('/admin/autenticacao/index');
            return true;
        }

        return $statusReturn;
    }

    /**
     * Retorna o array de configurações do módulo
     *
     * @name   getConfig
     * @access public
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Retorna o autoloader config do módulo
     *
     * @name   getAutoloaderConfig
     * @access public
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
