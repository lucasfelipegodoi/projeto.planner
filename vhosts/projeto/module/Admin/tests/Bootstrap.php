<?php

use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;

/**
 * Boostrap dos testes do módulo
 *
 * @name       Bootstrap
 * @category   Test
 * @package    Admin
 * @subpackage AdminBootstrap
 * @author
 */
class Bootstrap
{

    /**
     * Retorna o path do módulo
     *
     * @name   getModulePath
     * @access public static
     * @return string
     */
    public static function getModulePath()
    {
        return __DIR__ . '/../../../module/Admin';
    }

    /**
     * Executa o boostrap
     *
     * @name   go
     * @access public static
     * @return void
     */
    public static function go()
    {
        //@todo verificar se não existe uma constante indicando o diretório raiz do projeto
        chdir(dirname(__DIR__ . '/../../../..'));

        include 'init_autoloader.php';

        define('ZF2_PATH', realpath('vendor/zendframework'));

        $path = array(ZF2_PATH, get_include_path());
        set_include_path(implode(PATH_SEPARATOR, $path));

        // setup autoloader
        AutoloaderFactory::factory(array(
            'Zend\Loader\StandardAutoloader' => array(
                StandardAutoloader::AUTOREGISTER_ZF => true,
                StandardAutoloader::ACT_AS_FALLBACK => false,
                StandardAutoloader::LOAD_NS => array(
                    'Core' => getcwd() . '/module/Core/src/Core'
                ),
            ),
        ));
    }
}

Bootstrap::go();
