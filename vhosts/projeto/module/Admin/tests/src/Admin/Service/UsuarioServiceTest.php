<?php
/**
 * Service
 */
namespace Admin\Service;

use Core\Test\ServiceTestCase;
use Admin\DataSourceTest;
use Admin\Model\UsuarioModel;
use Zend\Paginator\Paginator;

/**
 * Testes do service
 *
 * @group   Service
 *
 * @name    UsuarioServiceTest
 * @package Admin\Service
 * @author  Author <e-mail>
 */
class UsuarioServiceTest extends ServiceTestCase
{

    /**
     * Namespace completo do Model
     *
     * @var string
     */
    //protected $modelFQDN = UsuarioModel::class;

    /**
     * @var
     */
    protected $usuarioService;

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->usuarioService = $this->getService(UsuarioService::class);
        $this->executeDependentRoutines();
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Teste para verficar se todos os itens são retornados
     *
     * @name   testGetAll
     * @access public
     * @return void
     */
    public function testGetAll()
    {
        $totalItens = $this->executeSave();
        $this->assertEquals($totalItens, count($this->usuarioService->getAll()));
    }

    /**
     * Teste para verficar a inclusão/alteração de um item inválido
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testSaveInvalid
     * @access public
     * @return void
     */
    public function testSaveInvalid()
    {
        $result = $this->usuarioService->save([
            'dataSource' => DataSourceTest::getInvalidData(['model' => UsuarioModel::class])[0],
        ]);

        $this->assertFalse($result);
    }

    /**
     * Teste para verficar a exclusão de um item
     *
     * @name   testDeleteById
     * @access public
     * @return void
     */
    public function testDeleteById()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $result = $this->usuarioService->deleteById(1);

        $this->assertTrue($result);
    }

    /**
     * /**
     * Teste para verficar o retorno de um item
     *
     * @name   testGetItemById
     * @access public
     * @return void
     */
    public function testGetItemById()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $item = $this->usuarioService->getItemById(1);

        $this->assertEquals(1, $item->getPrimaryKeyValue());
    }

    /**
     * Teste para verficar a paginação dos dados
     *
     * @name   testPaginate
     * @access public
     * @return void
     */
    public function testPaginate()
    {
        $this->executeSave();
        $paginator = $this->usuarioService->paginate();

        $this->assertInstanceOf(Paginator::class, $paginator);
    }

    /**
     * Teste para verficar o método [chave => valor]
     *
     * @name
     * @access public
     * @return void
     */
    public function testFetchPairs()
    {
        $totalItens = $this->executeSave();
        $pairs      = $this->usuarioService->fetchPairs();

        $this->assertEquals($totalItens, count($pairs));
    }
    
    /**
     *
     */
    public function testEnviarNovaSenhaDeAcesso()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $identificacaoPorEmail = [
            'login_ou_email' => 'usuario@teste.com',
        ];
        $identificacaoPorLogin = [
            'login_ou_email' => 'usuario',
        ];
        
        $this->assertTrue($this->usuarioService->enviarNovaSenhaDeAcesso($identificacaoPorEmail));
        $this->assertTrue($this->usuarioService->enviarNovaSenhaDeAcesso($identificacaoPorLogin));
    }

    /**
     *
     * @expectedException Core\Exception\MandatoryConditionException
     */
    public function testExcecaoAoEnviarNovaSenhaDeAcessoSemInformarLoginOuEmail()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $identificacao = [
            'login_ou_email' => null,
        ];
        
        $this->usuarioService->enviarNovaSenhaDeAcesso($identificacao);
    }

    /**
     *
     * @expectedException Core\Exception\NotFoundException
     */
    public function testExcecaoAoEnviarNovaSenhaDeAcessoComLoginOuEmailInexistente()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $identificacao = [
            'login_ou_email' => 'usuario_inexistente',
        ];
        
        $this->usuarioService->enviarNovaSenhaDeAcesso($identificacao);
    }
    
    /**
     *
     * @expectedException Core\Exception\NotFoundException
     */
    public function testExcecaoAoPassarIdInexistenteAoGerarESalvarNovaSenhaDeAcesso()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $pkIdUsuario  = 178743123;
        
        $this->usuarioService->gerarESalvarNovaSenhaDeAcesso($pkIdUsuario);
    }
        
    /**
     *
     * @name   executeSave
     * @access private
     * @return int
     */
    private function executeSave($numberOfItems = null)
    {
        $itens      = DataSourceTest::getValidData(['model' => UsuarioModel::class]);
        $savedItens = $numberOfItems === null ? count($itens) : $numberOfItems;

        foreach ($itens as $item) {
            if ($savedItens == 0) {
                break;
            }
            
            $this->usuarioService->save([
                'dataSource' => $item,
            ]);
            
            $savedItens--;
        }

        return count($itens);
    }
    
    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        $this->migrationService->runSeed('NivelDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
