<?php

namespace Admin\Controller;

use Core\Test\ControllerTestCase;
use Admin\Controller\AutenticacaoController;
use Core\Service\AutenticacaoService;

/**
 * Testes do controller
 *
 * @group      Controller
 *
 * @name       IndexControllerTest
 * @category   Test
 * @package    Skel
 * @subpackage SkelController
 * @author     Author <e-mail>
 */
class AutenticacaoControllerTest extends ControllerTestCase
{

    /**
     * Namespace completa do Controller
     * @var string
     */
    protected $controllerFQDN = AutenticacaoController::class;

    /**
     * Nome da rota padrão definida no arquivo module.config
     * @var string
     */
    protected $controllerRoute = 'admin';

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->executeDependentRoutines();
    }

    /**
     *
     * @return type
     */
    public function testAutenticar()
    {
        $this->setRouteMatch([
            'method' => 'post',
            'params' => [
                'action' => 'autenticar',
            ],
        ]);

        $post = $this->request->getPost();
        $post->set('login', 'admin');
        $post->set('senha_de_acesso', '3Tyu12');

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/index/index', $result['headers']->get('Location'));
    }

    public function testAutenticarInvalidoSeNaoForViaPost()
    {
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'autenticar',
            ],
        ]);

        $post = $this->request->getPost();
        $post->set('login', 'admin');
        $post->set('senha_de_acesso', '3Tyu12');

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/autenticacao/index', $result['headers']->get('Location'));
    }

    /**
     *
     * @return type
     */
    public function testEnviarNovaSenhaDeAcesso()
    {
        $this->setRouteMatch([
            'method' => 'post',
            'params' => [
                'action' => 'enviar-nova-senha-de-acesso',
            ],
        ]);

        $post = $this->request->getPost();
        $post->set('login_ou_email', 'admin');

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/autenticacao/index', $result['headers']->get('Location'));
    }

    /**
     *
     * expectedException Core\Exception\MandatoryConditionException
     *
     */
    public function testEnviarNovaSenhaDeAcessoComUsuarioInvalido()
    {
        $this->setRouteMatch([
            'method' => 'post',
            'params' => [
                'action' => 'enviar-nova-senha-de-acesso',
            ],
        ]);

        $post = $this->request->getPost();
        $post->set('login_ou_email', 'usuario_inexistente');

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/autenticacao/enviar-nova-senha-de-acesso', $result['headers']->get('Location'));
    }

   
    /**
     * Teste da action de gerenciamento
     *
     * @todo Verificar porque este teste só passa se autenticarmos o usuário. Essa é uma rota pública.
     *
     * @name   testIndexAction
     * @access public
     * @return void
     */
    public function testIndex()
    {
        $this->autenticarUsuario();
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'index',
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/index/index', $result['headers']->get('Location'));
    }

    public function testSairDoSistema()
    {
        $this->autenticarUsuario();
        $this->setRouteMatch([
            'method' => 'post',
            'params' => [
                'action' => 'sair-do-sistema',
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/autenticacao/index', $result['headers']->get('Location'));
    }

    public function testSairDoSistemaDuasVezes()
    {
        $this->autenticarUsuarioComLogout();
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'sair-do-sistema',
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/index/index', $result['headers']->get('Location'));
    }

    /**
     *
     * @todo Corrigir o problema que ocorre ao fazer a seguinte validação:
     * 'Location: /admin/autenticacao/forbidden' depois de executar a função $this->getResponse()
     * mais de uma vez no mesmo teste.
     *
     */
    public function testForbiddenAcessoNegado()
    {
        $this->autenticarUsuario();
        $this->setRouteMatch([
            'method' => 'get',
            'recreate_route_match' => true,
            'params' => [
                //'module' => '',//Usar um module que deverá ser verificado
                //'controller' => ,//Usar um controller que deverá ser verificado
                'action' => 'index',
                'check_if_has_identity' => true,
                'check_is_authorized' => true,
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /admin/autenticacao/forbidden', $result['headers']->get('Location'));

        $this->setRouteMatch([
            'method' => 'get',
            'recreate_route_match' => true,
            'params' => [
                'action' => 'forbidden',
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(200, $result['response']->getStatusCode());
    }
    
    /**
     * @todo Verificar porque este teste se comporta de forma diferente ao rodarmos ele sozinho e no grupo
     * @todo A assertiva 'Location: /admin/autenticacao/index' não está funcionando
     *
     */
    public function testForbiddenComUsuarioNãoAutenticado()
    {
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'forbidden',
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(200, $result['response']->getStatusCode());
    }

    private function autenticarUsuario()
    {
        $autenticacaoService = $this->getService(AutenticacaoService::class);
        $autenticacaoService->authenticate([
            'login' => 'admin',
            'senha_de_acesso' => '3Tyu12'
        ]);

        return $autenticacaoService;
    }

    private function autenticarUsuarioComLogout()
    {
        $this->autenticarUsuario()->logout();
    }

    private function executeDependentRoutines()
    {
        $this->migrationService->runSeed('UsuarioSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
