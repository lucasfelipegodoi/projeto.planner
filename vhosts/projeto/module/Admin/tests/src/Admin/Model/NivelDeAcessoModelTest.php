<?php
/**
* Model
*/
namespace Admin\Model;

use Core\Test\ModelTestCase;
use Admin\DataSourceTest;
use Admin\Model\NivelDeAcessoModel;
use Zend\InputFilter\InputFilterInterface;

/**
* Testes do model
*
* @group   Model
*
* @name    NivelDeAcessoModelTest
* @package Admin\Model
* @author  Author <e-mail>
*/
class NivelDeAcessoModelTest extends ModelTestCase
{
    
    /**
    * Namespace completo do Model
    *
    * @var string
    */
    protected $modelFQDN = NivelDeAcessoModel::class;
    
    /**
    * Rotinas que serão executadas antes do início do teste
    *
    * @name   setup
    * @access public
    * @return void
    */
    public function setup()
    {
        parent::setup();
    }
    
    /**
    * Rotinas que serão executadas ao final do teste
    *
    * @name   tearDown
    * @access public
    * @return void
    */
    public function tearDown()
    {
        parent::tearDown();
    }
    
    
    /**
    * Retorna os inputs filters do model
    *
    * @name   testGetInputFilter
    * @access public
    * @return void
    */
    public function testGetInputFilter()
    {
        $nivelDeAcessoModel = $this->getModel(NivelDeAcessoModel::class);
        $inputFilter  = $nivelDeAcessoModel->getInputFilter();
        
        $this->assertInstanceOf(InputFilterInterface::class, $inputFilter);
    }
    
    /**
    * Testa se os input filters são válidos
    *
    * @name   testValidInputFilter
    * @access  public
    * @return void
    */
    public function testValidInputFilter()
    {
        $nivelDeAcessoModel = $this->getModel(NivelDeAcessoModel::class);
        $inputFilter  = $nivelDeAcessoModel->getInputFilter();
        
        $this->assertTrue($inputFilter->has($nivelDeAcessoModel->getPrimaryKeyField()));
        $this->assertTrue($inputFilter->has('nivel_de_acesso'));
    }
    
    /**
    * Verifica se um input filter inválido lança uma exceção.
    *
    * @expectedException Core\Model\EntityException
    *
    * @name   testInvalidInputFilter
    * @access public
    * @return void
    */
    public function testInvalidInputFilter()
    {
        $nivelDeAcessoModel = $this->getModel(NivelDeAcessoModel::class);
        $nivelDeAcessoModel->setData(DataSourceTest::getInvalidData(['model' => NivelDeAcessoModel::class])[0]);
    }
}
