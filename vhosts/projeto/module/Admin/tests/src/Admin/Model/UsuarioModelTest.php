<?php
/**
 * Model
 */
namespace Admin\Model;

use Core\Test\ModelTestCase;
use Admin\DataSourceTest;
use Zend\InputFilter\InputFilterInterface;

/**
 * Testes do model
 *
 * @group   Model
 *
 * @name    UsuarioModelTest
 * @package Admin\Model
 * @author  Author <e-mail>
 */
class UsuarioModelTest extends ModelTestCase
{

    /**
     * Namespace completo do Model
     *
     * @var string
     */
    protected $modelFQDN = UsuarioModel::class;

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        parent::setup();
        $this->executeDependentRoutines();
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }


    /**
     * Retorna os inputs filters do model
     *
     * @name   testGetInputFilter
     * @access public
     * @return void
     */
    public function testGetInputFilter()
    {
        $usuarioModel = $this->getModel(UsuarioModel::class);
        $inputFilter  = $usuarioModel->getInputFilter();

        $this->assertInstanceOf(InputFilterInterface::class, $inputFilter);
    }

    /**
     * Testa se os input filters são válidos
     *
     * @name   testValidInputFilter
     * @access  public
     * @return void
     */
    public function testValidInputFilter()
    {
        $usuarioModel = $this->getModel(UsuarioModel::class);
        $inputFilter  = $usuarioModel->getInputFilter();
        
        $this->assertTrue($inputFilter->has($usuarioModel->getPrimaryKeyField()));
        $this->assertTrue($inputFilter->has('nome'));
        $this->assertTrue($inputFilter->has('fk_id_nivel_de_acesso'));
        $this->assertTrue($inputFilter->has('nome'));
        $this->assertTrue($inputFilter->has('login'));
        $this->assertTrue($inputFilter->has('email'));
        $this->assertTrue($inputFilter->has('senha_de_acesso'));
    }

    /**
     * Verifica se um input filter inválido lança uma exceção.
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testInvalidInputFilter
     * @access public
     * @return void
     */
    public function testInvalidInputFilter()
    {
        $usuarioModel = $this->getModel(UsuarioModel::class);
        $usuarioModel->setData(DataSourceTest::getInvalidData(['model' => UsuarioModel::class])[0]);
    }

    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        if ($this->mockShouldBeUsedForModel) {
            return 1;
        }
                
        $this->migrationService->runSeed('NivelDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
