<?php
namespace Admin;

use Admin\Model\UsuarioModel;
use Admin\Model\NivelDeAcessoModel;

/**
 * Classe provedora dos dados de testes
 *
 * @name     DataSourceTest
 * @category Test
 * @package  Admin
 * @author   Author <e-mail>
 */
class DataSourceTest
{
    /**
     * Exemplo de utilização do método de retorno dos dados válidos para teste
     *
     * @name   getValidData
     * @access public static
     * @return array
     */
    public static function getValidData(array $options)
    {
        $data = [
            UsuarioModel::class => [
                [
                    'pk_id_usuario'         => null,
                    'fk_id_nivel_de_acesso' => 1,
                    'nome'                  => 'Usuário 1',
                    'esta_ativo'            => '1',
                    'login'                 => 'usuario',
                    'email'                 => 'usuario@teste.com',
                    'senha_de_acesso'       => 'advbf',
                ],
                [
                    'pk_id_usuario'         => null,
                    'fk_id_nivel_de_acesso' => 1,
                    'nome'                  => 'Usuário 2',
                    'esta_ativo'            => '1',
                    'login'                 => 'novo',
                    'email'                 => 'outro@teste.com',
                    'senha_de_acesso'       => null,
                ],
            ],
            ////////////////////////////////////////////////////////////////////
            NivelDeAcessoModel::class => [
                [
                    'pk_id_nivel_de_acesso' => null,
                    'nivel_de_acesso' => 'Admin',
                ],
                [
                    'pk_id_nivel_de_acesso' => null,
                    'nivel_de_acesso' => 'Funcionario',
                ],
            ],
            ////////////////////////////////////////////////////////////////////

        ];
        
        return $data[$options['model']];
    }

    /**
     * Exemplo de utilização do método de retorno dos dados inválidos para teste
     *
     * @name   getInvalidData
     * @access public static
     * @return array
     */
    public static function getInvalidData(array $options)
    {
        $data = [
            UsuarioModel::class => [
                [
                    'pk_id_usuario' => null,
                    'fk_id_nivel_de_acesso' => null,
                    'nome'     => null,
                    'esta_ativo'=> '1',
                    'login'    => ' fdf - c',
                    'email'    => 'usuario',
                    'senha_de_acesso' => null,
                ],
                [
                    'pk_id_usuario' => -1,
                    'fk_id_nivel_de_acesso' => null,
                    'nome'     => null,
                    'esta_ativo'=> '1',
                    'login'    => ' 4 y % g',
                    'email'    => 'usuario',
                    'senha_de_acesso' => null,
                ],
            ],
            ////////////////////////////////////////////////////////////////////
            NivelDeAcessoModel::class => [
                [
                    'pk_id_nivel_de_acesso' => null,
                    'nivel_de_acesso' => null,
                ],
                [
                    'pk_id_nivel_de_acesso' => null,
                    'nivel_de_acesso' => null,
                ],
            ],
        ];
        
        return $data[$options['model']];
    }
}
