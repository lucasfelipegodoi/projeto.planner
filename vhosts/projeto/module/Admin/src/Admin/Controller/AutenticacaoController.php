<?php

namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Admin\Form\AutenticacaoForm;
use Admin\Form\RecuperarSenhaForm;
use Admin\Service\UsuarioService;
use Core\Controller\ActionController;
use Core\Service\AutenticacaoService;
use User\Service\UserService;
use User\Form\UserForm;
use \Exception;

/**
 *
 * @name       AutenticacaoController
 * @category   Application
 * @package    Admin
 * @subpackage AdminController
 * @author     Author <e-mail>
 */
class AutenticacaoController extends ActionController
{
    /**
     *
     * @name   forbiddenAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function forbiddenAction()
    {
        if ($this->getService(AutenticacaoService::class)->hasIdentity() == false) {
            $this->redirect()->toUrl('/admin/autenticacao/index');
        }
        
        $viewModel = $this->getService(ViewModel::class);

        return $viewModel;
    }

    /**
     *
     * @name   indexAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        if ($this->getService(AutenticacaoService::class)->hasIdentity()) {
            $this->redirect()->toUrl('/admin/index/index');
        }

        $this->layout()->setTemplate('layout/layout_autenticacao');

        $viewModel = $this->getService(ViewModel::class);
        $viewModel->setVariables(array(
            'form' => $this->getService(AutenticacaoForm::class),
        ));

        return $viewModel;
    }

    /**
     *
     * @name   autenticarAction
     * @access public
     * @return void
     */
    public function autenticarAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->redirect()->toUrl('/admin/autenticacao/index');
        }

        try {
            $dadosDoPost = $this->getRequest()->getPost()->toArray();

            $this->getService(AutenticacaoService::class)->authenticate($dadosDoPost);
            
            $this->addSuccessMessage('Você foi autenticado com sucesso');
            $this->redirect()->toUrl('/plan/default/index');
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/admin/autenticacao/index');
        }
    }

    public function saveAction()
    {
        $this->layout()->setTemplate('layout/layout_autenticacao');

        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(UserService::class);
        $form = $this->getService(UserForm::class);
        $id = $this->params()->fromRoute('id', 0);

        $viewModel->setVariables([
            'form' => $form,
        ]);

        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {
                    $this->addSuccessMessage('Usuário cadastrado com sucesso! Insira abaixo as senhas cadastradas para começar a utilizar o sistema !');
                    $this->redirect()->toUrl('/admin/autenticacao/index');
                }
            }
        } catch (EntityException $entityException) {
            $this->addErrorMessage('Item inválido');
            $this->redirect()->toUrl('/admin/autenticacao/index');
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/admin/autenticacao/index');
        }

        return $viewModel;
    }


    /**
     *
     * @return type
     */
    public function enviarNovaSenhaDeAcessoAction()
    {
        $this->layout()->setTemplate('layout/layout_autenticacao');

        $usuarioService = $this->getService(UserService::class);
        $viewModel = $this->getService(ViewModel::class);
        $form = $this->getService(RecuperarSenhaForm::class);

        $viewModel->setVariables(array(
            'form' => $form,
        ));

        if ($this->getRequest()->isPost()) {
            try {
                $form->setData($this->getRequest()->getPost());

                if ($usuarioService->enviarNovaSenhaDeAcesso($form)) {
                    $this->addSuccessMessage('Ação realizada com sucesso ! <br>A nova senha foi enviado para o seu email. ');
                    $this->redirect()->toUrl('/admin/autenticacao/index');
                }
            } catch (Exception $exception) {
                $this->addExceptionMessage($exception);
                $this->redirect()->toUrl('/admin/autenticacao/enviar-nova-senha-de-acesso');
            }
        }

        return $viewModel;
    }

    /**
     *
     *
     * @name   sairDoSistemaAction
     * @access public
     * @return void
     */
    public function sairDoSistemaAction()
    {
        if ($this->getService(AutenticacaoService::class)->logout()) {
            $this->addSuccessMessage('Sua saída foi realizada com sucesso');
            $this->redirect()->toUrl('/admin/autenticacao/index');
        } else {
            $this->addErrorMessage('Houve um erro ao executar a saída');
            $this->redirect()->toUrl('/admin/index/index');
        }
    }
}
