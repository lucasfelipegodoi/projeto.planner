<?php

/**
 * Controller
 */

namespace Admin\Controller;

use \Exception;
use Core\Controller\ActionController;
use Admin\Form\UsuarioForm;
use Admin\Form\UsuarioFormFilter;
use Admin\Service\UsuarioService;
use Zend\View\Model\ViewModel;
use Admin\Form\AlterarSenhaForm;

/**
 * Controller Usuario
 *
 * @name    UsuarioController
 * @package Admin\Controller
 * @author  Author <e-mail>
 */
class UsuarioController extends ActionController
{
    /**
     * Método para excluir um item
     *
     * @name   deleteAction
     * @access public
     * @return void
     */
    public function deleteAction()
    {
        try {
            $usuarioService = $this->getService(UsuarioService::class);
            $id = $this->params()->fromRoute('id', 0);

            if ($usuarioService->deleteById($id)) {
                $this->addSuccessMessage('Exclusão realizada com sucesso!');
            } else {
                $this->addErrorMessage('Não foi possível executar a exclusão!');
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }
        
        $this->redirect()->toUrl('/admin/usuario/index');
    }

    /**
     * Método para gerenciar os itens
     *
     * @name   indexAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $formFilter = $this->getService(UsuarioFormFilter::class);
        $usuarioService = $this->getService(UsuarioService::class);
        $params = $this->getParamsFromRoute();

        $viewModel->setVariables([
            'formFilter' => $formFilter->bind($params['parameters']),
            'paginator' => $usuarioService->paginate($params),
        ]);

        return $viewModel;
    }

    /**
     * Método para salvar um item
     *
     * @name   saveAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function saveAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $usuarioService = $this->getService(UsuarioService::class);
        $form = $this->getService(UsuarioForm::class);
        $id = $this->params()->fromRoute('id', 0);

        $viewModel->setVariables([
            'form' => $form,
        ]);
        
        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($usuarioService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($usuarioService->save($form)) {
                    $this->addSuccessMessage('Os dados do usuário foram salvos com sucesso!');
                    $this->redirect()->toUrl('/admin/usuario/index');
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/admin/usuario/save');
        }

        return $viewModel;
    }

    /**
     * Método para visualizar um item
     *
     * @name   viewAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $usuarioService = $this->getService(UsuarioService::class);
        $id = $this->params()->fromRoute('id', 0);
       

        try {
            $viewModel->setVariables([
                'item' => $usuarioService->getAll(['pk_id_usuario' => $id]),
                
            ]);
           
            $viewModel->setTerminal(true);

            return $viewModel;
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/admin/usuario/index');
        }
    }
    
    public function alterarSenhaAction()
    {
        try {
            $usuarioService = $this->getService(UsuarioService::class);
            $form = $this->getService(UsuarioForm::class);
            $form->setData($this->getRequest()->getPost());

            if ($usuarioService->deleteById()) {
                $this->addSuccessMessage('Exclusão realizada com sucesso!');
            } else {
                $this->addErrorMessage('Não foi possível executar a exclusão!');
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }
        
        $this->redirect()->toUrl('/admin/usuario/index');
    }
    public function alterarSenhaDeAcessoAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $usuarioService = $this->getService(UsuarioService::class);
        $form = $this->getService(AlterarSenhaForm::class);

        $viewModel->setVariables([
            'form' => $form,
        ]);
        
        try {
            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($usuarioService->alterarNovaSenhaDeAcesso($form)) {
                    $this->addSuccessMessage('Sua senha foi alterada com sucesso!</br>'
                                            . 'Um email foi enviado a você com sua nova senha!');
                    $this->redirect()->toUrl('/admin/usuario/index');
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/admin/usuario/alterar-senha-de-acesso');
        }
        return $viewModel;
    }
}
