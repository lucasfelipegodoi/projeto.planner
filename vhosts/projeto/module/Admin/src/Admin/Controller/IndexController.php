<?php
namespace Admin\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\ActionController;

/**
 *
 * @name       IndexController
 * @category   Application
 * @package    Admin
 * @subpackage AdminController
 * @author     Author <e-mail>
 */
class IndexController extends ActionController
{
    /**
     *
     * @name   indexAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        
        return $viewModel;
    }
}
