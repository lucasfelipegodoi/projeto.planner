<?php
/**
 * Service
 */
namespace Admin\Parameter;

use Core\Stdlib\Parameters;
use Admin\Model\UsuarioModel;

/**
 *
 */
class UsuarioParameter extends Parameters
{
    /**
     * Parâmetros de busca do service
     */
    protected $parameters = [
        'pk_id_usuario' => [
            'type' => self::TYPE_INT,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_EQUAL,
        ],
        'nome' => [
            'type' => self::TYPE_STRING,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_LIKE,
        ],
        'email' => [
            'type' => self::TYPE_STRING,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_LIKE,
        ],
        'login' => [
            'type' => self::TYPE_STRING,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_EQUAL,
        ],
        'fk_id_nivel_de_acesso' => [
            'type' => self::TYPE_INT,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_IN,
        ],
        'esta_ativo' => [
            'type' => self::TYPE_BOOLEAN,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_EQUAL,
        ],
        'senha_de_acesso' => [
            'type' => self::TYPE_STRING,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_EQUAL,
        ],
    ];

    
    public function __construct(array $values = null)
    {
        parent::__construct($values);
    }

    public static function getModelName()
    {
        return UsuarioModel::class;
    }
}
