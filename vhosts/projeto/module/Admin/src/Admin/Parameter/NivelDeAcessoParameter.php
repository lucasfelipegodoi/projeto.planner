<?php
/**
 * Service
 */
namespace Admin\Parameter;

use Core\Stdlib\Parameters;
use Admin\Model\NivelDeAcessoModel;

class NivelDeAcessoParameter extends Parameters
{
    /**
     * Parâmetros de busca do service
     */
    protected $parameters = [
        'pk_id_nivel_de_acesso' => [
            'type' => self::TYPE_INT,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_IN,
        ],
        'nivel_de_acesso' => [
            'type' => self::TYPE_STRING,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_LIKE,
        ],
    ];

    public function __construct(array $values = null)
    {
        parent::__construct($values);
    }


    public static function getModelName()
    {
        return NivelDeAcessoModel::class;
    }
}
