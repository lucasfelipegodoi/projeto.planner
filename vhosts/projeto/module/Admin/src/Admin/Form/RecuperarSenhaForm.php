<?php
namespace Admin\Form;

use Core\Form\BaseForm;

/**
 *
 * @name       AutenticacaoForm
 * @category   Application
 * @package    Admin
 * @subpackage AdminForm
 * @author     Author <e-mail>
 */
class RecuperarSenhaForm extends BaseForm
{

    /**
     * @name   __construct
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('form', '/admin/autenticacao/enviar-nova-senha-de-acesso');

        $this->addText([
            'name'        => 'login_ou_email',
            'label'       => 'Digite seu email ou login de acesso',
            'placeholder' => 'Digite seu email ou login de acesso',
            'class' => 'form-control',
        ]);

        $this->addSubmit([
            'value'=> 'Enviar nova senha',
            'class'   => 'btn btn-primary',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Enviar',
        ]);
        
        $this->addButton([
            'name'    => 'voltar',
            'label'   => 'Voltar',
            'id'=> '',
            'class'   => 'btn btn-warning',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "goTo('/admin/usuario/index')",
        ]);
    }

    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
