<?php

namespace Admin\Form;

use Core\Form\BaseForm;

/**
 *
 * @name       AutenticacaoForm
 * @category   Application
 * @package    Admin
 * @subpackage AdminForm
 * @author     Author <e-mail>
 */
class AutenticacaoForm extends BaseForm
{

    /**
     * @name   __construct
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('action', '/admin/autenticacao/autenticar');

        $this->addText([
            'name'        => 'email',
            'label'       => 'Emil',
            'placeholder' => 'Email',
            'class' => 'form-control',
        ]);

        $this->addPassword([
            'name'        => 'senha',
            'label'       => 'Senha de acesso',
            'placeholder' => 'Senha de acesso',
            'class' => 'form-control',

        ]);

        self::add([
            'name'       => 'submit',
            'attributes' => [
                'type'  => 'submit',
                'value' => 'Acessar',
                'id'    => 'submit',
                'class' => 'btn btn-primary',
               
            ],
        ]);
    }

    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
