<?php
/**
 * Form
 */
namespace Admin\Form;

use Core\Form\BaseForm;

/**
 * Formulário para incluir um item
 *
 * @name    AlterarSenhaForm
 * @package Admin\Form
 * @author  Author <e-mail>
 */
class AlterarSenhaForm extends BaseForm
{
    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('form', '/admin/autenticacao/alterarsenha');
        
        $this->addPassword([
            'name'        => 'senha_de_acesso_atual',
            'label'       => 'Senha atual',
            'placeholder' => 'Digite sua senha atual',
            'class' => 'form-control',
        ]);
        
        $this->addPassword([
            'name'        => 'nova_senha_de_acesso',
            'label'       => 'Nova senha',
            'placeholder' => 'Digite sua nova senha',
            'class' => 'form-control',
        ]);
        
        $this->addPassword([
            'name'        => 'confirmacao_de_nova_senha_de_acesso',
            'label'       => 'Confirmação de nova senha',
            'placeholder' => 'Confirme sua nova senha',
            'class' => 'form-control',
        ]);
        
        /**
        $this->addText([
            'name'  => 'exemplo_data',
            'label' => 'Data',
            'column-size' => 'sm-1',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Início',
        ]);

        $this->addStaticText([
            'name' => 'exemplo_static',
            'label' => 'Campo estático',
            'value' => 'elton@tmax.com.br',
            'column-size' => 'sm-2',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Início',
        ]);

        $this->addSelect([
            'name' => 'exemplo_select',
            'label' => 'Exemplo Select',
            'column-size' => 'sm-3',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'value_options' => [
                1 => 'Opção 1',
                2 => 'Opção 2',
            ],
            'empty_option' => '', //É possível colocar um texto para o valor null
            'help-block' => 'Será a opção alterarsenha',
        ]);


        $this->addMultiCheckbox([
            'name' => 'exemplo_checkbox',
            'label' => 'Tipos de usuário',
            'column-size' => 'sm-1',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'value_options' => [
                [
                    'label' => 'Checkbox 1',
                    'value' => 1,
                    'attributes' => ['id' => 'exemplo_checkbox_1']
                ],
                [
                    'label' => 'Checkbox 2',
                    'value' => 2,
                    'attributes' => ['id' => 'exemplo_checkbox_2']
                ],
            ],

        ]);
        */
        
        
        $this->addSubmit([
            'class'   => 'btn btn-success',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Salvar',
        ]);

        $this->addButton([
            'name'    => 'reset',
            'label'   => 'Limpar formulário',
            'class'   => 'btn btn-primary',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "resetForm(this.form)",
        ]);

        $this->addButton([
            'name'    => 'cancel',
            'label'   => 'Cancelar',
            'class'   => 'btn btn-warning',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "goTo('/admin/index/index')",
        ]);
    }
    
    /**
     *
     * @name setData
     * @param array $data
     */
    public function setData($data)
    {
        parent::setData($data);
    }
    
    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
