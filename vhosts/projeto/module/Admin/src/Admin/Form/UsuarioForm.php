<?php
/**
 * Form
 */
namespace Admin\Form;

use Core\Form\BaseForm;
use Admin\Service\NivelDeAcessoService;

/**
 * Formulário para incluir um item
 *
 * @name    UsuarioForm
 * @package Admin\Form
 * @author  Author <e-mail>
 */
class UsuarioForm extends BaseForm
{
    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
        
        $nivelDeAcessoService =  $this->serviceManager->get(NivelDeAcessoService::class);

        $this->setAttribute('id', 'admin-usuario-save');
        

        $this->addHidden([
            'name' => 'pk_id_usuario',
        ]);
        
        $this->addCheckbox([
            'name' => 'esta_ativo',
            'label' => 'Está ativo?',
            'data_attributes' => [
                'data-size' =>'small',
                'data-toggle' => 'toggle',
                'data-on' => 'Sim',
                'data-off' => 'Não',
                'data-onstyle' => 'success',
                'data-offstyle' => 'danger',
            ],
        ]);
        
        $this->get('esta_ativo')->setChecked(1);

        $this->addText([
            'name'  => 'nome',
            'label' => 'Nome completo do usuário',
            'placeholder' => 'Nome completo do usuário',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'help-block' => 'Nome do usuário',
        ]);
        
        $this->addText([
            'name'  => 'login',
            'label' => 'Login de acesso',
            'placeholder' => 'Login de acesso',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'help-block' => 'Login de acesso',
        ]);
          
        $this->addText([
            'name'  => 'email',
            'label' => 'Email ',
            'placeholder' => 'Email do usuário',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'help-block' => 'Login de acesso',
        ]);
        
        $this->addSelect([
            'name' => 'fk_id_nivel_de_acesso',
            'label' => 'Nível de acesso',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1 '
            ],
            'value_options' => $nivelDeAcessoService->fetchPairs(),
            'empty_option' => 'Escolha um nível',
            'help-block' => 'Será a opção default',
            'class' =>'form-control select2',
            'id'=> '',
        ]);
          
        
        
        $this->addSubmit([
            'class'   => 'btn btn-success',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Salvar',
        ]);

        $this->addButton([
            'name'    => 'reset',
            'label'   => 'Limpar formulário',
            'class'   => 'btn btn-primary',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "resetForm(this.form)",
        ]);

        $this->addButton([
            'name'    => 'cancel',
            'label'   => 'Cancelar',
            'id'=> '',
            'class'   => 'btn btn-warning',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "goTo('/admin/usuario/index')",
        ]);
    }
    
    /**
     *
     * @name setData
     * @param array $data
     */
    public function setData($data)
    {
        parent::setData($data);
    }
    
    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
