<?php
/**
 * Form
 */
namespace Admin\Form;

use Core\Form\BaseFormFilter;
use Admin\Service\NivelDeAcessoService;

/**
 * Formulário para filtrar itens
 *
 * @name    UsuarioFormFilter
 * @package Admin\Form
 * @author  Author <e-mail>
 */
class UsuarioFormFilter extends BaseFormFilter
{

    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
        
        $nivelDeAcessoService =  $this->serviceManager->get(NivelDeAcessoService::class);

        $this->setAttribute('id', 'admin-usuario-index');
        $this->setAttribute('class', 'form-vertical');

        $this->addText([
            'name'  => 'nome',
            'label' => 'Nome',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Description of item',
        ]);
        
        $this->addText([
            'name'  => 'email',
            'label' => 'Email',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Description of item',
        ]);

        $this->addText([
            'name'  => 'login',
            'label' => 'Login',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Description of item',
        ]);
        
        $this->addSelect([
            'name' => 'fk_id_nivel_de_acesso',
            'required'=> false,
            'label' => 'Nível acesso ',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'value_options' => $nivelDeAcessoService->fetchPairs(),
            'empty_option' => 'Escolha uma opção',
            'help-block' => 'Será a opção default',
            'class' =>'form-control select2_allow_clear',
            'id' => '',
        ]);
        
        $this->addSelect([
            'name' => 'esta_ativo',
            'required'=> false,
            'class'=>'form-control select2_allow_clear',
            'label' => 'Está Ativo?',
            'column-size' => 'sm-3',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'value_options' => [
                1 => 'Sim',
                0 => 'Não',
            ],
            'empty_option' => 'Todos',
            'help-block' => 'Será a opção pessoa',
        ]);
        
        $this->addSubmit([
            'value' => 'Buscar',
            'class' => 'btn btn-primary',
        ]);
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
