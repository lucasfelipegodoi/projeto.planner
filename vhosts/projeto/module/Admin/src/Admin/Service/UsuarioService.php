<?php
/**
 * Service
 */
namespace Admin\Service;

use Core\Service\Service;
use \Exception;
use Admin\Model\UsuarioModel;
use Admin\Model\NivelDeAcessoModel;
use Core\Service\MailService;
use Plan\Model\UserModel;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Expression;
use Admin\Parameter\UsuarioParameter;
use Admin\Parameter\NivelDeAcessoParameter;
use Core\Service\AutenticacaoService;

/**
 * Classes com as regras de négocio
 *
 * @name       UsuarioService
 * @package    Admin\Service
 * @author     Author <e-mail>
 */
class UsuarioService extends Service
{
    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */


   
    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $usuarioModel = $this->getModel(UsuarioModel::class);
        $usuarioModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($usuarioModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }



    public function getClassName()
    {
        return self::class;
    }
}
