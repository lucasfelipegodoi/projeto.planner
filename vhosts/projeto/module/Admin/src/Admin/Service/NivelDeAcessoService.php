<?php
/**
 * Service
 */
namespace Admin\Service;

use Core\Service\Service;
use \Exception;
use Admin\Model\NivelDeAcessoModel;
use Admin\Parameter\NivelDeAcessoParameter;

/**
 * Classes com as regras de négocio
 *
 * @name       NivelDeAcessoService
 * @package    Admin\Service
 * @author     Author <e-mail>
 */
class NivelDeAcessoService extends Service
{
    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($data)
    {
        $nivelDeAcessoModel = $this->getModel(NivelDeAcessoModel::class);

        if ($this->isValid($nivelDeAcessoModel, $data) === false) {
            return false;
        }

        try {
            $this->beginTransaction();
            $this->saveModel($nivelDeAcessoModel);
            $this->commit();

            return $nivelDeAcessoModel;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $nivelDeAcessoModel = $this->getService(NivelDeAcessoModel::class);
        $nivelDeAcessoModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($nivelDeAcessoModel);
            $this->commit();

            return $result;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $nivelDeAcessoModel = $this->getModel(NivelDeAcessoModel::class);
        $nivelDeAcessoModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($nivelDeAcessoModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Método para retornar os itens paginados
     *
     * @name         paginate
     *
     * @param  array $parametres
     *
     * @access public
     * @return \Zend\Paginator\Paginator
     */
    public function paginate(array $parameters = [])
    {
        $nivelDeAcessoParameter = $this->getService(NivelDeAcessoParameter::class);
        
        $select  = $this->getSelect();
        $options = [
            'select' => $select,
            'parameters' => $parameters,
            'models' => [
                ['model' => $this->getModel(NivelDeAcessoModel::class), 'alias' => 'na'],
            ],
        ];
        
        $select->from(['n' => NivelDeAcessoModel::TABLE_NAME])
            ->columns($this->getModelColumns(NivelDeAcessoModel::class));
        
        $nivelDeAcessoParameter->setValuesInSelect($options);

        return $this->generatePaginator([
            'itemCountPerPage' => self::ITEM_COUNT_PER_PAGE,
            'select'           => $select,
            'sql'              => $this->getTable(NivelDeAcessoModel::class)->getSql(),
            'page'             => isset($parameters['page']) ? $parameters['page'] : 1,
        ]);
    }

    /**
     * Métodos para retornar todos os itens
     *
     * @name   getAll
     * @access public
     * @return array
     */
    public function getAll()
    {
        $select = $this->getSelect()
            ->from(['n' => NivelDeAcessoModel::TABLE_NAME])
            ->columns($this->getModelColumns(NivelDeAcessoModel::class));

        return $this->fetchAll($select);
    }

    /**
     * Método para retornar todos os itens como [chave => valor]
     *
     * @name         fetchPairs
     *
     * @param  array $parametros
     *
     * @access public
     * @return array
     */
    public function fetchPairs(array $parametros = [])
    {
        $nivelDeAcessoModel   = $this->getModel(NivelDeAcessoModel::class);
        $options = [
            'tableName'   => NivelDeAcessoModel::TABLE_NAME,
            'index'       => $nivelDeAcessoModel->getPrimaryKeyField(),
            'description' => 'nivel_de_acesso', //Trocar pelo campo específico
            'filters'     => $parametros,
        ];

        return parent::fetchPairs($options);
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
