<?php
/**
 * Module config
 */
use User\Form\UserForm;
use User\Form\UserFormFilter;
use User\Model\UserModel;
use User\Service\UserService;

return [
//    'asset_bundle' => array(
//        'assets' => array(
//            'less' => array('@zfRootPath/vendor/twbs/bootstrap/less/bootstrap.less')
//        )
//    ),
    'controllers'     => [
        'invokables' => [
            'User\Controller\User' => 'User\Controller\UserController'
        ],
    ],
    'service_manager' => [
        'factories' => [
            // ----------------- Factories Forms ---------------------------- //

            'User\Form\UserForm' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new UserForm($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'User\Model\UserModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new UserModel($options);
            },

            // -------------- Factories Services ---------------------------- //

            'User\Service\UserService' => function ($serviceManager) {

                $options = [];

                return new UserService($options);
            },

        ],
    ],
    'router'          => [
        'routes' => [
            'user' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/user',
                    'defaults' => [
                        'controller'    => 'User',
                        'action'        => 'index',
                        '__NAMESPACE__' => 'User\Controller',
                        'module'        => 'user'
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'default' => [
                        'type'         => 'Segment',
                        'options'      => [
                            'route'       => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'User'    => [],
                        ],
                        'child_routes' => [ //permite mandar dados pela url
                            'wildcard' => [
                                'type' => 'Wildcard'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager'    => [
        'display_not_found_reason' => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'display_exceptions'       => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'partials/paginator/control' => __DIR__ . '/../../Core/view/partials/paginator/control.phtml',
            'layout/layout'              => __DIR__ . '/../../Core/view/layout/layout.phtml',
            'layout/layout_email'        => __DIR__ . '/../../Core/view/layout/layout_email.phtml',
            'error/404'                  => __DIR__ . '/../../Core/view/error/404.phtml',
            'error/index'                => __DIR__ . '/../../Core/view/error/index.phtml',
        ],
        'template_path_stack'      => [
            'user' => __DIR__ . '/../view',
        ],
    ],
];
