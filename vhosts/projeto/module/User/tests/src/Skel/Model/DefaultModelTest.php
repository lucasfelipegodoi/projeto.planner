<?php
/**
 * Model
 */
namespace Skel\Model;

use Core\Test\ModelTestCase;
use Skel\DataSourceTest;
use Skel\Model\DefaultModel;
use Zend\InputFilter\InputFilterInterface;

/**
 * Testes do model
 *
 * @group   Model
 *
 * @name    DefaultModelTest
 * @package Paciente\Model
 * @author  Author <e-mail>
 */
class DefaultModelTest extends ModelTestCase
{

    /**
     * Namespace completo do Model
     *
     * @var string
     */
    protected $modelFQDN = DefaultModel::class;

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->executeDependentRoutines();  
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }


    /**
     * Retorna os inputs filters do model
     *
     * @name   testGetInputFilter
     * @access public
     * @return void
     */
    public function testGetInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $inputFilter  = $defaultModel->getInputFilter();

        $this->assertInstanceOf(InputFilterInterface::class, $inputFilter);
    }

    /**
     * Testa se os input filters são válidos
     *
     * @name   testValidInputFilter
     * @access  public
     * @return void
     */
    public function testValidInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $inputFilter  = $defaultModel->getInputFilter();
        
        $this->assertTrue($inputFilter->has($defaultModel->getPrimaryKeyField()));
        $this->assertTrue($inputFilter->has('description'));
    }

    /**
     * Verifica se um input filter inválido lança uma exceção.
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testInvalidInputFilter
     * @access public
     * @return void
     */
    public function testInvalidInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $defaultModel->setData(DataSourceTest::getInvalidData(['model' => DefaultModel::class])[0]);
    }

    /**
     * Verifica se uma inserção válida do model funciona corretamente.
     *
     * @name   testValidInsert
     * @access public
     * @return void
     */
    public function testValidInsert()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $item  = DataSourceTest::getValidData(['model' => DefaultModel::class]);

        $defaultModel->setData($item[0]);
        $this->saveModel($defaultModel);

        $this->assertEquals(1, $defaultModel->getPrimaryKeyValue());
    }

    /**
     * Verifica se uma inserção inválida do model lança uma exceção.
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testInvalidInsert
     * @access public
     * @return void
     */
    public function testInvalidInsert()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $item         = DataSourceTest::getInvalidData(['model' => DefaultModel::class]);

        $defaultModel->setData($item[0]);
        
        $this->saveModel($defaultModel);
    }

    /**
     * Verifica se a atualização do model está funcionando corretamente.
     *
     * @name   testValidUpdate
     * @access public
     * @return void
     */
    public function testValidUpdate()
    {
        $defaultModel    = $this->getModel(DefaultModel::class);
        $primaryKeyField = $defaultModel->getPrimaryKeyField();
        $item  = DataSourceTest::getValidData(['model' => DefaultModel::class]);
        $diff  = array_diff_assoc($item[0], $item[1]);
        
        $defaultModel->setData($item[0]);
        $this->saveModel($defaultModel);
        
        $item[1][$primaryKeyField] = $defaultModel->getPrimaryKeyValue();
                
        $defaultModel->setData($item[1]);
        $this->saveModel($defaultModel);

        
        $this->assertEquals(1, $defaultModel->getPrimaryKeyValue());
        $this->assertGreaterThan(0, count($diff));
    }

    /**
     * Verifica se a exclusão do model está funcionando corretamente
     *
     * @expectedException Core\Model\EntityException
     * @expectedExceptionMessage Could not find row
     *
     * @name   testValidDelete
     * @access public
     * @return void
     */
    public function testValidDelete()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $item  = DataSourceTest::getValidData(['model' => DefaultModel::class]);
        $defaultModel->setData($item[0]);

        $this->saveModel($defaultModel);
        $this->deleteModel($defaultModel);
        $this->getSavedModel($defaultModel);
    }
    
    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        //Coloque aqui as rotinas que deverão ser executadas antes dos testes iniciarem
        //Por exemplo: $this->migrationService->runSeed('NiveisDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);                
    } 
    
}
