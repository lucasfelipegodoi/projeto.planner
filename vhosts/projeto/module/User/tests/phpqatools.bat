echo "============================================  PHP Coding Standards Fixer ==========================================="
../../../vendor/bin/php-cs-fixer fix ../ --level=psr2


echo "============================================ PHPUNIT ==========================================="
../../../vendor/bin/phpunit


echo "============================================ PHP Code Sniffer ==========================================="
rm -R build/phpcs/
mkdir build/phpcs/

../../../vendor/bin/phpcs --standard=PSR2 --report=xml ../src/  --report-file=build/phpcs/report_code_sniffer.xml


echo "============================================ PHP Mess Detector ==========================================="
rm -R build/phpmd/
mkdir build/phpmd/

../../../vendor/bin/phpmd ../src/ html --reportfile build/phpmd/report_unusedcode.html unusedcode
../../../vendor/bin/phpmd ../src/ html --reportfile build/phpmd/report_controversial.html controversial
../../../vendor/bin/phpmd ../src/ html --reportfile build/phpmd/report_design.html design
../../../vendor/bin/phpmd ../src/ html --reportfile build/phpmd/report_naming.html naming
../../../vendor/bin/phpmd ../src/ html --reportfile build/phpmd/report_codesize.html codesize


echo "============================================ PHP Depend ==========================================="
rm -R build/pdepend/
mkdir build/pdepend/

../../../vendor/bin/pdepend --summary-xml=build/pdepend/summary.xml --jdepend-chart=build/pdepend/jdepend.svg --overview-pyramid=build/pdepend/pyramid.svg ../src/


echo "============================================ PHP Copy/Paste Detector ==========================================="
rm -R build/phpcpd/
mkdir build/phpcpd/

../../../vendor/bin/phpcpd --min-lines 10 --min-tokens 10 --log-pmd build/phpcpd/report.xml ../src/


echo "============================================ PHP CodeBrowser ==========================================="
rm -R build/phpcb/
mkdir build/phpcb/

../../../vendor/bin/phpcb --source ../src --output build/phpcb


echo "============================================ PHP L.O.C. ==========================================="
rm -R build/phploc/
mkdir build/phploc/

../../../vendor/bin/phploc ../src --log-xml build/phploc/relatorio.xml


echo "============================================ PHP Documentor Phpdox ==========================================="
rm -R build/phpdox/
mkdir build/phpdox/

../../../vendor/bin/phpdox --file ../phpdox.xml


echo "============================================ PHP Metrics ==========================================="
rm -R build/phpmetrics/
mkdir build/phpmetrics/

../../../vendor/bin/phpmetrics --report-html=build/phpmetrics/index.html ../src