<?php
/**
 * Aqui deverão ser salvos os script de criação, inserção e exclusão das tabelas
 * que serão utilizadas durante os testes.
 * A ordem de criação dos arrays de cada tabela deverão seguir as ordens
 * de depedência entre cada tabela. *
 *
 */
return array(
//    'nome_da_tabela' => array(
//        'beforeCreate' => "DROP TABLE IF EXISTS nome_da_tabela;",
//        'create'       => "",
//        'drop'   => "DROP TABLE nome_da_tabela;",
//        'insert' => array(
//
//        ),
//    ),
);
