<?php
/**
 * DataSourceTest
 */
namespace Skel;

/**
 * Classe provedora dos dados de testes
 *
 * @name    DataSourceTest
 * @package Paciente
 * @author  Author <e-mail>
 */
class DataSourceTest
{
    /**
     * Exemplo de utilização do método de retorno dos dados válidos para teste
     *
     * @name   getValidData
     * @access public static
     * @return array
     */
    public static function getValidData(array $options)
    {
        $data = [
            'Paciente\Model\DefaultModel' => [
                [
                    'pk_id' => null,
                    'description' => 'Novo teste',
                ],
                [
                    'pk_id' => null,
                    'description' => 'New description',
                ],
            ],
            ////////////////////////////////////////////////////////////////////
            
        ];
        
        return $data[$options['model']];
    }

    /**
     * Exemplo de utilização do método de retorno dos dados inválidos para teste
     *
     * @name   getInvalidData
     * @access public static
     * @return array
     */
    public static function getInvalidData(array $options)
    {
        $data = [
            'Paciente\Model\DefaultModel' => [
                [
                    'pk_id' => null,
                    'description' => null,
                ],
                [
                    'pk_id' => null,
                    'description' => null,
                ],
            ],
            ////////////////////////////////////////////////////////////////////
        ];
        
        return $data[$options['model']];
    }
}
