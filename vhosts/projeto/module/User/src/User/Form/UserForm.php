<?php
/**
 * Form
 */
namespace User\Form;

use Core\Form\BaseForm;

/**
 * Formulário para incluir um item
 *
 * @name    UserForm
 * @package User\Form
 * @author  Lucas Godoi <e-mail>
 */
class UserForm extends BaseForm
{
    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('id', 'admin-autenticacao-save');
        

        $this->addHidden([
            'name' => 'ID_Usuario',
        ]);

        $this->addText([
            'name'  => 'Nome',
            'label' => 'Nome Completo',
            'placeholder' => 'Nome Completo',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'help-block' => 'Nome Completo',
        ]);


       $this->addText([
                'name'  => 'Email',
                'label' => 'Email',
                'placeholder' => 'contato@email.com',
                'label_attributes' => [
                    'class' => 'col-sm-2'
                ],
                'help-block' => 'Email',
       ]);
       $this->addPassword([
                'name'  => 'Senha',
                'label' => 'Senha',
                'label_attributes' => [
                    'class' => 'col-sm-2'
                ],
                'help-block' => 'Email',
       ]);


        
        $this->addSubmit([
            'class'   => 'btn btn-success',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Salvar',
        ]);


        $this->addButton([
            'name'    => 'cancel',
            'label'   => 'Cancelar',
            'class'   => 'btn btn-default',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "goTo('/admin/autenticacao/index')",
        ]);
    }
    
    /**
     * 
     * @name setData
     * @param array $data
     */
    public function setData($data)
    {
        parent::setData($data);
    }
    
    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
