<?php

/**
 * Controller
 */

namespace User\Controller;

use \Exception;
use Core\Model\EntityException;
use Core\Controller\ActionController;
use User\Form\DefaultForm;
use User\Form\DefaultFormFilter;
use User\Form\UserForm;
use User\Service\DefaultService;
use User\Service\UserService;
use Zend\View\Model\ViewModel;

/**
 * Controller Default
 *
 * @name    DefaultController
 * @package User\Controller
 * @author  Author <e-mail>
 */
class UserController extends ActionController
{
    /**
     * Método para excluir um item
     *
     * @name   deleteAction
     * @access public
     * @return void
     */
    public function deleteAction()
    {
        $defaultService = $this->getService(UserService::class);
        $id = $this->params()->fromRoute('id', 0);

        if ($defaultService->deleteById($id)) {
            $this->addSuccessMessage('Exclusão realizada com sucesso!');
        } else {
            $this->addErrorMessage('Não foi possível executar a exclusão!');
        }

        $this->redirect()->toUrl('/user/user/index');
    }

    /**
     * Método para gerenciar os itens
     *
     * @name   indexAction
     * @access public
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(UserService::class);
        $params = $this->getParamsFromRoute();

        $viewModel->setVariables([
            'paginator' => $defaultService->paginate($params),
        ]);

        return $viewModel;
    }

    /**
     * Método para salvar um item
     *
     * @name   saveAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function saveAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(UserService::class);
        $form = $this->getService(UserForm::class);
        $id = $this->params()->fromRoute('id', 0);

        $viewModel->setVariables([
            'form' => $form,
        ]);
        
        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {
                    $this->addSuccessMessage('Ação realizada com sucesso!');
                    $this->redirect()->toUrl('/user/user/index');
                }
            }
        } catch (EntityException $entityException) {
            $this->addErrorMessage('Item inválido');
            $this->redirect()->toUrl('/user/user/save');
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/user/user/save');
        }

        return $viewModel;
    }

    /**
     * Método para visualizar um item
     *
     * @name   viewAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $UserService = $this->getService(UserService::class);
        $id = $this->params()->fromRoute('id', 0);

        try {
            $viewModel->setVariables([
                'item' => $UserService->getItemById($id),
            ]);

            $viewModel->setTerminal(true);

            return $viewModel;
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/user/default/index');
        }
    }

    public function perfilAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $UserService = $this->getService(UserService::class);
        $id = $this->params()->fromRoute('id', 0);

        $viewModel->setVariables([
            'item' => $UserService->getPerfil($id),
        ]);

        return $viewModel;
    }
}
