<?php
/**
 * Service
 */
namespace User\Service;

use Core\Service\Service;
use \Exception;
use User\Model\UserModel;
use Zend\Db\Sql\Expression;
use Core\Service\MailService;


/**
 * Classes com as regras de négocio
 *
 * @name       UserService
 * @package    User\Service
 * @author     Lucas Godoi <e-mail>
 */
class UserService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($data)
    {
        $UserModel = $this->getModel(UserModel::class);

        if ($this->isValid($UserModel, $data) === false) {
            return false;
        }

        try {
            $this->beginTransaction();


            $UserModel->DT_Registro = date('Y-m-d H:i:s');
            $UserModel->Senha = hash('sha512',$UserModel->Senha);
            $UserModel->Ativo = 1;

            $this->saveModel($UserModel);
            $this->commit();

            return $UserModel;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $UserModel = $this->getService(UserModel::class);
        $UserModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($UserModel);
            $this->commit();

            return $result;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return \Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $UserModel = $this->getModel(UserModel::class);
        $UserModel->setPrimaryKeyValue($id);
        
        try {

            $retorno = $this->get($UserModel);

            return $retorno;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function getAllUsuarios($email)
    {
        $select = $this->getSelect();
        $select->from(UserModel::TABLE_NAME)->columns(["email","ID_Usuario"] )
            ->where->like("email","%".$email."%");

        $var = $this->fetchAll($select);

        $posicaoArray = 0;

        $dados = array();

        foreach($var as $v)
        {
            $dados[$posicaoArray]["label"] = $v["email"];
            $dados[$posicaoArray]["id"] = $v["ID_Usuario"];

            $posicaoArray++;

        }

        return $dados;
    }

    /**
     *
     * @param type $pkIdUsuario
     * @return type
     * @throws Exception
     */
    public function gerarESalvarNovaSenhaDeAcesso($pkIdUsuario)
    {
        try {

            $this->beginTransaction();

            $usuarioModel = $this->getItemById($pkIdUsuario);
            $senhaSemHash = substr(uniqid(rand(), true), 0, 8);
            $usuarioModel->Senha = hash('sha512', $senhaSemHash);
            $this->saveModel($usuarioModel);

            $this->commit();

            return $senhaSemHash;

        } catch (Exception $exception) {

            $this->rollback();
            throw $exception;

        }
    }

    /**
     *
     * @param type $dados
     * @return boolean
     * @throws Exception
     */
    public function enviarNovaSenhaDeAcesso($dados)
    {
        $dados = $this->normalizeDataSource($dados);
        $identificacao = isset($dados['login_ou_email']) ? $dados['login_ou_email'] : null;

        try {
            $this->beginTransaction();

            if (! $identificacao) {
                $this->throwMandatoryConditionException('Informe seu email  de acesso!');
            }

            $dadosDoUsuario = $this->getAllUsuarios($identificacao);

            if (!$dadosDoUsuario) {
                $this->throwNotFoundException('Nao foi encontrado nenhum usuário com o e-mail informado.');
            }

            $novaSenha = $this->gerarESalvarNovaSenhaDeAcesso($dadosDoUsuario[0]['id']);

            $dadosMapeadosParaOEmail = [
                'login' => $dadosDoUsuario[0]['label'],
                'senha_de_acesso' => $novaSenha ,
                'email' => $dadosDoUsuario[0]['label'],
            ];

            $this->enviarEmailComOsDadosDeAcesso($dadosMapeadosParaOEmail);

            $this->commit();

            return true;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     *
     *
     * @name   enviarEmailComOsDadosDeAcesso
     *
     * @param  $dadosDeAcesso
     *
     * @access public
     * @throws \Exception
     */
    public function enviarEmailComOsDadosDeAcesso(array $dadosDeAcesso)
    {
        $mailService    = $this->getService(MailService::class);
        $dados['title'] = 'Seguem os seus dados de acesso:<br><br>';
        $dados['body']  = '<b>Login: </b>' . $dadosDeAcesso['login'] . ' <br>' .
            '<b>Senha de acesso: </b>' . $dadosDeAcesso['senha_de_acesso'] . '<br>';

        try {
            $mailService->setVariables($dados);
            $status = $mailService->setSubject("PLANNER \ Dados de acesso ao sistema")
                ->addTo($dadosDeAcesso['email'])
                ->send();

            if ($status->isValid() === false) {
                $mensagem = 'NÃ£o foi possÃ­vel enviar o e-mail ' .
                    ' com os dados de acesso para o usuÃ¡rio <b>' .
                    $dadosDeAcesso['email'] . '</b>';

                $this->throwSendingEmailException($mensagem);
            }
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
