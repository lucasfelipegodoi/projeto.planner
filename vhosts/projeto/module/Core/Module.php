<?php

namespace Core;

use Zend\I18n\Translator\Translator;
use Zend\Mvc\I18n\Translator as MvcI18nTranslator;
use Zend\Validator\AbstractValidator;

class Module
{

    /**
     *
     * @name          onBootstrap
     *
     * @param  objetc $event
     *
     * @access public
     * @return void
     */
    public function onBootstrap($event)
    {
        $config = $event->getApplication()->getServiceManager()->get('config');
        $this->setTranslator($config['translator']['locale']);
        $sharedManager = $event->getApplication()->getEventManager()->getSharedManager();

        $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function ($e) {
            $controller      = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config          = $e->getApplication()->getServiceManager()->get('config');

            if (isset($config['module_layout'][$moduleNamespace])) {
                $controller->layout($config['module_layout'][$moduleNamespace]);
            }
        }, 100);
    }

    /**
     *
     * @name   getAutoloaderConfig
     * @access public
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     *
     * @name   getConfig
     * @access public
     * @return file
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     *
     * @name   getServiceConfig
     * @access public
     * @return array
     */
    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'DbAdapter' => 'Core\Db\AdapterServiceFactory',
                'DbAdapterInformationSchema' => 'Core\Db\AdapterInformationSchemaServiceFactory'
            )
        );
    }

    /**
     *
     * @name          setTranslator
     *
     * @param  string $locale = 'pt_BR'
     *
     * @access private
     * @return void
     */
    private function setTranslator($locale = 'pt_BR')
    {
        $translator = new MvcI18nTranslator(new Translator());
        $translator->addTranslationFile(
            'phpArray',
            __DIR__ . '/../../vendor/zendframework/zend-i18n-resources/languages/' . $locale . '/Zend_Validate.php',
            'default',
            $locale
        );

        $translator->setLocale($locale);

        AbstractValidator::setDefaultTranslator($translator);
    }
}
