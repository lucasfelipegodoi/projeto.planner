<?php
use AcMailer\Service\MailService as AcMailerMailService;
use Core\Service\AutenticacaoService;
use Core\Service\AutorizacaoService;
use Core\Service\ExcelService;
use Core\Service\MailService as CoreMailService;
use Core\Service\PdfService;
use Core\Service\MigrationService;
use Core\Service\PhabricatorConduitService;
use Core\Acl\Builder;
use Core\Service\ExceptionService;
use Core\View\Helper\RenderAuthorizedLink;
use Core\View\Helper\YesOrNo;
use Core\Filter\ToIntIfNotEmpty;
use Zend\View\Model\ViewModel;
use Phinx\Wrapper\TextWrapper;
use Phinx\Console\PhinxApplication;

return array(
    'di'              => array(),
    'view_helpers' => array(
        'invokables' => array(
            'renderAuthorizedLink' => RenderAuthorizedLink::class,
            'yesOrNo' => YesOrNo::class,
        ),
    ),
    'service_manager' => array(
        'factories' => array(
           
            'Core\Acl\Builder' => function ($serviceManager) {
                $aclBuilder = new Builder();
                
                return $aclBuilder;
            },
            // -------------------------- Factories Filters ---------------------------- //
            'Core\Filter\ToIntIfNotEmpty' => function () {
                return new ToIntIfNotEmpty();
            }, 
                    
           // -------------------------- Factories Forms ---------------------------- //


            // -------------------------- Factories Models ---------------------------- //


            // -------------------------- Factories Services ---------------------------- //
            'Core\Service\PhabricatorConduitService' => function ($serviceManager) {
                $defaultProject = 'PHID-PROJ-cooqtbhfxl3mn3hnqyjg';
                $options        = [
                    'apiToken'          => '',
                    'host'              => '',
                    'projectPHIDs'      => [$defaultProject],
                    'viewPolicy'        => $defaultProject,
                    'editPolicy'        => $defaultProject,
                    'priorityTaskLevel' => 25,
                    'ownerPHID'         => null,
                    'ccPHIDs'           => [],
                    'sizeOfTask'        => 0,
                    'estimatedHours'    => 0,
                ];
                
                return new PhabricatorConduitService($options);
            },
            'Core\Service\ExcelService'        => function ($serviceManager) {
                $options = array(
                    'phpExcel' => new PHPExcel(),
                );

                return new ExcelService($options);
            },
            'Core\Service\PdfService'          => function ($serviceManager) {
                $options = array(
                    'domPdf' => new DOMPDF(),
                );

                return new PdfService($options);
            },
            'Core\Service\AutenticacaoService' => function ($serviceManager) {
                $options = array(
                    'serviceManager' => $serviceManager,
                    'config'         => array(
                        'tableName'            => 'usuario',//Nome da tabela onde serão buscados os dados de acesso
                        'identityColumn'       => 'Email',//Nome da coluna de login na base de dados
                        'credentialColumn'     => 'Senha',//Nome da coluna de senha na base de dados
                        'identityField'        => 'email',//Nome do campo de login no formulário
                        'credentialField'      => 'senha',//Nome do campo de senha no formulário
                        'useHashForCredential' => true,
                    ),
                );

                return new AutenticacaoService($options);
            },
            'Core\Service\AutorizacaoService'  => function ($serviceManager) {
                $config  = $serviceManager->get('Config');
                $options = array(
                    'serviceManager' => $serviceManager,
                );

                $autorizacaoService = new AutorizacaoService($options);
                $autorizacaoService->setSourceAuthorizations($config['acl']);

                return $autorizacaoService;
            },
            'Core\Service\MailService'         => function ($serviceManager) {
                $config  = $serviceManager->get('Config');
                $executeRealSend = isset($config['environment_config']['execute_real_email_sending']) ?
                        $config['environment_config']['execute_real_email_sending'] :
                        false;
                 
                $viewModel = new ViewModel();
                $viewModel->setVariables(array(
                    'title' => null,
                    'body' => null,
                ))->setTemplate('layout/layout_email');

                $options = array(
                    //Comentar a linha 'executeRealSend' quando for realizar um ernvio de e-mail real independente do 'APPLICATION_ENV'
                    'executeRealSend' => $executeRealSend,
                    'mailService' => $serviceManager->get(AcMailerMailService::class),
                    'viewModel'   => $viewModel,
                );

                return new CoreMailService($options);
            },
            'Core\Service\MigrationService' => function ($serviceManager) {
                $dir  = realpath(__DIR__ . '/../../../');
                $configuration = $dir . '/phinx.yml';
                $parser        = 'yaml';
                $environment   = 'development';
                
                $phinxApplication  = new PhinxApplication();
                $textWrapper       = new TextWrapper($phinxApplication, [
                    'environment'   => $environment,
                    'configuration' => $configuration,
                    'parser'        => $parser,
                ]);
        
                $options = [
                    'textWrapper'      => $textWrapper,
                    'phinxApplication' => $phinxApplication,
                    'environment'      => $environment,
                    'configuration'    => $configuration,
                    'parser'           => $parser,
                ];
                
                return new MigrationService($options);
            },
            'Core\Service\ExceptionService' => function ($serviceManager) {
                return new ExceptionService();
            },
        ),
    ),
    'translator'      => array(
        'locale' => 'pt_BR',
    ),
);
