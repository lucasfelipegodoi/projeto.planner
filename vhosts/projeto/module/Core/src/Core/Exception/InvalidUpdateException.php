<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class InvalidUpdateException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 3;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 3, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
