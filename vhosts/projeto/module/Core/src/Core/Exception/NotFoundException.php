<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class NotFoundException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 7;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 7, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
