<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class ForbiddenException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 403;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 403, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
