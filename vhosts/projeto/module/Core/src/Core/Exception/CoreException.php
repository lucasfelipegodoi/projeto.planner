<?php

/**
 * @package    Core\Model
 * @author     Marcos Garcia<garcia@coderockr.com>
 */
namespace Core\Exception;

use \Exception;

abstract class CoreException extends Exception
{
    /**
     *
     * @var int
     */
    protected $code = 1;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 1, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
