<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class SendingEmailException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 9;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 9, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
