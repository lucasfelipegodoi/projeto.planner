<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class MandatoryConditionException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 6;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 6, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
