<?php

/**
 * @package    Core\Model
 * @author     Marcos Garcia<garcia@coderockr.com>
 */
namespace Core\Exception;

use \Exception;
use Core\Exception\CoreException;

/**
 *
 */
abstract class BusinessRuleException extends CoreException
{
    /**
     *
     * @var int
     */
    protected $code = 2;

    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 2, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
