<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class InvalidPeriodException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 5;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 5, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
