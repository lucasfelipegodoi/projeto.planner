<?php

/**
 * @package    Core\Model
 * @author     Marcos Garcia<garcia@coderockr.com>
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class PreexistingConditionException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 8;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 8, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
