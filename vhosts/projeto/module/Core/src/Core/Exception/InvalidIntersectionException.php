<?php

/**
 * @package    Core\Model
 */
namespace Core\Exception;

use \Exception;

/**
 *
 */
class InvalidIntersectionException extends BusinessRuleException
{
    /**
     *
     * @var int
     */
    protected $code = 4;
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 4, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
