<?php

namespace Core\Form;

use Exception;
use Zend\Form\Form;
use Zend\ServiceManager\ServiceManager;
use Core\Service\ExceptionService;
use Zend\InputFilter\InputFilterProviderInterface;

/**
 *
 * @name       BaseForm
 * @category   Core
 * @package    Core
 * @subpackage CoreForm
 */
class BaseForm extends Form implements InputFilterProviderInterface
{
    /**
     *
     * @var type
     */
    protected $serviceManager;
    
    /**
     *
     * @var type
     */
    protected $exceptionService;

    /**
     *
     * @name         __construct
     *
     * @param  array $options
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = array())
    {
        parent::__construct();

        if (!isset($options['serviceManager']) && !$options['serviceManager'] instanceof ServiceManager) {
            throw new Exception("Not found an instance of ServiceManager on index ['serviceManager']");
        }

        $this->serviceManager   = $options['serviceManager'];
        $this->exceptionService = $this->serviceManager->get(ExceptionService::class);
        

        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->setAttribute('id', '');
        $this->setAttribute('action', '');
        $this->setAttribute('role', 'form');
        $this->setAttribute('data-parsley-validate', '');

    }

    /**
     *
     * @name         addHidden
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addHidden(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\Hidden',
            'attributes' => array(
                'id'    => isset($options['id']) ? $options['id'] :$options['name'],
                'value' => isset($options['value']) ? $options['value'] : null,
                'style' => isset($options['style']) ? $options['style'] : null,
                'class' => isset($options['class']) ? $options['class'] : null,
            ),
           'options' => array(
                'label' => isset($options['label']) ? $options['label'] : null,
            ),
        ));

        return $this;
    }
    
    
     /**
     *
     * @name         setMultipleSelect
     *
     * @param  array $options = array()
     *
     * @access protected

     */
    
    protected function setMultipleSelect(array $options)
    {
        $data = isset($options['data']) ? $options['data']  : [];
        $name = isset($options['name']) ? $options['name']  : null;
        
        if ($name === null) {
            $this->exceptionService->throwNotFoundException("The key 'data' wasn't not found.");
        }
        
        $valueOptions = [];
        $element      =  $this->get($name);
        
        foreach ($element->getValueOptions() as $id => $description) {
            $valueOptions[$id] = [
                'value' => $id,
                'label' => is_array($description) ? $description['label'] : $description,
                'selected' => false,
            ];
        }
        
        foreach ($data as $id) {
            $valueOptions[$id]['selected'] = true;
        }
        
        $element->setOptions(['value_options' => $valueOptions]);
    }

    /**
     *
     * @name         addStaticText
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addStaticText(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => '\TwbBundle\Form\Element\StaticElement',
            'attributes' => array(
                'id'          => isset($options['id']) ? $options['id'] :$options['name'],
                'value'       => isset($options['value']) ? $options['value'] : null,
                'placeholder' => isset($options['placeholder']) ? $options['placeholder'] : null,
                'style'       => isset($options['style']) ? $options['style'] : null,
                'class'       => isset($options['class']) ? $options['class'] : null,
            ),
            'options' => array(
                'help-block' => isset($options['help-block']) ? $options['help-block'] : null,
                'label' => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
            ),
        ));

        return $this;
    }
    
    /**
     *
     * @name         addText
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addText(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\Text',
            
            'attributes' => array(
                'id'          => isset($options['id']) ? $options['id'] :$options['name'],
                'value'       => isset($options['value']) ? $options['value'] : null,
                'placeholder' => isset($options['placeholder']) ? $options['placeholder'] : null,
                'style'       => isset($options['style']) ? $options['style'] : null,
                'class'       => isset($options['class']) ? $options['class'] : 'form-control',
                'onblur'      => isset($options['onblur']) ? $options['onblur'] : null,
                'data-parsley-required'  => isset($options['required']) ? $options['required'] : null,
            ),
            'options' => array(
                'use_hidden_element' => true,
                'help-block' => isset($options['help-block']) ? $options['help-block'] : null,
                'label' => isset($options['label']) ? $options['label'] : null,
                'use_hidden_element' => true,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
                
            ),
        ));

        return $this;
    }

    /**
     *
     * @name         addPassword
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addPassword(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\Password',
            'attributes' => array(
                'id'          => isset($options['id']) ? $options['id'] :$options['name'],
                'value'       => isset($options['value']) ? $options['value'] : null,
                'placeholder' => isset($options['placeholder']) ? $options['placeholder'] : null,
                'style'       => isset($options['style']) ? $options['style'] : null,
                'class'       => isset($options['class']) ? $options['class'] : null,
            ),
            'options'    => array(
                'help-block' => isset($options['help-block']) ? $options['help-block'] : null,
                'label' => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
            ),
        ));

        return $this;
    }

    /**
     *
     * @name         addSelect
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addSelect(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
           
            'type'       => 'Zend\Form\Element\Select',
            'attributes' => array(
                'required' => isset($options['required']) ? $options['required'] : true,
                'id'       => isset($options['id']) ? $options['id'] :$options['name'],
                'multiple' => isset($options['multiple']) ? 'multiple' : null,
                'style'    => isset($options['style']) ? $options['style'] : null,
                'class'    => isset($options['class']) ? $options['class'] : 'form-control',
                'onchange' => isset($options['onchange']) ? $options['onchange'] : null,
            ),
            'options'    => array(
                'help-block' => isset($options['help-block']) ? $options['help-block'] : null,
                'label'         => isset($options['label']) ? $options['label'] : null,
                'value_options' => isset($options['value_options']) ? $options['value_options'] : array(),
                'empty_option' => isset($options['empty_option']) ? $options['empty_option'] : null,
                'unselected_value' => 0,
                'use_hidden_element' => true,
                'disable_inarray_validator' => true,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
            ),
        ));
        
        if ($options['name'] == 'fk_id_classificacao') {
            //$select = $this->get($options['name']);
        
        
        //$select->getValidatorChain()->addValidator();
        
        //$espec = $select->getInputSpecification();
        }

       
        return $this;
    }

    /**
     *
     * @name         addMultiCheckbox
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addMultiCheckbox(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'type'       => 'Zend\Form\Element\MultiCheckbox',
            'name'       => $options['name'],
            'attributes' => array(
                'id' => isset($options['id']) ? $options['id'] :$options['name'],
            ),
            'options'    => array(
                'label'              => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
                'use_hidden_element' => true,
                'value_options'      => isset($options['value_options']) ? $options['value_options'] : array()
            )
        ));

        return $this;
    }

    /**
     *
     * @name         Checkbox
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addCheckbox(array $options = array())
    {
        $attributes = [];
        
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }
        
        if (isset($options['data_attributes']) && is_array($options['data_attributes'])) {
            foreach ($options['data_attributes'] as $name => $value) {
                $attributes[$name] = $value;
            }
        }

        $attributes['id'] = isset($options['id']) ? $options['id'] :$options['name'];
        $attributes['required'] = isset($options['required']) ? $options['required'] : false;
        $attributes['onchange'] = isset($options['onchange']) ? $options['onchange'] : null;
                
        $this->add(array(
            'type'       => 'Zend\Form\Element\Checkbox',
            'name'       => $options['name'],
            'attributes' => $attributes,
            'options'    => array(
                'label'              => isset($options['label']) ? $options['label'] : null,
                'column-size'        => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes'   => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
                'use_hidden_element' => true,
                'unchecked_value'    => isset($options['unchecked_value']) ? $options['unchecked_value'] : 0,
                'checked_value'      => isset($options['checked_value']) ? $options['checked_value'] : 1,
            )
        ));
        
        return $this;
    }
    
    /**
     *
     * @name         addRadio
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addRadio(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'type'       => 'Zend\Form\Element\Radio',
            'name'       => $options['name'],
            'attributes' => array(
                'id' => isset($options['id']) ? $options['id'] :$options['name'],
                
            ),
            'options'    => array(
                'label'              => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : 'sm-10',
                'label_attributes' => isset($options['label_attributes']) ? $options['label_attributes'] : ['class' => 'col-sm-2'],
                'use_hidden_element' => true,
                'value_options'      => isset($options['value_options']) ? $options['value_options'] : array()
            )
        ));

        return $this;
    }
    
    /**
     *
     * @name         addTextarea
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addTextarea(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'id'    => isset($options['id']) ? $options['id'] :$options['name'],
                'class' => isset($options['class']) ? $options['class'] : null,
                'style' => isset($options['style']) ? $options['style'] : null,
                'rows'  => isset($options['rows']) ? $options['rows'] : 5,
                'data-parsley-required'  => isset($options['required']) ? $options['required'] : null,
            ),
            'options'    => array(
                'label' => isset($options['label']) ? $options['label'] : null,
            ),
        ));

        return $this;
    }

    /**
     *
     * @name         addFile
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addFile(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\File',
            'attributes' => array(
                'id'    => isset($options['id']) ? $options['id'] :$options['name'],
                'class' => isset($options['class']) ? $options['class'] : null,
                'style' => isset($options['style']) ? $options['style'] : null,
            ),
            'options'    => array(
                'label' => isset($options['label']) ? $options['label'] : null,
            ),
        ));

        return $this;
    }

    /**
     *
     * @name         addSubmit
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addSubmit(array $options = array())
    {
        $this->add(array(
            'name'       => isset($options['name']) ? $options['name'] : 'submit',
            'type'       => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => isset($options['value']) ? $options['value'] : 'Salvar',
                'id'    => isset($options['name']) ? $options['name'] : 'submit',
                'class' => isset($options['class']) ? $options['class'] : null,
                'style' => isset($options['style']) ? $options['style'] : null,
                'type' => 'submit',
                'onclick' => isset($options['onclick']) ? $options['onclick'] : null,
            ),
            'options'    => array(
                'label' => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : null,
            ),
        ));

        return $this;
    }

    /**
     *
     * @name         addButton
     *
     * @param  array $options = array()
     *
     * @access public
     * @return $this
     */
    public function addButton(array $options = array())
    {
        if (!isset($options['name'])) {
            throw new Exception("Option ['name'] is not present");
        }

        $this->add(array(
            'name'       => $options['name'],
            'type'       => 'Zend\Form\Element\Button',
            'attributes' => array(
                'id'      => isset($options['id']) ? $options['id'] :$options['name'],
                'class'   => isset($options['class']) ? $options['class'] : null,
                'style'   => isset($options['style']) ? $options['style'] : null,
                'onclick' => isset($options['onclick']) ? $options['onclick'] : null,
            ),
            'options'    => array(
                'label' => isset($options['label']) ? $options['label'] : null,
                'column-size' => isset($options['column-size']) ? $options['column-size'] : null,
            ),
        ));

        return $this;
    }



    /**
     *
     * @name dispatchErrorMessages
     * @access public
     * @return void
     */
    public function dispatchErrorMessages()
    {
        if ($this->getMessages()) {
            echo '<div class="alert alert-danger alert-dismissable fade in"">' .
                '<button class="close" data-dismiss="alert">×</button>' .
                '<i class="fa-fw fa-lg fa fa-times"></i>';

            echo 'Por favor, corrija os erros encontrados no formulário.<br /><br />';

            foreach ($this->getMessages() as $field => $message) {
                echo '<p>';
                echo '<b>' . $this->get($field)->getLabel() . '</b><br />';
                echo '<ul>' . join('<br />', $message) . '</ul>';
                echo '</p>';
            }

            echo '</div>';
        }
    }

    /**
     * Retrieve the validated data
     *
     * By default, retrieves normalized values; pass one of the
     * FormInterface::VALUES_* constants to shape the behavior.
     *
     * @param  int $flag
     *
     * @return array|object
     * @throws Exception\DomainException
     */
    public function getData($flag = \Zend\Form\FormInterface::VALUES_NORMALIZED)
    {
        $data = parent::getData($flag);

        if (isset($data['salvar'])) {
            unset($data['salvar']);
        }

        if (isset($data['cancelar'])) {
            unset($data['cancelar']);
        }

        if (isset($data['submit'])) {
            unset($data['submit']);
        }

        if (isset($data['cancel'])) {
            unset($data['cancel']);
        }

        if (isset($data['reset'])) {
            unset($data['reset']);
        }

        return $data;
    }

    public function getInputFilterSpecification()
    {
        $specification = [];
        $elements = $this->getElements();
        
        foreach ($elements as $name => $element) {
            if ($element->hasAttribute('required')) {
                $required = $element->getAttribute('required');
                $specification[$name] = [
                    'required' => $required,
                ];
            }
        }
        
        return $specification;
    }
}
