<?php

namespace Core\Form;

use \Exception;
use Core\Form\BaseForm;

/**
 *
 * @name       BaseFormFilter
 * @category   Core
 * @package    Core
 * @subpackage CoreForm
 */
class BaseFormFilter extends BaseForm
{

    /**
     *
     * @name   __construct
     * @param  array $options
     * @access public
     * @return void
     */
    public function __construct(array $options = array())
    {
        parent::__construct($options);

        $this->setAttribute('class', 'form-inline');
    }
    
    protected function addBooleanSelect()
    {
        $this->addSelect([
            'name' => 'esta_ativo',
            'required'=> false,
            'class'=>'form-control select2_allow_clear',
            'label' => 'Está Ativo?',
            'column-size' => 'sm-3',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'value_options' => [
                1 => 'Sim',
                0 => 'Não',
            ],
            'empty_option' => 'Todos',
            'help-block' => 'Será a opção pessoa',
        ]);
    }
}
