<?php
/**
 * Service
 */
namespace Core\Stdlib;

use Zend\Stdlib\Parameters as ZendStdlibParameters;
use Zend\Db\Sql\Select;
use Core\Service\ExceptionService;
use \DateTime;

/**
 * Description of Parameters
 *
 * @author Elton
 */
abstract class Parameters extends ZendStdlibParameters
{
    /**
     *
     */
    protected $parameters;
    
    protected $mergedParameters = [];

    /**
     *  Search types constants
     */
    const SEARCH_TYPE_LIKE   = 'LIKE';
    const SEARCH_TYPE_EQUAL  = '=';
    const SEARCH_TYPE_IN     = 'IN';
    const SEARCH_TYPE_NOT_IN = 'NOT IN';
    const SEARCH_TYPE_GREATER_OR_EQUAL_THAN = '>=';
    const SEARCH_TYPE_LESS_OR_EQUAL_THAN = '<=';
    const SEARCH_TYPE_BETWEEN = 'BETWEEN';

    /**
     *  Types constants
     */
    const TYPE_STRING = 'string';
    const TYPE_INT = 'int';
    const TYPE_FLOAT = 'float';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DATE = 'date';
    const TYPE_DATE_TIME = 'date_time';
    
    /**
     *
     * @param array $values
     */
    public function __construct(array $values = null)
    {
        parent::__construct($values);
    }
    
    /**
     *
     * @name __get
     * @param type $name
     * @return type
     */
    public function __get($name)
    {
        $parameters = $this->getParameters();
        
        if (isset($parameters[$name])) {
            return $parameters[$name]['value'];
        }
        
        return null;
    }
    
    /**
     *
     * @name __set
     * @param type $name
     * @param type $value
     * @return Core\Stdlib\Parameters
     */
    public function __set($name, $value)
    {
        if (isset($this->parameters[$name])) {
            $this->parameters[$name]['value'] = $value;
        }
        
        if (isset($this->parameters[$name]) === false) {
            $this->parameters[$name] = [
                'type' => self::TYPE_STRING,
                'value' => $value,
                'search_type' => self::SEARCH_TYPE_EQUAL,
            ];
        }
        
        return $this;
    }
    
    /**
     *
     * @name getParameters
     * @return array
     */
    public function getParameters()
    {
        return $this->parameters;
    }

    /**
     *
     * @name getParameters
     * @return array
     */
    public function getMergedParameters()
    {
        return $this->mergedParameters;
    }
    
    /**
     *
     * @name mergeParameters
     * @param array $parameters
     * @return Core\Stdlib\Parameters
     */
    public function mergeParameters(Parameters $parameters)
    {
        $this->mergedParameters[$this->getModelName()] = $this->parameters;
        $this->mergedParameters[$parameters->getModelName()] = $parameters->getParameters();
                
        return $this;
    }
    
    public static function getModelName()
    {
         return null ;
     }

    /**
     *
     * @name setConfigParameters
     * @param array $parameters
     * @return Core\Stdlib\Parameters
     */
    public function setConfigParameters(array $parameters)
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     *
     * @name setConfigMergedParameters
     * @param array $parameters
     * @return Core\Stdlib\Parameters
     */
    public function setConfigMergedParameters(array $parameters)
    {
        $this->mergedParameters = $parameters;
        return $this;
    }    
    
    public function setParametersToIgnoreInSql(array $options)
    {   
        foreach ($options as $option) {
            if(isset($option['model_name'])) {
                foreach ($option['parameters'] as $parameter) {
                    $this->mergedParameters[$option['model_name']][$parameter]['use_in_sql'] = false;
                }
            }
            
            if(isset($option['model_name']) === false) {
                foreach ($option['parameters'] as $parameter) {
                    $this->parameters[$parameter]['use_in_sql'] = false;
                }
            }
        }
     
        return $this;
    }
  
    /**
     *
     * @name setParametersValues
     * @param array $parametersValues
     * @return Core\Stdlib\Parameters
     */
    public function setParametersValues(array $parametersValues)
    {
        $parametersValues = isset($parametersValues) ? $parametersValues : [];
        $parametersValues = isset($parametersValues['filters']) ? $parametersValues['filters'] : $parametersValues;
        $mergedParameters = $this->getMergedParameters();
        $this->cleanParametersValues();
        
        if ($mergedParameters == false) {
            foreach ($parametersValues as $key => $value) {

                if (isset($this->parameters[$key]) && ($value != null || is_numeric($value))) {
                    $this->parameters[$key]['value'] = $value;
                }
            }            
        }
                
        if ($mergedParameters) {
            foreach ($mergedParameters as $modelName => $parametersModel) {
                foreach ($parametersValues as $key => $value) {

                    if (isset($parametersModel[$key]) && ($value != null || is_numeric($value))) {
                        $this->mergedParameters[$modelName][$key]['value'] = $value;
                    }
                }                 
            }
        }
        
        return $this;
    }
    
    private function cleanParametersValues()
    {
        $mergedParameters = $this->getMergedParameters();
        
        if ($mergedParameters == false) {
            foreach ($this->getParameters() as $key => $config) {
                $this->parameters[$key]['value'] = null;
            }            
        }
                
        if ($mergedParameters) {
            foreach ($mergedParameters as $modelName => $parametersModel) {
                foreach ($parametersModel as $key => $config) {
                    $this->mergedParameters[$modelName][$key]['value'] = null;
                }
                
            }
        }
        
        return $this;        
    }


    /**
     *
     * @name setValuesInSelect
     * @param array $options
     * @return Core\Stdlib\Parameters
     */
    
    public function setValuesInSelect(array $options)
    {
        $exceptionService = $this->serviceManager->get(ExceptionService::class);
        
        $models = isset($options['models']) && is_array($options['models']) ? $options['models'] : [];
        
        if (isset($options['parameters'])) {
            $this->setParametersValues($options['parameters']);
        }
        
        if (! isset($options['select']) || ! $options['select'] instanceof Select) {
            $exceptionService->throwMandatoryConditionException('The index select was not found or is not a ' . Select::class . ' instance');
        }
        
        $select = $options['select'];
        
        foreach ($models as $model) {
            $modelColumns = $model['model']->getColumns();
            $modelName    = $model['model']->getClassName();
            $alias        = $model['alias'];
            $parameters   = $this->getMergedParameters() ? $this->getMergedParameters()[$modelName] : $this->parameters;
            
            foreach ($parameters as $key => $config) {
                $value = $this->getValueByType($config);
                $useInSql = isset($config['use_in_sql']) ? $config['use_in_sql'] : true;
                $columnName = isset($config['column_name']) ? $config['column_name'] : $key;
                $columnsName = isset($config['columns_name']) ? $config['columns_name'] : [];                
                $areAllColumnsValid = true;
                array_map(function($value) use ($modelColumns, &$areAllColumnsValid){
                    if(in_array($value, $modelColumns) === false || $areAllColumnsValid === false){
                        $areAllColumnsValid = false;
                        return;
                    }

                }, $columnsName);
        
                $isValidColumn = (in_array($columnName, $modelColumns) || $areAllColumnsValid)
                        && $value !== null;
                
                if($useInSql == false) {
                    continue;
                }
                
                if ($isValidColumn) {
                    switch ($config['search_type']) {
                        case self::SEARCH_TYPE_EQUAL:
                            $select->where($alias . "." . $columnName . " = '" . $value . "'");
                            break;
                        
                        case self::SEARCH_TYPE_LIKE:
                            $select->where($alias . "." . $columnName . "   LIKE '%" . $value . "%'");
                            break;
                        
                        case self::SEARCH_TYPE_IN:
                            $select->where($alias . "." . $columnName . " IN(" . $value . ")");
                            break;
                        
                        case self::SEARCH_TYPE_NOT_IN:
                            $select->where($alias . "." . $columnName . " NOT IN(" . $value . ")");
                            break;
                        
                        case self::SEARCH_TYPE_GREATER_OR_EQUAL_THAN:
                            $select->where($alias . "." . $columnName . " >= '" . $value . "'");
                            break;

                        case self::SEARCH_TYPE_LESS_OR_EQUAL_THAN:
                            $select->where($alias . "." . $columnName . " <= '" . $value . "'");
                            break;

                        case self::SEARCH_TYPE_BETWEEN:
                            $begin = $columnsName['begin'];
                            $end   = $columnsName['end'];
                            $select->where("'". $value . "' BETWEEN " . $alias . "." . $begin . ' AND ' . $alias . "." .   $end);
                            break;
                        
                        default:
                            $select->where($alias . "." . $columnName . " = '" . $value . "'");
                    }
                }
            }
        }
        
        return $this;
    }
    
    /**
     *
     * @name getValueByType
     * @param array $config
     * @return string|int|float
     */
    private function getValueByType(array $config)
    {
        $value = null;
        
        switch ($config['type']) {
            case self::TYPE_BOOLEAN:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = $value === null ? $value : (int) $value;
                break;

            case self::TYPE_FLOAT:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = $value === null ? $value : (float) $value;
                break;

            case self::TYPE_INT:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = $value === null ? $value : (int) $value;
                break;

            case self::TYPE_STRING:
                $value = is_array($config['value']) ? join("','", $config['value']) : $config['value'];
                $value = is_array($config['value']) ? "'" . $value . "'"  : $value;
                $value = $value === null ? $value : (string) $value;
                
                break;

            case self::TYPE_DATE:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = is_array($config['value']) ? "'" . $value . "'"  : $value;
                $value = $value === null ? $value : (string) $value;
                
                if($value != null) {
                    $format = 'd/m/Y';
                    
                    $date  = DateTime::createFromFormat($format, $value);
                    $value = $date->format('Y-m-d'); 
                }
                
                break;

            case self::TYPE_DATE_TIME:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = is_array($config['value']) ? "'" . $value . "'"  : $value;
                $value = $value === null ? $value : (string) $value;
                break;

            default:
                $value = is_array($config['value']) ? join(',', $config['value']) : $config['value'];
                $value = is_array($config['value']) ? "'" . $value . "'"  : $value;
                $value = $value === null ? $value : (string) $value;
                break;
        }
        
        return $value;
    }
}
