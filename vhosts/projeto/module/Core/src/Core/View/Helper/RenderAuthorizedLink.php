<?php

namespace Core\View\Helper;

use Core\Service\AutorizacaoService;
use Zend\Form\View\Helper\AbstractHelper;

class RenderAuthorizedLink extends AbstractHelper
{
    /**
     *
     * @var object
     */
    private $serviceManager = null;

    /**
     *
     * @param type $options
     * @return \Core\View\Helper\RenderAuthorizedLink
     */
    public function __invoke($options)
    {
        if (!$options) {
            return $this;
        }
        
        return $this->render($options);
    }

    /**
     *
     * @param array $options
     * @return string
     */
    public function render(array $options)
    {
        $this->serviceManager = $this->getView()->getHelperPluginManager()->getServiceLocator();
        $autorizacaoService   = $this->serviceManager->get(AutorizacaoService::class);
        
        $showLinkIfUnauthorized = isset($options['showLinkIfUnauthorized']) ? $options['showLinkIfUnauthorized'] : true;
        $href                   = null;
        $link                   = isset($options['link']) ? $options['link'] : null;
        $type                   = isset($options['type']) ? $options['type'] : 'save';
        $class                  = isset($options['class']) ? $options['class'] : null;
        $id                     = isset($options['id']) ? $options['id'] : null;
        $onclick                = isset($options['onclick']) ? $options['onclick'] : null;
        $titleButton            = isset($options['title_button']) ? $options['title_button'] : null ;
        $separatedLink          = explode('/', $link);
        $module                 = ucfirst($separatedLink[1]);
        $controller             = ucfirst($separatedLink[2]);
                
        if (strpos($separatedLink[1], '-')) {
            $module = null;
            
            foreach (explode('-', $separatedLink[1]) as $value) {
                $module .= ucfirst($value);
            }
        }
        
        if (strpos($separatedLink[2], '-')) {
            $controller = null;
            
            foreach (explode('-', $separatedLink[2]) as $value) {
                $controller .= ucfirst($value);
            }
        }
        
        $route = [
            'module'     => $separatedLink[1],
            'controller' => ucfirst($module) . "\\Controller\\" . ucfirst($controller),
            'action'     => $separatedLink[3],
        ];
        
        $isAuthorized = $autorizacaoService->isAuthorized($route);
        
        if ($isAuthorized === false && $showLinkIfUnauthorized == false) {
            return null;
        }
        
        if ($type === 'new') {
            $href      = $link;
            $class     = $class ? $class : 'btn btn-warning';
            $class     = $isAuthorized ? $class : $class . ' disabled';
            $iconClass = 'fa fa-save';
            $title     = $titleButton? $titleButton : 'Novo';
            
            return '<a id="' . $id . '"  title="' . $title . '" href="' . $href . '" class="' . $class . '"><i class="' . $iconClass . '"></i> ' . $title . '</a>';
        }
        
        if ($type === 'edit') {
            $href      = $link;
            $class     = $class ? $class : 'btn btn-success btn-xl';
            $class     = $isAuthorized ? $class : $class . ' disabled';
            $iconClass = 'glyphicon glyphicon-edit';
            $title     = 'Editar';
            
            return '<a id="' . $id . '" style="margin-left: 5px;" title="' . $title . '" href="' . $href . '" class="' . $class . '"><i class="' . $iconClass . '"></i></a>';
        }
        
        if ($type === 'view') {
            $href      = '#myModal';
            $class     = $class ? $class : 'btn btn-primary btn-xl';
            $class     = $isAuthorized ? $class : $class . ' disabled';
            $iconClass = 'glyphicon glyphicon-eye-open';
            $title     = 'Visualizar';
            $onClick   = $onclick ? $onclick : "renderViewOnModal('" . $link . "')";
            
            return '<a id="' . $id . '" style="margin-left: 5px;" title="' . $title . '" data-toggle="modal" href="' . $href . '" onclick="' . $onClick . '" class="' . $class . '"><i class="' . $iconClass . '"></i></a>';
        }
        
        if ($type === 'delete') {
            $href      = '#';
            $class     = $class ? $class : 'btn btn-danger btn-xl';
            $class     = $isAuthorized ? $class : $class . ' disabled';
            $iconClass = 'glyphicon glyphicon-trash';
            $title     = 'Excluir';
            $onClick   = $onclick ? $onclick : "confirmModal('Você confirma a exclusão?', '" . $link . "'); return false;";

            return '<a id="' . $id . '" style="margin-left: 5px;" title="' . $title . '" href="' . $href . '" onclick="' . $onClick . '" class="' . $class . '"><i class="' . $iconClass . '"></i></a>';
        }
        
        return null;
    }
}
