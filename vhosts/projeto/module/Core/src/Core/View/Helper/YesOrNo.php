<?php

namespace Core\View\Helper;

use Zend\Form\View\Helper\AbstractHelper;

class YesOrNo extends AbstractHelper
{
    /**
     *
     * @var object
     */
    private $serviceManager = null;

    /**
     *
     * @param type $options
     * @return \Core\View\Helper\YesOrNo
     */
    public function __invoke($options)
    {
        if (!$options) {
            return $this;
        }
        
        return $this->render($options);
    }

    /**
     *
     * @param array $options
     * @return string
     */
    public function render(array $options)
    {
        $this->serviceManager = $this->getView()->getHelperPluginManager()->getServiceLocator();
        
        
        $value= isset($options['value']) ? $options['value'] : 0;
      
        return $value ? '<span class="label label-success">Sim</span>' : '<span class="label label-danger">Não</span>';
    }
}
