<?php

namespace Core\Db;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Adapter\Adapter;

/**
 * Factory to build a DbAdapter
 *
 * @category   Core
 * @package    Db
 */
class AdapterInformationSchemaServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Configuration');
        $mvcEvent = $serviceLocator->get('Application')->getMvcEvent();
        if ($mvcEvent) {
            $routeMatch = $mvcEvent->getRouteMatch();
            $moduleName = $routeMatch->getParam('module');

            if ($moduleName) {
                $includeFile = dirname($_SERVER['DOCUMENT_ROOT']) . '/module/' . ucfirst($moduleName) . '/config/module.config.php';
            }

            if (!$moduleName) {
                $includeFile = dirname($_SERVER['DOCUMENT_ROOT']) . '/module/core/config/module.config.php';
            }

            if (isset($moduleConfig['db_information_schema'])) {
                $config['db_information_schema'] = $moduleConfig['db_information_schema'];
            }
        }

        return new Adapter($config['db_information_schema']);
    }
}
