<?php

namespace Core\Db;

use Core\Model\Entity;
use Core\Model\EntityException;
use Exception;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Exception\InvalidArgumentException;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\ServiceManager\ServiceManager;
use Core\Service\ExceptionService;

/**
 * Class TableGateway
 *
 * @package Core\Db
 */
class TableGateway extends AbstractTableGateway
{
    /**
     * Primary Key field name
     *
     * @var string
     */
    protected $primaryKeyField;

    /**
     * ObjectPrototype
     *
     * @var stdClass
     */
    protected $objectPrototype;
    
    private $exceptionService;
    
    private $serviceManager;

    public function __construct(Adapter $adapter, $table, $objectPrototype, ServiceManager $serviceManager = null)
    {
        $this->serviceManager     = $serviceManager;
        $this->exceptionService   = $this->serviceManager->get(ExceptionService::class);
        $this->adapter            = $adapter;
        $this->table              = $objectPrototype->getTableName();
        $this->objectPrototype    = $objectPrototype;
        $this->resultSetPrototype = new ResultSet();
        $this->resultSetPrototype->setArrayObjectPrototype($objectPrototype);
    }


    public function initialize()
    {
        parent::initialize();

        $this->primaryKeyField = $this->objectPrototype->primaryKeyField;
        if (!is_string($this->primaryKeyField)) {
            $this->primaryKeyField = 'id';
        }
    }

    public function fetchAll($columns = null, $where = null, $limit = null, $offset = null)
    {
        $select = new Select();
        $select->from($this->getTable());

        if ($columns) {
            $select->columns($columns);
        }

        if ($where) {
            $select->where($where);
        }

        if ($limit) {
            $select->limit((int)$limit);
        }

        if ($offset) {
            $select->offset((int)$offset);
        }

        return $this->selectWith($select);
    }

    public function get($id)
    {
        $id     = (int)$id;
        $rowset = $this->select(array($this->primaryKeyField => $id));
        $row    = $rowset->current();
        if (!$row) {
            //throw new EntityException("Could not find row $id");
            $this->exceptionService->throwNotFoundException('Não foi possível encontrar o registro de id igual a ' . $id);
        }

        return $row;
    }

    /**
     * @param $object
     *
     * @return mixed
     * @throws EntityException
     * @throws Exception
     */
    public function save($object)
    {
        if ($object->isAValidModel() === false) {
            throw new InvalidArgumentException('This is an invalid model');
        }

        $data = $object->getData();
        $id   = (int) isset($data[$this->primaryKeyField]) ? $data[$this->primaryKeyField] : 0;

        try {
            if ($id == 0) {
                $statusInsert = $this->insert($data);

                if ($statusInsert < 1) {
                    throw new EntityException("Erro ao inserir", 1);
                }

                $object->id                       = $this->lastInsertValue;
                $object->{$this->primaryKeyField} = $this->lastInsertValue;
                $object->wasMadeAnInsert          = true;
                $object->wasMadeAnUpdate          = false;
            }

            if ($id) {
                if (!$this->get($id)) {
                    throw new EntityException('Id does not exist');
                }

                try {
                    if (isset($data['id'])) {
                        unset($data['id']);
                    }

                    $this->update($data, array($this->primaryKeyField => $id));
                    $object->wasMadeAnInsert = false;
                    $object->wasMadeAnUpdate = true;
                } catch (Exception $exception) {
                    throw $exception;
                }
            }

            return $object;
        } catch (Exception $exception) {
            throw new EntityException($exception->getMessage());
        }
    }

    public function delete($where)
    {
        if (is_array($where)) {
            return parent::delete($where);
        }

        if (is_int($where)) {
            return parent::delete(array($this->primaryKeyField => $where));
        }

        if ($where instanceof Entity) {
            return parent::delete(array($this->primaryKeyField => $where->getPrimaryKeyValue()));
        }

        return parent::delete($where);
    }
    
    /**
     *
     * @param array $options
     */
    public function getAllTablesWhereThisPrimaryKeyColumnIsBeingUsed(array $options)
    {
        $informationSchemaKeyColumnUsage = 'KEY_COLUMN_USAGE';
        $referencedTableName             = isset($options['referencedTableName']) ? $options['referencedTableName'] : $this->getTable();
        $referencedColumnName            = isset($options['referencedColumnName']) ? $options['referencedColumnName'] : null;
        $tableSchema                     = isset($options['tableSchema']) ? $options['tableSchema'] : null;
        
        $sql    = new Sql($this->adapter);
        $select = new Select();
        $select->from(['kcu' => $informationSchemaKeyColumnUsage])
                ->columns(['CONSTRAINT_NAME', 'REFERENCED_TABLE_NAME', 'REFERENCED_COLUMN_NAME', 'TABLE_NAME', 'COLUMN_NAME'])
                ->where("REFERENCED_TABLE_NAME = '" . $referencedTableName . "'")
                ->where("REFERENCED_COLUMN_NAME = '" . $referencedColumnName . "'")
                ->where("TABLE_SCHEMA = '" . $tableSchema . "'");
        
        $query = $sql->prepareStatementForSqlObject($select)->execute();
        
        $result = [];

        foreach ($query as $data) {
            $result[] = $data;
        }

        return $result;
    }
}
