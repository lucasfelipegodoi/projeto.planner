<?php
namespace Core\Validator;

use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;
use \DateTime;

class GreaterThanDate extends AbstractValidator
{
    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'min' => 'min',
        'specific_name_date' => 'specific_name_date',
    ];

    /**
     * Minimum value
     *
     * @var mixed
     */
    protected $min;
    protected $specific_name_date;

    /**
     * Whether to do inclusive comparisons, allowing equivalence to min
     *
     * If false, then strict comparisons are done, and the value may equal
     * the min option
     *
     * @var bool
     */
    protected $inclusive;

    /**
     * Sets validator options
     *
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($options = null)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (!is_array($options)) {
            $options = func_get_args();
            $temp['min'] = array_shift($options);
            $temp['specific_name_date'] = array_shift($options);

            if (!empty($options)) {
                $temp['inclusive'] = array_shift($options);
            }

            $options = $temp;
        }

        if (!array_key_exists('min', $options)) {
            throw new Exception\InvalidArgumentException("Missing option 'min'");
        }
        
        if (!array_key_exists('specific_name_date', $options)) {
            throw new Exception\InvalidArgumentException("Missing option 'specific_name_date'");
        }

        if (!array_key_exists('inclusive', $options)) {
            $options['inclusive'] = false;
        }

        $this->setMin($options['min'])
             ->setSpecific_name_date($options['specific_name_date'])
             ->setInclusive($options['inclusive']);

        parent::__construct($options);
    }

    /**
     * Returns the min option
     *
     * @return mixed
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Sets the min option
     *
     * @param  mixed $min
     * @return GreaterThan Provides a fluent interface
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }
    
    
      /**
     * Returns the min option
     *
     * @return mixed
     */
    public function getSpecific_name_date()
    {
        return $this->specific_name_date;
    }

    /**
     * Sets the min option
     *
     * @param  mixed $specific_name_date
     * @return GreaterThan Provides a fluent interface
     */
    public function setSpecific_name_date($specific_name_date)
    {
        $this->specific_name_date = $specific_name_date;
        return $this;
    }

    /**
     * Returns the inclusive option
     *
     * @return bool
     */
    public function getInclusive()
    {
        return $this->inclusive;
    }

    /**
     * Sets the inclusive option
     *
     * @param  bool $inclusive
     * @return GreaterThan Provides a fluent interface
     */
    public function setInclusive($inclusive)
    {
        $this->inclusive = $inclusive;
        return $this;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($data)
    {
        $dataMinimaPermitida = $this->min;
        $dataASerComparada   = $data ;
        
        if(strlen($dataMinimaPermitida) == 10 && strlen($dataASerComparada ) == 10){
            if ($this->min) {
                $dataMinimaPadraoBr = strpos($this->min, '/');

                if ($dataMinimaPadraoBr == true) {
                    $dataMinimaPermitida =  $this->normalizarAsDatas(['de' => 'd/m/Y', 'para' => 'Y-m-d', 'data' => $this->min]);
                }

                $dataASerComparadaPadraoBr = strpos($data, '/');

                if ($dataASerComparadaPadraoBr == true) {
                    $dataASerComparada =  $this->normalizarAsDatas(['de' => 'd/m/Y', 'para' => 'Y-m-d', 'data' => $data]);
                }

                if (strtotime($dataMinimaPermitida) > strtotime($dataASerComparada)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Returns an array of messages that explain why the most recent isValid()
     * call returned false.
     * The array keys are validation failure message identifiers,
     * and the array values are the corresponding human-readable message strings.
     *
     * If isValid() was never called or if the most recent isValid() call
     * returned true, then this method returns an empty array.
     *
     * @return array
     */
    public function getMessages()
    {
        return ["A data precisa ser maior ou igual que a  ".$this->specific_name_date . ' ('.$this->min.'). '];
    }
    
    public function normalizarAsDatas(array $params = [])
    {
        $data = DateTime::createFromFormat($params['de'], $params['data']);
        return $data->format($params['para']);
    }
}
