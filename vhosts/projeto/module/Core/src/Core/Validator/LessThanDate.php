<?php
namespace Core\Validator;

use Traversable;
use Zend\Stdlib\ArrayUtils;
use Zend\Validator\AbstractValidator;
use \DateTime;

class LessThanDate extends AbstractValidator
{
    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'max' => 'max'
    ];

    /**
     * Maximum value
     *
     * @var mixed
     */
    protected $max;

    /**
     * Whether to do inclusive comparisons, allowing equivalence to max
     *
     * If false, then strict comparisons are done, and the value may equal
     * the max option
     *
     * @var bool
     */
    protected $inclusive;

    /**
     * Sets validator options
     *
     * @param  array|Traversable $options
     * @throws Exception\InvalidArgumentException
     */
    public function __construct($options = null)
    {
        if ($options instanceof Traversable) {
            $options = ArrayUtils::iteratorToArray($options);
        }
        if (!is_array($options)) {
            $options = func_get_args();
            $temp['max'] = array_shift($options);

            if (!empty($options)) {
                $temp['inclusive'] = array_shift($options);
            }

            $options = $temp;
        }

        if (!array_key_exists('max', $options)) {
            throw new Exception\InvalidArgumentException("Missing option 'max'");
        }

        if (!array_key_exists('inclusive', $options)) {
            $options['inclusive'] = false;
        }

        $this->setMax($options['max'])
             ->setInclusive($options['inclusive']);

        parent::__construct($options);
    }

    /**
     * Returns the max option
     *
     * @return mixed
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Sets the max option
     *
     * @param  mixed $max
     * @return LessThan Provides a fluent interface
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    /**
     * Returns the inclusive option
     *
     * @return bool
     */
    public function getInclusive()
    {
        return $this->inclusive;
    }

    /**
     * Sets the inclusive option
     *
     * @param  bool $inclusive
     * @return LessThan Provides a fluent interface
     */
    public function setInclusive($inclusive)
    {
        $this->inclusive = $inclusive;
        return $this;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($data)
    {
        $dataMaximaPermitida = $this->max;
        $dataASerComparada   = $data ;
        
        if (strlen($dataMaximaPermitida) == 10 && strlen($dataASerComparada) == 10) {
            if ($this->max) {
                $dataMaximaPadraoBr = strpos($this->max, '/');

                if ($dataMaximaPadraoBr == true) {
                    $dataMaximaPermitida =  $this->normalizarAsDatas(['de' => 'd/m/Y', 'para' => 'Y-m-d', 'data' => $this->max]);
                }

                $dataASerComparadaPadraoBr = strpos($data, '/');

                if ($dataASerComparadaPadraoBr == true) {
                    $dataASerComparada =  $this->normalizarAsDatas(['de' => 'd/m/Y', 'para' => 'Y-m-d', 'data' => $data]);
                }

                if (strtotime($dataMaximaPermitida) < strtotime($dataASerComparada)) {
                    return false;
                }
            }
        }
        
        return true;
    }

    /**
     * Returns an array of messages that explain why the most recent isValid()
     * call returned false.
     * The array keys are validation failure message identifiers,
     * and the array values are the corresponding human-readable message strings.
     *
     * If isValid() was never called or if the most recent isValid() call
     * returned true, then this method returns an empty array.
     *
     * @return array
     */
    public function getMessages()
    {
        return ["A data precisa ser menor ou igual a  " . $this->max];
    }
    
    public function normalizarAsDatas(array $params = [])
    {
        $data = DateTime::createFromFormat($params['de'], $params['data']);
        return $data->format($params['para']);
    }
}
