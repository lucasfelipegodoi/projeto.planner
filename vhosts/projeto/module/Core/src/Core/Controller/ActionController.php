<?php
namespace Core\Controller;

use Core\Db\TableGateway;
use Exception;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Core\Exception\BusinessRuleException;
use Core\Service\AutorizacaoService;
use Core\Service\AutenticacaoService;
use Core\Test\TestCase;

class ActionController extends AbstractActionController
{
    /**
     * Action called if is a forbidden action
     *
     * @return array
     */
    public function forbiddenAction()
    {
        return $this->redirect()->toUrl('/admin/autenticacao/forbidden');
    }
    
    /**
     * Execute the request
     *
     * @param  MvcEvent $e
     * @return mixed
     * @throws Exception\DomainException
     */
    
    public function onDispatch(MvcEvent $e)
    {
        $autorizacaoService  = $this->serviceLocator->get(AutorizacaoService::class);
        $autenticacaoService = $this->serviceLocator->get(AutenticacaoService::class);

        $routeMatch = $e->getRouteMatch();
        $params     = $routeMatch->getParams();

        if (!$routeMatch) {
            /**
             * @todo Determine requirements for when route match is missing.
             *       Potentially allow pulling directly from request metadata?
             */
            throw new Exception\DomainException('Missing route matches; unsure how to retrieve action');
        }

        $action = $routeMatch->getParam('action', 'not-found');
        $method = static::getMethodFromAction($action);

        if (!method_exists($this, $method)) {
            $method = 'notFoundAction';
        }

        $route = [
            'module'     => isset($params['module']) ? $params['module'] : null,
            'controller' => isset($params['controller']) ? $params['controller'] : null,
            'action'     => isset($params['action']) ? $params['action'] : null,
        ];

        $isApigilityRequest   = substr_count($route['controller'], 'ZF\Apigility\Admin\Controller') && $route['module'] === null;
        $isAPublicRoute       = ($route['controller'] == 'Admin\Controller\Autenticacao'
                                    || $route['controller'] == 'User\Controller\User') ||
                                $isApigilityRequest;
        $isTestingEnvironment = isset($params['is_testing_environment']) ? $params['is_testing_environment'] : false;
        $checkIfHasIdentity   = isset($params['check_if_has_identity']) ? $params['check_if_has_identity'] : true;
        $checkIsAuthorized    = isset($params['check_is_authorized']) ? $params['check_is_authorized'] : true;

        $ignoreIdentityAndAuthorized = $checkIfHasIdentity === false &&
                $checkIsAuthorized === false &&
                $isTestingEnvironment &&
                $params['token_testing'] == TestCase::TOKEN_TESTING;

        if ($ignoreIdentityAndAuthorized) {
            $actionResponse = $this->$method();
            $e->setResult($actionResponse);
            return $actionResponse;
        }

        if ($autenticacaoService->hasIdentity() === true) {
            if ($isAPublicRoute === false && $autorizacaoService->isAuthorized($route) === false) {
                return $this->forbiddenAction();
            }
        } else {
            if ($isAPublicRoute === false) {
                return $this->redirect()->toUrl('/admin/autenticacao/index');
            }
        }

        $actionResponse = $this->$method();
        $e->setResult($actionResponse);
        return $actionResponse;
    }

    /**
     *
     * @name   getParamsFromRoute
     * @access protected
     * @return array
     */
    protected function getParamsFromRoute()
    {
        $params = $this->getRequest()->isPost() ? $this->getRequest()->getPost() : $this->getRequest()->getQuery();
        $page = $this->params()->fromRoute('page', 1);

        if ($this->getRequest()->isPost()) {
            $page = 1;
        }

        return array(
            'filters' => $params->toArray(),
            'parameters' => $params,
            'page'    => $page,
        );
    }

    /**
     * Returns a TableGateway
     *
     * @param  string $table
     *
     * @return TableGateway
     */
    protected function getTable($table)
    {
        $sm           = $this->getServiceLocator();
        $dbAdapter    = $sm->get('DbAdapter');
        $tableGateway = new TableGateway($dbAdapter, $table, new $table);
        $tableGateway->initialize();

        return $tableGateway;
    }

    /**
     * Returns a Service
     *
     * @param  string $service
     *
     * @return Service
     */
    protected function getService($service)
    {
        return $this->getServiceLocator()->get($service);
    }

    /**
     * @name             addExceptionMessage
     * @access protected
     *
     * @param  Exception $exception
     *
     * @return void
     */
    protected function addExceptionMessage(Exception $exception)
    {
        if ($exception instanceof BusinessRuleException) {
            $this->addErrorMessage($exception->getMessage());
            return 1;
        }
            
            
        if (defined('APPLICATION_ENV') && (APPLICATION_ENV == 'production' || APPLICATION_ENV == 'staging')) {
            //Aqui serão incluídas as rotinas de inclusão de tarefas no Phabricator
                        
            
            
            $mensagem = '<b>Atenção!</b><br />' .
                    'Houve um erro na aplicação.<br />' .
                    'Por favor, tente executar a ação novamente.<br />';
            
            $this->addErrorMessage($mensagem);
        } else {
            $this->addErrorMessage($exception->getMessage());
        }
    }

    /**
     * @name          addSuccessMessage
     * @access protected
     *
     * @param  string $message
     *
     * @return void
     */
    protected function addSuccessMessage($message)
    {
        self::flashMessenger()->addMessage(array('success' => $message));
    }

    /**
     * @name          addInfoMessage
     * @access protected
     *
     * @param  string $message
     *
     * @return void
     */
    protected function addInfoMessage($message)
    {
        self::flashMessenger()->addMessage(array('info' => $message));
    }

    /**
     * @name          addWarningMessage
     * @access protected
     *
     * @param  string $message
     *
     * @return void
     */
    protected function addWarningMessage($message)
    {
        self::flashMessenger()->addMessage(array('warning' => $message));
    }

    /**
     * @name          addErrorMessage
     * @access protected
     *
     * @param  string $message
     *
     * @return void
     */
    protected function addErrorMessage($message)
    {
        self::flashMessenger()->addMessage(array('danger' => $message));
    }
}
