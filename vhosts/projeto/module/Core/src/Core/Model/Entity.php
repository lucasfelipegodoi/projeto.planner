<?php

/**
 * Entity class
 *
 * @package    Core\Model
 * @author     Elton Minetto<eminetto@coderockr.com>
 */

namespace Core\Model;

use \Exception;
use Zend\Form\Form;
use Zend\InputFilter\Exception\InvalidArgumentException;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\Validator\Db\NoRecordExists;
use Zend\ServiceManager\ServiceManager;
use Zend\Db\Adapter\Adapter as DbAdapter;
use Core\Filter\ToIntIfNotEmpty;

abstract class Entity implements InputFilterAwareInterface
{

    /**
     * Primary Key field name
     *
     * @var string
     */
    protected $primaryKeyField = 'id';

    /**
     * The table name at the database
     *
     * @var string
     */
    protected $tableName;

    /**
     * Filters
     *
     * @var InputFilter
     */
    protected $inputFilter = null;

    /**
     * Filters
     *
     * @var Factory
     */
    protected $factory = null;
    
    /**
     *
     * @var type
     */
    protected $serviceManager;
    
    /**
     *
     * @var type
     */
    protected $wasMadeAnInsert = null;
    
    /**
     *
     * @var type
     */
    protected $wasMadeAnUpdate = null;
    
    
    /**
     * Foi utilizado esse valor porque definimos que na base de dados
     * iríamos usar um decimal com os seguintes limites: decimal(12,2)
     *
     * @var decimal
     */
    const MAX_POSITIVE_DECIMAL = 9999999999.99;
    
    
    const MIN_POSITIVE_INTEGER = 0;

    /**
     *
     * @name        __construct
     *
     * @param array $data = array()
     *
     * @return void
     */
    public function __construct(array $data = array())
    {
        if (isset($data['serviceManager']) === false || $data['serviceManager'] instanceof ServiceManager === false) {
            throw new Exception("The wasn't found");
        }
        
        $this->serviceManager = $data['serviceManager'];
    }
    
    
    /**
     * @name executeDependentRoutines
     * @param array|object $data
     */
    protected function executeDependentRoutines($data)
    {
        $data = $this->normalizeDataSource($data);
        $this->getInputFilter($data);
        $isUpdate = isset($data[$this->primaryKeyField]) && $data[$this->primaryKeyField];
        
        if ($isUpdate === true) {
            $this->inputFilter->get($this->primaryKeyField)->setRequired(true);
            unset($data['cadastrado_em']);
            $this->alterado_em = date('Y-m-d H:i:s');
        }
        
        if ($isUpdate === false) {
            unset($data['alterado_em']);
            $this->cadastrado_em = date('Y-m-d H:i:s');
        }
    }
    
    /**
     * Set and validate field values
     *
     * @param string $key
     * @param string $value
     *
     * @return void
     */
    public function __set($key, $value)
    {
        if ($key === 'wasMadeAnInsert' || $key === 'wasMadeAnUpdate') {
            $this->$key = $value;
        }
        
        $this->$key = $this->valid($key, $value);

        if ($key === $this->primaryKeyField) {
            $this->id = $value;
        }
    }

    /**
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->$key;
    }

    /**
     * Override __toString
     *
     * @name   __toString
     * @access public
     * @return null|string
     */
    public function __toString()
    {
        $string = null;
        $data   = $this->getData();

        foreach ($data as $key => $value) {
            $string .= ucfirst($key) . ' : ' . $value . '<br />';
        }

        return $string;
    }


    /**
     * Return de class name
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    abstract public function getClassName();

    /**
     *
     * @name   createInputFilterFactory
     * @access public
     * @return Core\Model\Entity
     */
    public function createInputFilterFactory()
    {
        $this->inputFilter = null;
        $this->factory     = null;

        $this->inputFilter = new InputFilter();
        $this->factory     = new InputFactory();

        return $this;
    }
    
    /**
     *
     * @name getValidatorsFromInputFilter
     *
     * @param type $inputFilterName
     */
    public function getValidatorsFromInputFilter($inputFilterName)
    {
        $inputFilter = $this->inputFilter->get($inputFilterName);
        return $inputFilter->getValidatorChain()->getValidators();
    }
    
    /**
     * @name getValidatorFromInputFilter
     *
     * @param type $inputFilterName
     * @param type $validatorNameSpace
     */
    public function getValidatorFromInputFilter($inputFilterName, $validatorNameSpace)
    {
        foreach ($this->getValidatorsFromInputFilter($inputFilterName) as $configs) {
            foreach ($configs as $validator) {
                if ($validator instanceof $validatorNameSpace) {
                    return $validator;
                }
            }
        }
    }

    /**
     * @param array $options
     */
    public function inputFilterAdd(array $options)
    {
        $this->inputFilter->add($this->factory->createInput(array(
            'name'       => isset($options['name']) ? $options['name'] : null,
            'required'   => isset($options['required']) ? $options['required'] : false,
            'filters'    => isset($options['filters']) ? $options['filters'] : $this->getStringFilters(),
            'validators' => isset($options['validators']) ? $options['validators'] : array(),
        )));
    }
    
    private function addInputFilterBaseDate(array $options)
    {
        if (!$options['name']) {
            $this->throwMandatoryConditionException('Informe o nome');
        }
        
        if (isset($options['validators']) && is_array($options['validators'])) {
            $validators = array_merge($validators, $options['validators']);
        }
        
        $this->inputFilterAdd([
            'name' => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] : true,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => isset($options['min']) ? $options['min'] : 19,
                    'max' => isset($options['max']) ? $options['max'] : 19,
                ]),
            ],
        ]);
        
        return $this;
    }
    
    protected function addInputFilterDateTime(array $options)
    {
        $options['min'] = 19;
        $options['max'] = 19;
        $this->addInputFilterBaseDate($options);
    }
    
    protected function addInputFilterDate(array $options)
    {
        $options['min'] = 10;
        $options['max'] = 10;
        $this->addInputFilterBaseDate($options);
    }
    
    protected function addInputFilterTime(array $options)
    {
        $options['min'] = 8;
        $options['max'] = 9;
        $this->addInputFilterBaseDate($options);
    }
    
    protected function addInputFilterToInsertAndUpdate()
    {
        $this->addInputFilterDateTime([
           'name' => 'cadastrado_em',
           'required' => false,
        ]);
        
        $this->addInputFilterDateTime([
           'name' => 'alterado_em',
           'required' => false,
        ]);
        
        return $this;
    }
    
    protected function addInputFilterPrimaryKey()
    {
        $this->inputFilterAdd([
            'name'       => $this->primaryKeyField,
            'required'   => false,
            'filters'    => [
                ['name' => 'ToInt'],
            ]
        ]);
        
        return $this;
    }
    
    protected function addInputFilterForeignKey(array $options)
    {
        
        $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => isset ($options['required']) ? $options['required'] : true ,
            'filters'    => [$this->serviceManager->get(ToIntIfNotEmpty::class)], 
        ]);
        
        return $this;
    }
    

    protected function addAllInputFilterForeignKey()
    {
        foreach ($this->getModelVars() as $key => $value) {
            if (strpos($key, 'fk_id_') !== false) {
                $this->addInputFilterForeignKey([
                   'name' => $key,
                ]);
            }
        }
        
        return $this;
    }

    protected function addInputFilterBoolean(array $options)
    {
        if (!$options['name']) {
                $this->throwMandatoryConditionException('Informe o nome');
        }
        
        $this->inputFilterAdd([
            'name' => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] : true,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getInArrayValidator([
                    'haystack' => [0, 1]
                ]),
            ],
        ]);
        
        return $this;
    }

    protected function addInputFilterFloat(array $options)
    {
        $this->inputFilterAdd([
            'name' => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] : false,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' =>[
                $this->getLessThanValidator([
                    'max' => self::MAX_POSITIVE_DECIMAL,
                    'inclusive' => true,
                    'message' => "O valor não pode ser maior que '%max%'",
                ]),
                $this->getGreaterThanValidator([
                    'min' => self::MIN_POSITIVE_INTEGER,
                    'inclusive' => true,
                    'message' => "O valor não pode ser menor que '%min%'",
                ])
            ],
        ]);
        
        return $this;
    }
    
    protected function addInputFilterEmail(array $options)
    {
        $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] : true,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                
                $this->getEmailAddressValidator(),
                $this->getStringLengthValidator([
                    'min' => 1,
                    'max' => 255,
                ]),
            ],
        ]);
        
        return $this;
    }
    
    protected function addInputFilterLogin(array $options)
    {
        $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] : true,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getRegexValidator([
                    'pattern' =>  '/^[\w.]*$/',
                    'message' => 'São aceitos somente números, letras e pontos (.)'
                ]),
                $this->getStringLengthValidator([
                    'min' => 2,
                    'max' => 255,
                ]),
            ],
        ]);
        
        return $this;
    }
    
    protected function addInputFilterLessThan(array $options)
    {
        $this->inputFilterAdd([
            'name' => $options['name'],
            'required' => isset($options['required']) ? $options['required'] : true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => 10,
                    'max' => 10,
                ]),
                $this->getLessThanDateValidator([
                'max' => $options['max'],
                ]),
            ],
        ]);
    }
    
    
     protected function addInputFilterGreaterThan(array $options)
    {
        $this->inputFilterAdd([
            'name' => $options['name'],
            'required' => isset($options['required']) ? $options['required'] : true,
            'filters' => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => 10,
                    'max' => 10,
                ]),
                $this->getGreaterThanDateValidator([
                'min' => $options['min'],
                'specific_name_date' => $options['specific_name_date'],
                ]),
            ],
        ]);
    }
    
    protected function addInputFilterBaseText(array $options)
    {
         $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => $options['required'],
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => isset($options['min']) ? $options['min'] : 1,
                    'max' => isset($options['max']) ? $options['max'] : 255,
                ]),
            ],
         ]);
         
      
         return $this;
    }
    
    protected function addInputFilterSmallText(array $options)
    {
        $options['min'] = 2;
        $options['max'] = 25;
        $this->addInputFilterBaseText($options);
        
        return $this;
    }
    
    protected function addInputFilterMediumText(array $options)
    {
        $options['min'] = 2;
        $options['max'] = 255;
        $this->addInputFilterBaseText($options);
        
        return $this;
    }
    
    protected function addInputFilterLargeText(array $options)
    {
        $options['min'] = 2;
        $options['max'] = 8000;
        $this->addInputFilterBaseText($options);
        
        return $this;
    }
    
    protected function addInputFilterExtraLargeText(array $options)
    {
        $options['min'] = 2;
        $options['max'] = 60000;
        $this->addInputFilterBaseText($options);
        
        return $this;
    }
    
    protected function addInputFilterForDigits(array $options)
    {
         $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => $options['required'],
            'filters'    => [
                ['name' => 'Digits'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => isset($options['min']) ? $options['min'] : 1,
                    'max' => isset($options['max']) ? $options['max'] : 255,
                ]),
            ],
         ]);
         
      
         return $this;
    }
    
    protected function addInputFilterWithNoRecordExists(array $options)
    {
        $data = isset($options['data']) ? $options['data'] : [];
        
        $filters = [
            ['name' => 'StripTags'],
            ['name' => 'StringTrim'],
        ];
        
        $validators = [
            $this->getNoRecordExistsValidator([
                'table' => $options['table'],
                'field' => $options['field'],
                'dbAdapter' => $this->serviceManager->get(DbAdapter::class),
                'message' => 'Esse valor já está sendo utilizado.<br /> ',
                'exclude' => [
                    'field' => $options['field_exclude'],
                    'value' => isset($data[$options['field_exclude']]) ? $data[$options['field_exclude']] : null,
                ],
            ]),
        ];
        
        if (isset($options['filters']) && is_array($options['filters'])) {
            $filters = array_merge($filters, $options['filters']);
        }

        if (isset($options['validators']) && is_array($options['validators'])) {
            $validators = array_merge($validators, $options['validators']);
        }
        
        $this->inputFilterAdd([
            'name'       => $options['name'],
            'required'   => isset($options['required']) ? $options['required'] :true,
            'filters'    => $filters,
            'validators' => $validators
        ]);
        
        return $this;
    }
    
    /**
     * Get table name
     *
     * @name getTableName
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     *
     *
     * @name  setPrimaryKeyValue
     *
     * @param $value
     *
     * @return $this
     */
    public function setPrimaryKeyValue($value)
    {
        $this->getInputFilter([$this->primaryKeyField => $value]);
        self::__set($this->primaryKeyField, $value);
        return $this;
    }

    /**
     * Set all entity data based in an array with data
     *
     * @name         setData
     *
     * @param  mixed $data
     *
     * @access public
     * @return void
     */
    public function setData($data)
    {
        $columns = $this->getColumns();

        $normalizedData = $this->normalizeDataSource($data);

        foreach ($normalizedData as $key => $value) {
            if (in_array($key, $columns)) {
                $this->__set($key, $value);
            }
        }
    }

    /**
     *
     * @name   getData
     * @access public
     * @return array
     */
    public function getData()
    {
        $fields = $this->getModelVars();
        $data   = [];
        array_walk($fields, function ($value, $key) use (&$data) {
            
            if ($value === null) {
                unset($data[$key]);

                return $value;
            }
            
            if ($value === '') {
                $data[$key] = null;

                return $value;
            }

            $data[$key] = $value;

            return $value;
        });

        return $data;
    }

    /**
     *
     *
     * @name                         normalizeDataSource
     *
     * @param  Form|Parameters|array $dataSource
     *
     * @access public
     * @return array
     */
    public function normalizeDataSource($dataSource)
    {
        if ($dataSource instanceof Form) {
            return $dataSource->getData()->toArray();
        }

        if ($dataSource instanceof Parameters) {
            return $dataSource->toArray();
        }

        if ($dataSource instanceof Entity) {
            return $dataSource->toArray();
        }

        if (is_array($dataSource)) {
            return $dataSource;
        }
    }

    /**
     *
     * @name   getColumns
     * @access public
     * @return array
     */
    public function getColumns()
    {
        $columns = array_keys(self::getModelVars());

        return $columns;
    }

    /**
     * Used by TableGateway
     *
     * @name         exchangeArray
     *
     * @param  array $data
     *
     * @access public
     * @return void
     */
    public function exchangeArray($data)
    {
        $this->setData($data);
    }

    /**
     * Used by TableGateway
     *
     * @name         getArrayCopy
     *
     * @param  array $data
     *
     * @access public
     * @return array
     */
    public function getArrayCopy()
    {
        return $this->getData();
    }

    /**
     *
     * @name                        setInputFilter
     *
     * @param  InputFilterInterface $inputFilter
     *
     * @access public
     * @return void
     */
    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new EntityException("Not used");
    }

    /**
     *
     * @name         getBetweenValidator
     *
     * @param  array $options = array()
     *
     * @access public
     * @return type array
     */
    public function getBetweenValidator(array $options = array())
    {
        $min = isset($options['min']) ? $options['min'] : -PHP_INT_MAX ;
        $max = isset($options['max']) ? $options['max'] : PHP_INT_MAX;
        
        return array(
            'name'    => 'Between',
            'options' => array(
                'messages' => array(
                    'notBetween' => 'O valor não está dentro do intervalo [' .
                        $min . '] e [' . $max . ']',
                ),
                'min'      => $min,
                'max'      => $max,
            ),
        );
    }
    
    public function getInArrayValidator(array $options = [])
    {
        $haystack = isset($options['haystack']) ? $options['haystack'] : [] ;
        
        return array(
            'name'    => 'InArray',
            'options' => array(
                'messages' => array(
                    'notInArray' => 'O valor não está dentro da seguinte lista [' .
                        join(',  ', $haystack) . ']',
                ),
                'haystack' => $haystack,
            ),
        );
    }

    public function getCnpjValidator(array $options = array())
    {
        return array(
            'name' => 'Core\Validator\Cnpj',
            'options' => [],
        );
    }
    
    public function getToNotEmptyFilter()
    {
        return array(
            'name' => 'Core\Filters\ToIntNotEmpty',
           
        );
    }
    

    public function getCpfValidator(array $options = array())
    {
        return array(
            'name' => 'Core\Validator\Cpf',
            'options' => [],
        );
    }
    
    
    public function getLessThanDateValidator(array $options = array())
    {
        return array(
            'name' => 'Core\Validator\LessThanDate',
            'options' => [
                'max' => isset($options['max']) ? $options['max'] : null,
            ],
        );
    }
    
    
    public function getGreaterThanDateValidator(array $options = array())
    {
        return array(
            'name' => 'Core\Validator\GreaterThanDate',
            'options' => [
                'min' => isset($options['min']) ? $options['min'] : null,
                'specific_name_date' => isset($options['specific_name_date']) ? $options['specific_name_date'] : null,
            ],
        );
    }
    /**
     *
     * @name   getEmailAddressValidator
     * @access public
     * @return type array
     */
    public function getEmailAddressValidator()
    {
        return array(
            'name'    => 'EmailAddress',
            'options' => array(
                'useDomainCheck' => true,
            ),
        );
    }

    /**
     *
     * @name         getStringLengthValidator
     *
     * @param  array $options
     *
     * @access public
     * @return type array
     */
    public function getStringLengthValidator(array $options = array())
    {
        return array(
            'name'    => 'StringLength',
            'options' => array(
                'encoding' => 'UTF-8',
                'min'      => isset($options['min']) ? $options['min'] : 1,
                'max'      => isset($options['max']) ? $options['max'] : 255,
            ),
        );
    }
    
    /**
     *
     * @name getRegexValidator
     *
     * @param array $options
     * @return array
     */
    public function getRegexValidator(array $options = [])
    {
        return [
            'name' => 'Regex',
            'options' => [
                'pattern' => $options['pattern'],
                'message' => isset($options['message']) ? $options['message'] : 'O valor é inválido',
            ],
        ];
    }

    /**
     *  Returns the No Record Exist input filter configured.
     *
     * @name   getNoRecordExistsValidator
     * @access public
     * @return array
     */
    public function getNoRecordExistsValidator(array $options)
    {
        $optionsNoRecordExists = [
            'table'   => isset($options['table']) ? $options['table'] : null,
            'field'   => isset($options['field']) ? $options['field'] : null,
            'adapter' => isset($options['dbAdapter']) ? $options['dbAdapter'] : null,
        ];

        if (isset($options['message'])) {
            $optionsNoRecordExists['message'] = $options['message'];
        }
        
        if (isset($options['exclude']) && $options['exclude'] != null) {
            $isAValidFieldAndValue = isset($options['exclude']['field']) &&
                    $options['exclude']['field'] && isset($options['exclude']['value']) && $options['exclude']['value'];
            
            if ($isAValidFieldAndValue) {
                $optionsNoRecordExists['exclude'] = [
                    'field' => isset($options['exclude']['field']) ? $options['exclude']['field'] : null,
                    'value' => isset($options['exclude']['value']) ? $options['exclude']['value'] : null,
                ];
            }
        }
        
        return new NoRecordExists($optionsNoRecordExists);
    }
    
    
    public function getLessThanValidator(array $options)
    {
        $config = array(
            'name'    => 'LessThan',
            'options' => array(
               'max' => isset($options['max']) ? $options['max'] : null,
               'inclusive' => isset($options['inclusive']) ? $options['inclusive'] : true,
            ),
        );
        
        if (isset($options['message'])) {
            $config['options']['message'] = $options['message'];
        }
        
        return $config;
    }
     
     
    public function getGreaterThanValidator(array $options)
    {
        $min       = isset($options['min']) ? $options['min'] : null;
        $inclusive = isset($options['inclusive']) ? $options['inclusive'] : true;

        $config =  array(
            'name'    => 'GreaterThan',
            'options' => array(
               'min' => $min,
               'inclusive' => $inclusive,
            ),
        );
        
        if (isset($options['message'])) {
            $config['options']['message'] = $options['message'];
        }
        
        return $config;
    }
     
     
    /**
     *
     * @name         getStringFilters
     *
     * @param  array $options = array()
     *
     * @access public
     * @return array
     */
    public function getStringFilters(array $options = array())
    {
        if (!$options) {
            return array(
                array('name' => 'StripTags'),
                array('name' => 'StringTrim'),
            );
        }

        throw new EntityException("Not implemented");
    }

    /**
     *
     * @name         getIntFilters
     *
     * @param  array $options = array()
     *
     * @access public
     * @return array
     */
    public function getIntFilters(array $options = array())
    {
        if (!$options) {
            return array(
                array('name' => 'Int'),
            );
        }

        throw new EntityException("Not implemented");
    }

    /**
     *
     * @name         getBooleanFilters
     *
     * @param  array $options = array()
     *
     * @access public
     * @return array
     */
    public function getBooleanFilters(array $options = array())
    {
        if (!$options) {
            return array(
                array('name' => 'Boolean'),
            );
        }

        throw new EntityException("Not implemented");
    }

    /**
     *
     * @name   getInputFilter
     * @access public
     * @return null
     */
    public function getInputFilter()
    {
    }

    /**
     * Used by TableGateway
     *
     * @name   toArray
     * @access public
     * @return array
     */
    public function toArray()
    {
        return $this->getData();
    }

    /**
     *
     *
     * @name   getPrimaryKeyValue
     * @access public
     * @return int
     */
    public function getPrimaryKeyValue()
    {
        return isset($this->id) && $this->id ? $this->id : $this->{$this->primaryKeyField};
    }

    /**
     *
     *
     * @name   getPrimaryKeyField
     * @access public
     * @return string
     */
    public function getPrimaryKeyField()
    {
        return $this->primaryKeyField;
    }

    /**
     *
     *
     * @name   isUpdate
     * @access public
     * @return boolean
     */
    public function isUpdate()
    {
        return $this->wasMadeAnUpdate() === true || $this->{$this->primaryKeyField} != null;
    }

    /**
     *
     *
     * @name   isInsert
     * @access public
     * @return boolean
     */
    public function isInsert()
    {
        return $this->wasMadeAnInsert() === true || $this->{$this->primaryKeyField} == null;
    }
    
    /**
     * @name wasMadeAnUpdate
     *
     * @return boolean
     */
    public function wasMadeAnUpdate()
    {
        return $this->wasMadeAnUpdate === true;
    }
    
    /**
     * @name wasMadeAnInsert
     *
     * @return boolean
     */
    public function wasMadeAnInsert()
    {
        return $this->wasMadeAnInsert === true;
    }

    /**
     *
     *
     * @name         isValid
     *
     * @param  array $data
     *
     * @access public
     * @return boolean
     */
    public function isValid(array $data)
    {
        if (isset($data['salvar'])) {
            unset($data['salvar']);
        }

        if (isset($data['cancelar'])) {
            unset($data['cancelar']);
        }

        if (isset($data['submit'])) {
            unset($data['submit']);
        }

        if (isset($data['cancel'])) {
            unset($data['cancel']);
        }

        foreach ($data as $key => $value) {
            $this->valid($key, $value);
        }

        return true;
    }

    /**
     *
     */
    public function isAValidModel()
    {
        $modelVars = $this->getModelVars();

        try {
            return $this->isValid($modelVars);
        } catch (Exception $exception) {
            return false;
        }
    }

    
    /**
     *
     * @throws Exception
     */
    public function clearModelVars()
    {
        if (property_exists($this, $this->primaryKeyField) === false) {
            throw new Exception("The attribute 'primaryKeyField' is not defined on the model.");
        }
        
        $this->{$this->primaryKeyField} = null;
    }

    /**
     * Filter and validate data
     *
     * @name          valid
     *
     * @param  string $key
     * @param  mixed  $value
     *
     * @access protected
     * @return mixed
     */
    protected function valid($key, $value)
    {
        if (!$this->getInputFilter()) {
            return $value;
        }

        try {
            $filter = $this->getInputFilter()->get($key);
        } catch (InvalidArgumentException $e) {
            //não existe filtro para esse campo
            return $value;
        }

        $filter->setValue($value);

        if (!$filter->isValid()) {
            $messages = $filter->getMessages();
            
            if ($messages) {
                $errorMessage = "O campo <b>" . $key . "</b> contém os seguintes erros: <br />" .
                        join('<br />', $messages);
                
                throw new EntityException($errorMessage);
            } else {
                throw new EntityException("Campo inválido: <b>" . $key . "</b> =  " . $value);
            }
        }

        return $filter->getValue($key);
    }

    /**
     *
     * @name   getModelVars
     * @access private
     * @return array
     */
    private function getModelVars()
    {
        $data     = get_object_vars($this);
        $dataCopy = $data;

        if ($data['primaryKeyField'] != 'id') {
            unset($data['id']);
        }

        unset($data['factory']);
        unset($data['inputFilter']);
        unset($data['tableName']);
        unset($data['primaryKeyField']);
        unset($data['serviceManager']);
        unset($data['wasMadeAnInsert']);
        unset($data['wasMadeAnUpdate']);

        foreach ($dataCopy as $key => $value) {
            if (strripos($key, '_') === 0) {
                unset($data[$key]);
            }
        }

        return $data;
    }
}
