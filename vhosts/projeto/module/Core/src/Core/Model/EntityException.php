<?php

/**
 * @package    Core\Model
 * @author     Marcos Garcia<garcia@coderockr.com>
 */
namespace Core\Model;

use \Exception;
use Core\Exception\CoreException;

class EntityException extends CoreException
{
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function __construct($message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
