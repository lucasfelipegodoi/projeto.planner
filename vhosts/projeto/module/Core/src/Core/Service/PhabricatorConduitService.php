<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Core\Service;

use \Exception;

/**
 * Description of PhabricatorConduitService
 *
 * @author Elton
 */
class PhabricatorConduitService
{
    private $host;
    
    private $apiToken;
    
    private $projectPHIDs;
    
    private $viewPolicy;
    
    private $editPolicy;
    
    private $priorityTaskLevel;
    
    private $ownerPHID;
    
    private $ccPHIDs;
    
    private $sizeOfTask;
    
    private $estimatedHours;
    
    private $dateTime;

    private $maniphestQueryMethod = 'maniphest.query';
    
    private $maniphestCreateTaskMethod = 'maniphest.createtask';
    
    public function __construct($options)
    {
        $this->host              = $options['host'];
        $this->apiToken          = $options['apiToken'];
        $this->projectPHIDs      = $options['projectPHIDs'];
        $this->viewPolicy        = $options['viewPolicy'];
        $this->editPolicy        = $options['editPolicy'];
        $this->priorityTaskLevel = $options['priorityTaskLevel'];
        $this->ownerPHID         = $options['ownerPHID'];
        $this->ccPHIDs           = $options['ccPHIDs'];
        $this->sizeOfTask        = $options['sizeOfTask'];
        $this->estimatedHours    = $options['estimatedHours'];
        $this->dateTime          = null;
    }
    
    /**
     *
     * @name  getTaskById
     * @param int $id
     * @return array
     */
    public function getTaskById($id)
    {
    }

    /**
     *
     * @name   getTasks
     * @param  array $options
     * @return array
     */
    public function getTasks(array $options)
    {
        $tasks         = [];
        $apiParameters = [
            'projectPHIDs' => $this->projectPHIDs,
            'fullText'     => $options['fullText'],
            'status'       => $options['status'],
            'order'        => $options['order'],
            'limit'        => $options['limit'],
        ];
    }

    /**
     *
     * @name  createTask
     * @param array $options
     * @return type
     */
    public function createTask(array $options)
    {
        $this->dateTime = time();
        
        $apiParameters = [
            'title' => $options['title'],
            'description' => $options['description'],
            'viewPolicy' => $this->viewPolicy,
            'editPolicy' => $this->editPolicy,
            'priority' => $this->priorityTaskLevel,
            'projectPHIDs' => $this->projectPHIDs,
            'auxiliary' => [
              'std:maniphest:mycompany:estimated-hours' => $this->estimatedHours,
              'std:maniphest:mycompany:size-of-task' => $this->sizeOfTask,
              'std:maniphest:mycompany:start-date' => $this->dateTime,
            ],
        ];
        
        if ($this->ownerPHID) {
            $apiParameters['ownerPHID'] = $this->ownerPHID;
        }
        
        if ($this->ccPHIDs) {
            $apiParameters['ccPHIDs'] = $this->ccPHIDs;
        }
    }
}
