<?php

namespace Core\Service;

use Zend\Session\Container as SessionContainer;
use Zend\Authentication\Adapter\DbTable as AdapterDbTable;
use Zend\Authentication\AuthenticationService;
use Exception;
use stdClass;
use Admin\Model\NivelDeAcessoModel;
use Core\Service\ExceptionService;

//use Core\Service\AutorizacaoService;

/**
 * Serviço de autenticação
 *
 * @name       AutenticacaoService
 * @category   Application
 * @package    Core
 * @subpackage CoreService
 * @author     Author <e-mail>
 */
class AutenticacaoService extends Service
{

    /**
     * Serviço de autenticação do Zend
     *
     * @var \Zend\Authentication\AuthenticationService
     */
    protected $authenticationService;

    /**
     * Adaptador de autenticação
     *
     * @var \Zend\Authentication\Adapter\DbTable
     */
    protected $authenticationAdapter;

    /**
     * Container do Zend Session
     *
     * @var \Zend\Session\Container
     */
    protected $session;

    /**
     * Parâmetros de configuração
     *
     * @var array
     */
    protected $config;

    /**
     * Nome da sessão
     *
     * @var string
     */
    const AUTHENTICATION_SESSION = 'authenticationSession';

    /**
     * Variável com os dados do usuário autenticado
     *
     * @var string
     */
    const USER_DATA = 'userData';

    /**
     * Nome da variável principal da sessão
     *
     * @var string
     */
    const AUTHENTICATED_USER = 'authenticatedUser';

    /**
     * Algoritmo de hash usado para autenticação
     *
     * @var string
     */
    const ALGORITMO_DE_HASH = 'sha512';

    /**
     * Construtor
     *
     * @name         __construct
     *
     * @param  array $options
     *
     * @access public
     * @return void
     */
    public function __construct(array $options)
    {
        $this->setServiceManager($options['serviceManager']);

        if (!isset($options['config']) || !is_array($options['config'])) {
            throw new Exception("[config] is not present or is not an array");
        }

        $this->config                = $options['config'];
        $this->authenticationService = $this->getService(AuthenticationService::class);
        $this->authenticationAdapter = $this->getService(AdapterDbTable::class);
        $this->session               = $this->getService(SessionContainer::class);
    }

    /**
     *
     *
     * @name         authenticate
     *
     * @param  array $identificacao
     *
     * @access public
     * @return boolean
     */
    public function authenticate(array $identificacao = array())
    {
        $exceptionService = $this->serviceManager->get(ExceptionService::class);

        if ($this->hasIdentity()) {
            return $this;
        }

        if ($identificacao == false) {
            $exceptionService->throwInvalidParameterException('Parametros inválidos');
        }

        if ($identificacao[$this->config['identityField']] == null || $identificacao[$this->config['credentialField']] == null) {
            $exceptionService->throwMandatoryConditionException('Informe o login e senha de acesso');
        }

        if ($this->config['useHashForCredential']) {
            $identificacao[$this->config['credentialField']] = hash(
                self::ALGORITMO_DE_HASH,
                $identificacao[$this->config['credentialField']]
            );
        }

        $this->authenticationAdapter->setTableName($this->config['tableName'])
            ->setIdentityColumn($this->config['identityColumn'])
            ->setCredentialColumn($this->config['credentialColumn'])
            ->setIdentity($identificacao[$this->config['identityField']])
            ->setCredential($identificacao[$this->config['credentialField']]);

        if ($this->authenticationService->authenticate($this->authenticationAdapter)->isValid() === false) {
            $exceptionService->throwNotFoundException('Login e/ou senha inválidos');
        }

        $this->createAuthenticatedSession();

        $this->getService(AutorizacaoService::class)->setAuthorizations();

        return $this;
    }

    /**
     * @todo Implement this method
     * @return string
     */
    public function getRoleUser()
    {
        return 'admin';
    }
    
    /**
     * Retorna se o usuário está autenticado ou não
     *
     * @name   hasIdentity
     * @access public
     * @return boolean
     */
    public function hasIdentity()
    {
        return $this->authenticationService->hasIdentity();
    }

    /**
     *
     *
     * @name   getAuthenticatedSession
     * @access public
     * @return stdClass|array
     */
    public function getAuthenticatedSession($variavel = null)
    {
        if ($this->authenticationService->hasIdentity() === false) {
            return null;
        }

        if ($variavel === null) {
            return $this->session->offsetGet(self::AUTHENTICATED_USER);
        }

        $sessao = $this->session->offsetGet(self::AUTHENTICATED_USER);

        return isset($sessao[$variavel]) ? $sessao[$variavel] : null;
    }

    /**
     *
     *
     * @name   setDataOnAuthenticatedSession
     * @access public
     * @return void
     */
    public function setDataOnAuthenticatedSession($data)
    {
        $this->session->offsetSet(self::AUTHENTICATED_USER, $data);

        if ($this->session->offsetGet(self::AUTHENTICATED_USER) == false) {
            throw new Exception('Não foi possível setar os dados na sessão autenticada', 4);
        }
    }

    /**
     *
     *
     * @name   getUserData
     * @access public
     * @return stdClass
     */
    public function getUserData()
    {
        return $this->getAuthenticatedSession(self::USER_DATA);
    }

    public function getUserID()
    {
        return $this->getUserData()->ID_Usuario;
    }


    /**
     * Encerra a sessão do usuário
     *
     * @name   logout
     * @access public
     * @return boolean
     */
    public function logout()
    {
        if ($this->session->offsetGet(self::AUTHENTICATED_USER) === null) {
            return false;
        }
            
        $this->session->offsetUnset(self::AUTHENTICATED_USER);
        $this->authenticationService->clearIdentity();

        return $this->session->offsetGet(self::AUTHENTICATED_USER) === null;
    }

    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }

    /**
     *
     *
     * @name   createAuthenticatedSession
     * @access protected
     * @return void
     */
    protected function createAuthenticatedSession()
    {
        $dadosDaSessao = array(
            self::USER_DATA => $this->authenticationAdapter->getResultRowObject(),
        );

        $this->setDataOnAuthenticatedSession($dadosDaSessao);
    }
}
