<?php

namespace Core\Service;

use \Exception;
use Phinx\Wrapper\TextWrapper;
use Phinx\Console\PhinxApplication;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\StreamOutput;

/**
 * Class PdfService
 *
 * @package Core\Service
 */
class MigrationService extends Service
{
    /**
     *
     * @var type
     */
    private $phinxApplication;
    
    /**
     *
     * @var type
     */
    private $textWrapper;
    
    /**
     *
     * @var type
     */
    private $exitCode;
    
    /**
     *
     * @var type
     */
    private $configuration;
    
    /**
     *
     * @var type
     */
    private $parser;
    
    /**
     *
     * @var type
     */
    private $environment = 'development';

    /**
     * @name  __construct
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        if (isset($options['textWrapper']) === false || $options['textWrapper'] instanceof TextWrapper === false) {
            throw new Exception("The instance " . TextWrapper::class . " wasn't found.");
        }

        if (isset($options['phinxApplication']) === false || $options['phinxApplication'] instanceof PhinxApplication === false) {
            throw new Exception("The instance " . PhinxApplication::class . " wasn't found.");
        }

        if (isset($options['configuration']) === false || $options['configuration'] == false) {
            throw new Exception("The configuration wasn't found.");
        }
        
        $this->textWrapper      = $options['textWrapper'];
        $this->phinxApplication = $options['phinxApplication'];
        $this->configuration    = $options['configuration'];
        
        $this->environment      = isset($options['environment']) ? $options['environment'] : 'development';
        $this->parser           = isset($options['parser']) ? $options['parser'] : 'yaml';
    }

    /**
     *
     * @param type $environment
     */
    public function getMigrate($environment = 'development')
    {
        return $this->textWrapper->getMigrate($environment);
    }
    
    /**
     *
     * @param type $environment
     * @param type $target
     * @return type
     */
    public function getRollback($environment = 'development', $target = 0)
    {
        return $this->textWrapper->getRollback($environment, $target);
    }
    
    
    /**
     *
     * @param type $seed
     * @param type $environment
     * @return type
     */
    public function runSeed($seed = null, $environment = 'development')
    {
        try {
            $path    = realpath(__DIR__ . '/../../../../../');
            $command = $path . '/vendor/bin/phinx seed:run -e ' . $environment;

            $config = array(
                'seed:run',
                '-e' => $environment,
                '-c' => $this->configuration,
                '-p' => $this->parser
            );

            if ($seed != null) {
                $config['-s'] = $seed;
                $command .= ' -s ' . $seed;
            }

            shell_exec($command);
            //return $this->executeRun($config);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     *
     * @param type $command
     * @return type
     */
    public function runCommand($command, $environment = 'development')
    {
        $config = array(
            $command,
            '-e' => $environment,
            '-c' => $this->configuration,
            '-p' => $this->parser
        );
        
        return $this->executeRun($config);
    }

    /**
     * Execute a command, capturing output and storing the exit code.
     *
     * @param  array $command
     * @return string
     */
    private function executeRun(array $command)
    {
        // Output will be written to a temporary stream, so that it can be
        // collected after running the command.
        $stream = fopen('php://temp', 'w+');

        // Execute the command, capturing the output in the temporary stream
        // and storing the exit code for debugging purposes.
        $this->exitCode = $this->phinxApplication->doRun(new ArrayInput($command), new StreamOutput($stream));

        // Get the output of the command and close the stream, which will
        // destroy the temporary file.
        $result = stream_get_contents($stream, -1, 0);
        fclose($stream);

        return $result;
    }
}
