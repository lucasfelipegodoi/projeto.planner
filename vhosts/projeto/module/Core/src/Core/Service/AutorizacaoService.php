<?php

namespace Core\Service;

use Zend\Session\Container as SessionContainer;
use \Exception;
use Core\Service\Service;
use Core\Acl\Builder as AclBuilder;
use Core\Service\AutenticacaoService;

/**
 * Serviço de autorização
 *
 * @name       AutorizacaoService
 * @category   Application
 * @package    Admin
 * @subpackage AdminService
 * @author
 */
class AutorizacaoService extends Service
{

    /**
     * Array com as permissões do sistema
     * @var array
     */
    protected $authorizationsSource;

    /**
     * Classe controladora da ACL de autorização
     * @var \Core\Acl\Builder
     */
    protected $aclBuilder;

    /**
     * Container de sessão do Zend
     * @var \Zend\Session\Container
     */
    protected $session;

    /**
     * Serviço de autenticação da EFGH
     * @var \Admin\Service\AutenticacaoService
     */
    protected $autenticacaoService;

    /**
     * Variável com a configurção da ACL
     * @var static
     */
    const NOME_DA_VARIAVEL_DE_ACL = 'acl';

    /**
     *
     *
     * @name   __construct
     * @param  array $options
     * @access public
     * @throws Exception
     * @return void
     */
    public function __construct(array $options)
    {
        $this->setServiceManager($options['serviceManager']);

        $this->aclBuilder = $this->getService(AclBuilder::class);
        $this->session = $this->getService(SessionContainer::class);
        $this->autenticacaoService = $this->getService(AutenticacaoService::class);
    }

    /**
     *
     *
     * @name   setSourceAuthorizations
     * @param  string $source
     * @access public
     * @return void
     */
    public function setSourceAuthorizations($source)
    {
        $this->authorizationsSource = $source;
    }

    /**
     *
     *
     * @name   isAuthorized
     * @param  array $route
     * @access public
     * @return boolean
     */
    public function isAuthorized(array $route)
    {
        if (!$this->autenticacaoService->hasIdentity()) {
            return false;
        }

        $role = $this->autenticacaoService->getRoleUser();
        $resource = $route['controller'] . '.' . $route['action'];
        $aclConfiguration = $this->getAuthorizations();

        if (!$aclConfiguration) {
            return false;
        }

        try {
            return $this->aclBuilder->build($aclConfiguration)->isAllowed($role, $resource);
        } catch (Exception $excecao) {
            return false;
        }
    }

    /**
     * Seta as autorizações do usuário
     *
     * @name   setAuthorizations
     * @access public
     * @return void
     */
    public function setAuthorizations()
    {
        $sessaoAutenticada = $this->autenticacaoService->getAuthenticatedSession();
        $role = $this->autenticacaoService->getRoleUser();
        $acl = array(
        'acl' => array(
        'roles' => array(
            $role => null,
        ),
        'resources' => array(
        ),
        'privilege' => array(
            $role => array(
            'allow' => array(
            ),
            ),
        ),
        ),
        );

        foreach ($this->authorizationsSource['privilege'][$role]['allow'] as $privilege) {
            $acl['acl']['resources'][] = $privilege;
            $acl['acl']['privilege'][$role]['allow'][] = $privilege;
        }

        $sessaoAutenticada[self::NOME_DA_VARIAVEL_DE_ACL] = $acl;
        $this->autenticacaoService->setDataOnAuthenticatedSession($sessaoAutenticada);
    }

    /**
     *
     *
     * @name   getAuthorizations
     * @access public
     * @return array
     */
    public function getAuthorizations()
    {
        $autorizations = $this->autenticacaoService->getAuthenticatedSession(self::NOME_DA_VARIAVEL_DE_ACL);
    
        return array_key_exists(self::NOME_DA_VARIAVEL_DE_ACL, $autorizations) ?
        array(self::NOME_DA_VARIAVEL_DE_ACL => $autorizations[self::NOME_DA_VARIAVEL_DE_ACL]) : null;
    }

    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
