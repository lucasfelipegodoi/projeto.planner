<?php

namespace Core\Service;

use Zend\View\Model\ViewModel;
use AcMailer\Service\MailService as AcMailerMailService;
use AcMailer\Result\MailResult;
use Exception;

/**
 * Class MailService
 *
 * @package Core\Service
 */
class MailService extends Service
{
    /**
     * @var
     */
    protected $mailService;

    /**
     * @var
     */
    protected $zendMailMessage;

    /**
     * @var
     */
    protected $viewlModel;
    
    /**
     *
     * @var type
     */
    protected $executeRealSend;

    /**
     * Namespace completo da classe
     *
     * @var const
     */
    const CLASS_NAME = __CLASS__;

    /**
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = array())
    {
        if (isset($options['mailService']) === false ||
            $options['mailService'] instanceof AcMailerMailService === false
        ) {
            throw new Exception("An instance of 'AcMailerMailService' wasn't found.");
        }

        if (isset($options['viewModel']) === false || $options['viewModel'] instanceof ViewModel === false) {
            throw new Exception("An instance of 'Zend\View\Model\ViewModel' wasn't found.");
        }

        $this->executeRealSend = isset($options['executeRealSend']) ? $options['executeRealSend'] : true;
        $this->mailService     = $options['mailService'];
        $this->zendMailMessage = $this->mailService->getMessage();
        $this->setTemplate($options['viewModel']);
    }

    /**
     * @return mixed
     */
    public function send()
    {
        if ($this->executeRealSend) {
            return $this->mailService->send();
        }
        
        $options = [
            'valid' => true,
            'message' => MailResult::DEFAULT_MESSAGE,
        ];
        
        return new MailResult($options['valid'], $options['message']);
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->mailService->getMessage();
    }

    /**
     * @return mixed
     */
    public function getVariables()
    {
        return $this->viewlModel->getVariables();
    }

    /**
     * @param array $variables
     *
     * @return $this
     */
    public function setVariables(array $variables)
    {
        $this->viewlModel->setVariables($variables);
        $this->setTemplate($this->viewlModel);
        return $this;
    }

    /**
     * @param $address
     *
     * @return $this
     */
    public function addTo($address)
    {
        $this->zendMailMessage->setTo($address);
        return $this;
    }

    /**
     * @param $address
     *
     * @return $this
     */
    public function addCC($address)
    {
        $this->zendMailMessage->setCC($address);
        return $this;
    }

    /**
     * @param $address
     *
     * @return $this
     */
    public function addBcc($address)
    {
        $this->zendMailMessage->setBcc($address);
        return $this;
    }

    /**
     * @param $subject
     *
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->mailService->setSubject($subject);
        return $this;
    }

    /**
     * @param $template
     *
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->viewlModel  = $template;
        $this->mailService->setTemplate($this->viewlModel);
        return $this;
    }


    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::CLASS_NAME;
    }
}
