<?php

namespace Core\Service;

use \SebastianBergmann\Exporter\Exception;
use \PHPExcel;
use \PHPExcel_IOFactory;
use \PHPExcel_Worksheet;
use Zend\Mime\Part as MimePart;

class ExcelService extends Service
{

    /**
     * ExcelService
     * @var spreadsheet
     */
    protected $spreadsheet;

    protected $activeSheet;

    /**
     * Namespace completo da classe
     * @var const
     */
    const CLASS_NAME = __CLASS__;

    /**
     *
     * @name   __construct
     * @param  array $options = array()
     * @access public
     * @return void
     */
    public function __construct(array $options = array())
    {
        if (isset($options['phpExcel']) && $options['phpExcel'] instanceof PHPExcel) {
            $this->spreadsheet = $options['phpExcel'];
        }

        $this->activeSheet = 0;
    }

    /**
     * @param $id
     */
    public function setActiveSheetIndex($id)
    {
        $this->activeSheet = $id;
        return $this;
    }

    /**
     * @param array $options
     * @param bool  $tituloColuna
     *
     * @throws \SebastianBergmann\Exporter\Exception
     */
    public function setData(array $options, $tituloColuna = true)
    {
        if (isset($options['dados']) == false) {
            throw new Exception("Posicao dados é obrigatória");
        }

        if (isset($options['cell']) == false) {
            $options['cell'] = 'A1';
        }

        if ($tituloColuna) {
            $colunas = array_keys($options['dados'][0]);
            array_unshift($options['dados'], $colunas);
        }

        $this->spreadsheet
            ->setActiveSheetIndex($this->activeSheet);

        $this->spreadsheet->getActiveSheet()
            ->fromArray($options['dados'], null, $options['cell']);
    }

    /**
     * @param $title
     * @param $indexSheet
     */
    public function setTitleSheet($title, $indexSheet)
    {
        $this->spreadsheet->setActiveSheetIndex($indexSheet);
        $this->spreadsheet->getActiveSheet()->setTitle($title);
        $this->spreadsheet->setActiveSheetIndex($this->activeSheet);
    }

    /**
     * @param $nome
     */
    public function createSheet($nome)
    {
        $myWorkSheet = new PHPExcel_Worksheet($this->spreadsheet, $nome);
        $this->spreadsheet->addSheet($myWorkSheet, $this->getSheetCount()+1);
        $this->setActiveSheetIndex($this->getSheetCount()-1);
    }

    /**
     *
     */
    public function removeSheet()
    {
        $this->spreadsheet->removeSheetByIndex(0);
    }

    /**
     * @return mixed
     */
    public function getSheetCount()
    {
        return $this->spreadsheet->getSheetCount();
    }

    /**
     * @param $fileName
     */
    public function downloadExcel($fileName)
    {
        $objWriter = PHPExcel_IOFactory::createWriter($this->spreadsheet, 'Excel5');
        header('Content-type: application/vnd.ms-excel');
        header('Content-Disposition: attachment; filename="'.$fileName.'.xls"');
        $objWriter->save('php://output');
    }

    /**
     *
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::CLASS_NAME;
    }
}
