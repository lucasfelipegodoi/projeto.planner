<?php

namespace Core\Service;

use \DOMPDF;
use \Exception;

/**
 * Class PdfService
 *
 * @package Core\Service
 */
class PdfService extends Service
{
    protected $domPdf;

    /**
     * @name  __construct
     * @param array $options
     */
    public function __construct(array $options = array())
    {
        if (isset($options['domPdf']) && $options['domPdf'] instanceof DOMPDF) {
            $this->domPdf = $options['domPdf'];
        }
    }

    /**
     * @name  generatePdf
     * @param array $options
     */
    public function generatePdf(array $options)
    {
        $html = isset($options['html']) && empty($options['html']) === false ? $options['html'] : null;
        $fileName = isset($options['name']) ? $options['name'] : 'index.pdf';
        
        if ($html === null) {
            throw new Exception("The index 'html' is empty.");
        }
        
        $this->domPdf->load_html($options['html']);
        $this->domPdf->render();
        $this->domPdf->stream($fileName);
    }
}
