<?php

namespace Core\Service;

use \Exception;
use Core\Exception\InvalidIntersectionException;
use Core\Exception\PreexistingConditionException;
use Core\Exception\InvalidPeriodException;
use Core\Exception\InvalidDeleteException;
use Core\Exception\InvalidUpdateException;
use Core\Exception\SendingEmailException;
use Core\Exception\MandatoryConditionException;
use Core\Exception\NotFoundException;
use Core\Exception\InvalidParameterException;
use Core\Exception\InvalidStatusException;
use Core\Exception\ForbiddenException;

/**
 * Class PdfService
 *
 * @package Core\Service
 */
class ExceptionService
{
    /**
     * @name  __construct
     * @param array $options
     */
    public function __construct()
    {
    }

    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwForbiddenException($message = null, $code = 0, Exception $previous = null)
    {
        throw new ForbiddenException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidStatusException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidStatusException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidParameterException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidParameterException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidPeriodException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidPeriodException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidIntersectionException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidIntersectionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwPreexistingConditionException($message = null, $code = 0, Exception $previous = null)
    {
        throw new PreexistingConditionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws InvalidDeleteException
     */
    public function throwInvalidDeleteException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidDeleteException($message, $code, $previous);
    }

    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws InvalidUpdateException
     */
    public function throwInvalidUpdateException($message = null, $code = 0, Exception $previous = null)
    {
        throw new InvalidUpdateException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws InvalidDeleteException
     */
    public function throwMandatoryConditionException($message = null, $code = 0, Exception $previous = null)
    {
        throw new MandatoryConditionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws SendingEmailException
     */
    public function throwSendingEmailException($message = null, $code = 0, Exception $previous = null)
    {
        throw new SendingEmailException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws NotFoundException
     */
    public function throwNotFoundException($message = null, $code = 0, Exception $previous = null)
    {
        throw new NotFoundException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws Exception
     */
    public function throwException($message = null, $code = 0, Exception $previous = null)
    {
        throw new Exception($message, $code, $previous);
    }
}
