<?php
namespace Core\Service;

use \Exception;
use Core\Db\TableGateway;
use Core\Model\Entity;
use Core\Model\EntityException;
use stdClass;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Form\Form;
use Zend\Paginator\Adapter\ArrayAdapter as PaginatorAdapterArrayAdapter;
use Zend\Paginator\Adapter\DbSelect as PaginatorAdapterDbSelect;
use Zend\Paginator\Paginator;
use Zend\ServiceManager\ServiceManager;
use Zend\ServiceManager\ServiceManagerAwareInterface;
use Zend\Stdlib\Parameters;
use Zend\Db\Sql\Combine;
use Core\Service\AutenticacaoService;
use Core\Service\ExceptionService;
use \DateTime;

abstract class Service implements ServiceManagerAwareInterface
{
    /**
     * @var ServiceManager
     */
    protected $serviceManager;
    
    /**
     *
     * @var type
     */
    protected $exceptionService;

    /**
     *
     */
    const ITEM_COUNT_PER_PAGE = 10;

    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidPeriodException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwInvalidPeriodException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwInvalidIntersectionException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwInvalidIntersectionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     */
    public function throwPreexistingConditionException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwPreexistingConditionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws InvalidDeleteException
     */
    public function throwInvalidDeleteException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwInvalidDeleteException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws InvalidDeleteException
     */
    public function throwMandatoryConditionException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwMandatoryConditionException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws SendingEmailException
     */
    public function throwSendingEmailException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwSendingEmailException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws NotFoundException
     */
    public function throwNotFoundException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwNotFoundException($message, $code, $previous);
    }
    
    /**
     *
     * @param type $message
     * @param type $code
     * @param Exception $previous
     * @throws Exception
     */
    public function throwException($message = null, $code = 0, Exception $previous = null)
    {
        $this->exceptionService->throwException($message, $code, $previous);
    }

    /**
     * @name                     isValid
     *
     * @param \Core\Model\Entity $model
     * @param type               $dataSource
     *
     * @access public
     * @return boolean
     */
    public function isValid(Entity $model, $dataSource)
    {
        $isValid = true;

        if (!array_key_exists('dataSource', $dataSource) && !$dataSource instanceof stdClass && !$dataSource instanceof Form) {
            throw new Exception("Set key 'dataSource' on variable 'dataSource' or send a stdClass or a Zend\\Form");
        }

        switch ($dataSource) {
            case $dataSource instanceof stdClass:
            case $dataSource instanceof Form:
                $data = $this->normalizeDataSource($dataSource);
                break;
            default:
                $data = $this->normalizeDataSource($dataSource['dataSource']);
                break;
        }

        if (array_key_exists('form', $dataSource) && $dataSource['form'] instanceof Form) {
            try {
                $model->setData($data);
                $dataSource['form']->bind($dataSource['dataSource']);
                $dataSource['form']->setInputFilter($model->getInputFilter());
                $isValid = $dataSource['form']->isValid() && $isValid;
            } catch (EntityException $exception) {
                $dataSource['form']->bind($dataSource['dataSource']);
                $dataSource['form']->setInputFilter($model->getInputFilter());
                $isValid = $dataSource['form']->isValid() && $isValid;
            }

            return $isValid;
        }

        if ($dataSource instanceof Form) {
            try {
                $model->setData($data);
                $dataSource->setData($data);
                $dataSource->setInputFilter($model->getInputFilter());
                $isValid = $dataSource->isValid() && $isValid;
            } catch (EntityException $exception) {
                $dataSource->setData($data);
                $dataSource->setInputFilter($model->getInputFilter());
                $isValid = $dataSource->isValid() && $isValid;
            }

            return $isValid;
        }

        try {
            $model->setData($data);
            return $model->isValid($data);
        } catch (EntityException $exception) {
            throw $exception;
        }
    }

    /**
     * @param ServiceManager $serviceManager
     */
    public function setServiceManager(ServiceManager $serviceManager)
    {
        $this->serviceManager   = $serviceManager;
        $this->exceptionService = $this->serviceManager->get(ExceptionService::class);
    }

    /**
     * Retrieve serviceManager instance
     *
     * @return ServiceLocatorInterface
     */
    public function getServiceManager()
    {
        return $this->serviceManager;
    }

    /**
     * Return pair key => description
     *
     * @name         fetchPairs
     *
     * @param  array $options
     *
     * @access public
     * @return array
     */
    public function fetchPairs(array $options = array())
    {
        $options['description'] = $options['description'] ? $options['description'] : $options['index'];

        $pairs  = array();
        $select = $this->getSelect();
        $select->from(array('rs' => $options['tableName']))
            ->columns(array($options['index'], $options['description']))
            ->order(array($options['description']));

        foreach ($this->executeSelect($select) as $pair) {
            $pairs[$pair[$options['index']]] = $pair[$options['description']];
        }

        return $pairs;
    }

    /**
     *
     *
     * @name                         normalizeDataSource
     *
     * @param  Form|Parameters|array $dataSource
     *
     * @access public
     * @return array
     */
    public function normalizeDataSource($dataSource)
    {
        if ($dataSource instanceof Form) {
            $dataSource->isValid();

            return $dataSource->getData();
        }

        if ($dataSource instanceof Parameters) {
            return $dataSource->toArray();
        }

        if ($dataSource instanceof Entity) {
            return $dataSource->toArray();
        }

        if ($dataSource instanceof stdClass) {
            return get_object_vars($dataSource);
        }

        if (is_array($dataSource)) {
            if (isset($dataSource['dataSource'])) {
                return $dataSource['dataSource'] instanceof stdClass ?
                    get_object_vars($dataSource['dataSource']) :
                    $dataSource['dataSource'];
            }
            
            return $dataSource;
        }
    }

    /**
     *
     * @name          getModel
     *
     * @param  string $fullNamespaceModel
     *
     * @access public
     * @return Model
     */
    public function getModel($fullNamespaceModel)
    {
        $model = $this->getServiceManager()->get($fullNamespaceModel);
        $model->clearModelVars();

        return clone $model;
    }

    /**
     * @name   beginTransaction
     * @access public
     * @return void
     */
    public function beginTransaction()
    {
        $connection = $this->getConnection();
        $connection->beginTransaction();
        
        return $this;
    }

    /**
     * @name   commit
     * @access public
     * @return void
     */
    public function commit()
    {
        $connection = $this->getConnection();
        
        if ($connection->inTransaction()) {
            $connection->commit();
        }
        
        return $this;
    }

    /**
     * @name   rollback
     * @access public
     * @return void
     */
    public function rollback()
    {
        $connection = $this->getConnection();
        
        if ($connection->inTransaction()) {
            $connection->rollback();
        }
        
        return $this;
    }
    /**
     *
     * @param array $params
     *
     */
    
    public function normalizeDate(array $params = [])
    {
        $data = DateTime::createFromFormat($params['de'], $params['data']);
        return $data->format($params['para']);
    }

    /**
     *
     * @name   getConnection
     * @access protected
     * @return mixed
     */
    protected function getConnection()
    {
        $dbAdapter = $this->getServiceManager()->get('DbAdapter');

        return $dbAdapter->getDriver()
            ->getConnection();
    }

    /**
     *
     * @name         fetchAll
     *
     * @param Select $select *
     *
     * @return array
     */
    protected function fetchAll(Select $select)
    {
        $data = [];

        foreach ($this->executeSelect($select) as $value) {
            $data[] = $value;
        }

        return $data;
    }

    /**
     *
     *
     * @name         generatePaginator
     *
     * @param  array $options
     *
     * @access protected
     * @return \Zend\Paginator\Paginator
     * @throws Exception
     */
    protected function generatePaginator(array $options)
    {
        $pageNumber       = isset($options['page']) ? $options['page'] : 1;
        $adapter          = isset($options['adapter']) ? $options['adapter'] : 'select';
        $itemCountPerPage = isset($options['itemCountPerPage']) ? $options['itemCountPerPage'] : self::ITEM_COUNT_PER_PAGE;

        if ($adapter === 'select') {
            if (!isset($options['sql']) || !$options['sql'] instanceof Sql) {
                throw new Exception("The variable 'sql' is not instance of Zend\Db\Sql\Sql");
            }

            if (!isset($options['select']) || !$options['select'] instanceof Select) {
                throw new Exception("The variable 'select' is not instance of Zend\Db\Sql\Select");
            }

            $paginator = new Paginator(new PaginatorAdapterDbSelect(
                $options['select'],
                $options['sql']
            ));
        }

        if ($adapter === 'array') {
            if (!isset($options['items']) || !is_array($options['items'])) {
                throw new Exception("The variable 'items' is not array");
            }

            $paginator = new Paginator(new PaginatorAdapterArrayAdapter(
                $options['items']
            ));
        }

        $paginator->setCurrentPageNumber($pageNumber)
            ->setItemCountPerPage($itemCountPerPage);

        return $paginator;
    }

    /**
     * Salva o model na base de dados e retorna a instância do model.
     *
     * @name         saveModel
     *
     * @param  Model $model
     *
     * @access private
     * @return Model
     */
    protected function saveModel(Entity $model, $protectAgainstUnsafeUpdate = false)
    {
        if ($protectAgainstUnsafeUpdate && $model->getPrimaryKeyValue()) {
            $options = [
                'fullNamespaceModel'   => $model->getClassName(),
                'referencedTableName'  => $model->getTableName(),
                'referencedColumnName' => $model->getPrimaryKeyField(),
                'pkIdValue'            => $model->getPrimaryKeyValue(),
            ];

            if ($this->thisPrimaryKeyValueIsBeingUsed($options)) {
                $this->exceptionService->throwInvalidUpdateException(
                    'Você não pode alterar o registro <b>[' . $model->getPrimaryKeyValue() . ']</b> pois seu valor está sendo utilizado em outros recursos do sistema'
                );
            }
        }
        
        $modelSaved = $this->getTable($model->getClassName())->save($model);

        return $modelSaved;
    }

    /**
     *
     *
     * @name          deleteModel
     *
     * @param  string $fullNamespaceModel
     * @param  string $where
     *
     * @access protected
     * @return type
     */
    protected function deleteModel($options, $protectAgainstUnsafeDelete = true)
    {
        if ($protectAgainstUnsafeDelete && $options instanceof Entity) {
            $parameters = [
                'fullNamespaceModel'   => $options->getClassName(),
                'referencedTableName'  => $options->getTableName(),
                'referencedColumnName' => $options->getPrimaryKeyField(),
                'pkIdValue'            => $options->getPrimaryKeyValue(),
            ];
            
            if ($this->thisPrimaryKeyValueIsBeingUsed($parameters)) {
                $this->exceptionService->throwInvalidDeleteException(
                    'Você não pode excluir o registro <b>[' . $options->getPrimaryKeyValue() . ']</b> pois seu valor está sendo utilizado em outros recursos do sistema'
                );
            }
        }
        
        if (is_array($options)) {
            return (bool) $this->getTable($options['fullNamespaceModel'])->delete($options['where']);
        }

        if ($options instanceof Entity) {
            $where = array(
                $options->getPrimaryKeyField() => $options->getPrimaryKeyValue(),
            );
            return (bool) $this->getTable($options->getClassName())->delete($where);
        }
    }
    
    /**
     *
     * @param type $options
     * @return type
     */
    protected function thisPrimaryKeyValueIsBeingUsed($options)
    {
        $serviceManager = $this->getServiceManager();
        
        $model                         = $options['fullNamespaceModel'] ? $options['fullNamespaceModel'] : null;
        $referencedTableName           = $options['referencedTableName'] ? $options['referencedTableName'] : null;
        $referencedColumnName          = $options['referencedColumnName'] ? $options['referencedColumnName'] : null;
        $pkIdValue                     = $options['pkIdValue'] ? $options['pkIdValue'] : null;
        $dbAdapterInformationSchema    = $serviceManager->get('DbAdapterInformationSchema');
        $dbAdapter                     = $serviceManager->get('DbAdapter');
        $combine                       = new Combine();
        $tableGatewayInformationSchema = new TableGateway($dbAdapterInformationSchema, $model, $serviceManager->get($model), $serviceManager);
        $tableGatewayInformationSchema->initialize();
        
        $parameters = [
            'referencedTableName'  => $referencedTableName,
            'referencedColumnName' => $referencedColumnName,
            'tableSchema'          => $dbAdapter->getCurrentSchema(),
        ];
        
        $tables = $tableGatewayInformationSchema->getAllTablesWhereThisPrimaryKeyColumnIsBeingUsed($parameters);
        
        if ($tables == false) {
            return false;
        }
        
        foreach ($tables as $key => $value) {
            $selectTemp = $this->getSelect();
            $selectTemp->from(['t' . $key => $value['TABLE_NAME']])
                ->where($value['COLUMN_NAME'] . ' = ' . $pkIdValue)
                ->columns(['id' => $value['COLUMN_NAME']]);
            
            $combine->union($selectTemp);
        }
        
        $query  = str_replace('"', '', $combine->getSqlString());
        $result = $dbAdapter->query($query)->execute()->current();
        
        return $result == true;
    }

    /**
     * @param $id
     *
     * @return array|\ArrayObject|Entity
     */
    protected function get($options)
    {
        try {
            if (is_array($options)) {
                return $this->getTable($options['fullNamespaceModel'])->get($options['id']);
            }

            if ($options instanceof Entity) {
                return $this->getTable($options->getClassName())->get($options->getPrimaryKeyValue());
            }
        } catch (EntityException $entityException) {
            throw $entityException;
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @name   getSql
     * @access protected
     * @return Sql
     */
    protected function getSql()
    {
        $sm = $this->getServiceManager();

        return new Sql($sm->get('DbAdapter'));
    }

    /**
     *
     * @name   getSelect
     * @access protected
     * @return select
     */
    protected function getSelect()
    {
        return $this->getSql()->select();
    }

    /**
     *
     * @name          executeSelect
     *
     * @param  Select $select
     *
     * @access protected
     * @return Result
     */
    protected function executeSelect($select)
    {
        return $this->getSql()
            ->prepareStatementForSqlObject($select)
            ->execute();
    }


    /**
     * Retrieve TableGateway
     *
     * @param  string $table
     *
     * @return TableGateway
     */
    protected function getTable($table)
    {
        $sm           = $this->getServiceManager();
        $dbAdapter    = $sm->get('DbAdapter');
        $tableGateway = new TableGateway($dbAdapter, $table, $sm->get($table), $sm);
        $tableGateway->initialize();

        return $tableGateway;
    }


    /**
     * Retrieve Service
     *
     * @return Service
     */
    protected function getService($service)
    {
        return $this->getServiceManager()->get($service);
    }

    /**
     *
     * @name          getModelColumns
     *
     * @param  string $fullNamespaceModel
     *
     * @access protected
     * @return array
     */
    protected function getModelColumns($fullNamespaceModel)
    {
        return $this->getModel($fullNamespaceModel)->getColumns();
    }


    /**
     *
     * @name   getCurrentTime
     * @access public
     * @return string
     */
    public function getTime($option = array())
    {
        if (isset($option['date'])) {
            $now = new \DateTime($option['date']);
        } else {
            $now = new \DateTime();
        }
        
        if (isset($option['modify'])) {
            $now->modify('+'.$option['modify']);
        }

        if (isset($option['format'])) {
            return $now->format($option['format']);
        }

        return $now->format('Y-m-d H:i:s');
    }
    

    /**
     *
     * @param array $options
     * @access public
     * @return array
     */
    public function getWhereClauses($options = array())
    {
        $where = array();

        while (list($key, $val) = each($options)) {
            if ($val != '') {
                if (strpos($key, 'str_') !== false) {
                    $where[] = substr($key, 4)." LIKE '%".$val."%'";
                    continue;
                }

                if (is_numeric($val)) {
                    $where[$key] = $val;
                    continue;
                }



                if (is_string($val)) {
                    $where[] = $key." LIKE '%".$val."%'";
                }
            }
        }

        return $where;
    }

    public function getSelectResult($select)
    {
        $result = array();

        foreach ($this->executeSelect($select) as $data) {
            $result[] = $data;
        }

        return $result;
    }
}
