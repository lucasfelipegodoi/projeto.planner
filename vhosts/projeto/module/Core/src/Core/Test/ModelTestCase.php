<?php
namespace Core\Test;

abstract class ModelTestCase extends TestCase
{
    /**
     *
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        
        $pathConfigTest = getcwd() . '/config/test.config.php';
        $configTest     = include $pathConfigTest;
        
        if (isset($configTest['mockShouldBeUsedForModel'])) {
            $this->mockShouldBeUsedForModel = (bool) $configTest['mockShouldBeUsedForModel'];
        }
        
        parent::setup();
    }
    
    /**
     *
     */
    public function tearDown()
    {
        if ($this->mockShouldBeUsedForModel === false) {
            parent::tearDown();
        }
    }
}
