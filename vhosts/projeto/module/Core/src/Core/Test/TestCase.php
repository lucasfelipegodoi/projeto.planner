<?php
namespace Core\Test;

use Core\Db\TableGateway;
use Core\Model\Entity;
use Core\Service\MigrationService;
use Core\Model\EntityException;
use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Service\ServiceManagerConfig;
use Zend\ServiceManager\ServiceManager;

abstract class TestCase extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Zend\ServiceManager\ServiceManager
     */
    protected $serviceManager;

    /**
     * @var Zend\Mvc\Application
     */
    protected $application;

    /**
     * @var Zend\Di\Di
     */
    protected $di;

    /**
     * @var bool
     */
    protected $createDataBase = false;
    
    /**
     *
     * @var bool
     */
    protected $createDataBaseUsingPhinx = false;
    
    /**
     *
     * @var
     */
    protected $migrationService;

    /**
     * @var bool
     */
    protected $databaseWasCreated = false;

    /**
     * @var array
     */
    protected $ignoreThoseModules = array();
    
    /**
     *
     * @var type
     */
    protected $mockShouldBeUsedForModel = false;

    /**
     * @var const Namespace completo do Paginator
     */
    const FULL_NAMESPACE_PAGINATOR = 'Zend\Paginator\Paginator';
    /**
     * @var const Namespace completo do ViewModel
     */
    const FULL_NAMESPACE_VIEW_MODEL = 'Zend\View\Model\ViewModel';
    
    /**
     *
     */
    const TOKEN_TESTING = '3f14ed21e597e103f9287a778757fda34f1d3f83';
    
    /**
     *
     */
    const DEFAULT_ENVIRONMENT = 'testing';

    /**
     *
     */
    public function setup()
    {
        parent::setup();
        $pathConfigTest = getcwd() . '/config/test.config.php';

        $config                                                   = include 'config/application.config.php';
        $config['module_listener_options']['config_static_paths'] = array($pathConfigTest);
        $configTest = include $pathConfigTest;

        if (file_exists(__DIR__ . '/config/test.config.php')) {
            $moduleConfig = include __DIR__ . '/config/test.config.php';
            array_unshift($config['module_listener_options']['config_static_paths'], $moduleConfig);
        }

        if (isset($configTest['ignoreThoseModules'])) {
            $this->ignoreThoseModules = $configTest['ignoreThoseModules'];
        }
        
        $this->serviceManager = new ServiceManager(new ServiceManagerConfig(
            isset($config['service_manager']) ? $config['service_manager'] : array()
        ));
        $this->serviceManager->setService('ApplicationConfig', $config);
        $this->serviceManager->setFactory('ServiceListener', 'Zend\Mvc\Service\ServiceListenerFactory');

        $moduleManager = $this->serviceManager->get('ModuleManager');
        $moduleManager->loadModules();
        $this->routes = array();
        foreach ($moduleManager->getModules() as $module) {
            if (in_array($module, $this->ignoreThoseModules)) {
                continue;
            }

            $moduleConfig = include __DIR__ . '/../../../../' . ucfirst($module) . '/config/module.config.php';
            if (isset($moduleConfig['router'])) {
                foreach ($moduleConfig['router']['routes'] as $key => $name) {
                    $this->routes[$key] = $name;
                }
            }
        }
        $this->serviceManager->setAllowOverride(true);
        $this->application = $this->serviceManager->get('Application');
        $this->event       = new MvcEvent();
        
        $this->event->setTarget($this->application);
        $this->event->setApplication($this->application)
            ->setRequest($this->application->getRequest())
            ->setResponse($this->application->getResponse())
            ->setRouter($this->serviceManager->get('Router'));
        
        $this->migrationService = $this->getService(MigrationService::class);
        
        if ($this->createDataBase) {
            $this->createDatabase();
        }
    }

    public function tearDown()
    {
        parent::tearDown();

        if ($this->databaseWasCreated) {
            $this->dropDatabase();
        }
    }

    /**
     * Salva o model na base de dados e retorna a instância do model.
     *
     * @name         saveModel
     *
     * @param  Entity $model
     *
     * @access private
     * @return Entity
     */
    protected function saveModel(Entity $model)
    {
        if ($this->mockShouldBeUsedForModel) {
            if ($model->isValid($model->getData()) === false) {
                throw new EntityException('Invalid model');
            }
            
            if ($model->getPrimaryKeyValue() == null) {
                $model->setPrimaryKeyValue(1);
                $model->wasMadeAnInsert = true;
                $model->wasMadeAnUpdate = false;
                $model->id = 1;
            }
            
            if ($model->getPrimaryKeyValue()) {
                $model->wasMadeAnInsert = false;
                $model->wasMadeAnUpdate = true;
            }
            
            return $model;
        }
        
        return $this->getTable($model->getClassName())->save($model);
    }

    /**
     * Exclui o model na base de dados
     *
     * @name         deleteModel
     *
     * @param  Entity $model
     *
     * @access private
     * @return
     */
    protected function deleteModel(Entity $model)
    {
        if ($this->mockShouldBeUsedForModel) {
            $model->setPrimaryKeyValue(null);
            return true;
        }
        
        return self::getTable($model->getClassName())->delete($model);
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    protected function getSavedModel(Entity $model)
    {
        if ($this->mockShouldBeUsedForModel) {
            if ($model->getPrimaryKeyValue() == null) {
                throw new EntityException('Could not find row');
            }
            
            return $model;
        }
        
        return self::getTable($model->getClassName())->get($model->getPrimaryKeyValue());
    }

    /**
     * Retorna uma ou mais instâncias preeenchidas do model.
     *
     * @name          getModel
     *
     * @param  string $fullNamespaceModel Name of model
     *
     * @access protected
     * @return Entity
     */
    protected function getModel($fullNamespaceModel)
    {
        if ($fullNamespaceModel != null) {
            if ($this->mockShouldBeUsedForModel) {
                $options = [
                    'serviceManager' => $this->serviceManager,
                ];
                        
                $model = $this->getMockBuilder($fullNamespaceModel)
                    ->setMethods(null)
                    //->disableOriginalConstructor()
                    //->enableArgumentCloning()
                    ->setConstructorArgs([$options])
                    ->getMock();
                
                return $model;
            }
            
            return $this->serviceManager->get($fullNamespaceModel);
        }

        return null;
    }

    /**
     * Retrieve TableGateway
     *
     * @param  string $table
     *
     * @return TableGateway
     */
    protected function getTable($table)
    {
        $dbAdapter    = $this->serviceManager->get('DbAdapter');
        $tableGateway = new TableGateway($dbAdapter, $table, self::getModel($table), $this->serviceManager);
        $tableGateway->initialize();

        return $tableGateway;
    }

    /**
     * Retrieve Service
     *
     * @param  string $service
     *
     * @return Service
     */
    protected function getService($service)
    {
        return $this->serviceManager->get($service);
    }

    /**
     * @return void
     */
    public function createDatabase()
    {
        if ($this->createDataBaseUsingPhinx === true) {
            $this->migrationService->getMigrate(self::DEFAULT_ENVIRONMENT);
            return $this->databaseWasCreated = true;
        }
        
        $dbAdapter = $this->getAdapter();

        if (get_class($dbAdapter->getPlatform()) == 'Zend\Db\Adapter\Platform\Sqlite') {
            //enable foreign keys on sqlite
            $dbAdapter->query('PRAGMA foreign_keys = ON;', Adapter::QUERY_MODE_EXECUTE);
        }

        if (get_class($dbAdapter->getPlatform()) == 'Zend\Db\Adapter\Platform\Mysql') {
            //enable foreign keys on mysql
            $dbAdapter->query('SET FOREIGN_KEY_CHECKS = 1;', Adapter::QUERY_MODE_EXECUTE);
        }

        $queries = include \Bootstrap::getModulePath() . '/data/test.data.php';

        foreach ($queries as $query) {
            if ($query['beforeCreate']) {
                $dbAdapter->query($query['beforeCreate'], Adapter::QUERY_MODE_EXECUTE);
            }
        }

        foreach ($queries as $query) {
            if ($query['create']) {
                $dbAdapter->query($query['create'], Adapter::QUERY_MODE_EXECUTE);
            }
        }

        foreach ($queries as $query) {
            foreach ($query['insert'] as $insert) {
                $dbAdapter->query($insert, Adapter::QUERY_MODE_EXECUTE);
            }
        }

        return $this->databaseWasCreated = true;
    }

    /**
     * @return void
     */
    public function dropDatabase()
    {
        if ($this->createDataBaseUsingPhinx === true) {
            $this->migrationService->getRollback(self::DEFAULT_ENVIRONMENT);
            return null;
        }
        
        $dbAdapter = $this->getAdapter();

        if (get_class($dbAdapter->getPlatform()) == 'Zend\Db\Adapter\Platform\Sqlite') {
            //disable foreign keys on sqlite
            $dbAdapter->query('PRAGMA foreign_keys = OFF;', Adapter::QUERY_MODE_EXECUTE);
        }
        if (get_class($dbAdapter->getPlatform()) == 'Zend\Db\Adapter\Platform\Mysql') {
            //disable foreign keys on mysql
            $dbAdapter->query('SET FOREIGN_KEY_CHECKS = 0;', Adapter::QUERY_MODE_EXECUTE);
        }

        $queries = include \Bootstrap::getModulePath() . '/data/test.data.php';
        foreach ($queries as $query) {
            if ($query['drop']) {
                $dbAdapter->query($query['drop'], Adapter::QUERY_MODE_EXECUTE);
            }
        }
    }

    /**
     *
     * @return Zend\Db\Adapter\Adapter
     */
    public function getAdapter()
    {
        return $this->serviceManager->get('DbAdapter');
    }
}
