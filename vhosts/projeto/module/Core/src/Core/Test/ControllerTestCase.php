<?php
namespace Core\Test;

use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\Router\RouteMatch;
use Zend\View\Resolver;

abstract class ControllerTestCase extends TestCase
{
    /**
     * The ActionController we are testing
     *
     * @var Zend\Mvc\Controller\AbstractActionController
     */
    protected $controller;

    /**
     * A request object
     *
     * @var Zend\Http\Request
     */
    protected $request;

    /**
     * A response object
     *
     * @var Zend\Http\Response
     */
    protected $response;

    /**
     * The matched route for the controller
     *
     * @var Zend\Mvc\Router\RouteMatch
     */
    protected $routeMatch;

    /**
     * An MVC event to be assigned to the controller
     *
     * @var Zend\Mvc\MvcEvent
     */
    protected $event;

    /**
     * The Controller fully qualified domain name, so each ControllerTestCase can create an instance
     * of the tested controller
     *
     * @var string
     */
    protected $controllerFQDN;

    /**
     * The route to the controller, as defined in the configuration files
     *
     * @var string
     */
    protected $controllerRoute;

    /**
     *
     */
    public function setup()
    {
        parent::setup();
        $this->configRoute([
            'module' => $this->controllerRoute,
            'controller' => $this->controllerFQDN,
        ]);
    }
    
    private function configRoute(array $options = [])
    {
        $this->controller = $this->serviceManager->get($options['controller']);
        $this->request    = new Request();
        $this->routeMatch = new RouteMatch(array(
            'is_testing_environment' => true,
            'check_if_has_identity'  => isset($options['check_if_has_identity']) ? $options['check_if_has_identity'] : false,
            'check_is_authorized'    => isset($options['check_is_authorized']) ? $options['check_is_authorized'] : false,
            'token_testing'          => self::TOKEN_TESTING,
            'module'                 => $options['module'],
            'controller'             => $options['controller'],
            'action'                 => null,
            '__NAMESPACE__'          => __NAMESPACE__,
            '__CONTROLLER__'         => null,
            'router' => array(
                'routes' => array(
                    $options['module'] => $this->routes[$options['module']]
                )
            )
        ));
        
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($this->serviceManager);
    }

    /**
     *
     */
    public function tearDown()
    {
        parent::tearDown();
        unset($this->controller);
        unset($this->request);
        unset($this->routeMatch);
        unset($this->event);
    }


    /**
     * Config route match
     *
     * @name         setRouteMatch
     *
     * @param  array $config
     *
     * @access protected
     * @return void
     */
    protected function setRouteMatch(array $config)
    {
        if (isset($config['recreate_route_match']) && $config['recreate_route_match'] === true) {
            $module     = $this->controllerRoute;
            $controller = $this->controllerFQDN;
            
            if (isset($config['params']['module']) && $config['params']['controller']) {
                $module     = $config['params']['module'];
                $controller = $config['params']['controller'];
                
                unset($config['params']['module']);
                unset($config['params']['controller']);
            }
            
            $this->configRoute([
                'module'     => $module ,
                'controller' => $controller,
            ]);
        }
        
        $this->request->setMethod($config['method']);

        foreach ($config['params'] as $key => $value) {
            $this->routeMatch->setParam($key, $value);
        }
    }

    /**
     * Return the response
     *
     * @name   getResponse
     * @access protected
     * @return array
     */
    protected function getResponse()
    {
        $this->controller->dispatch($this->request, $this->response);
        $response = $this->controller->getResponse();

        return array(
            'response' => $response,
            'headers'  => $response->getHeaders(),
        );
    }
}
