<?php
namespace Core\Test;

use \Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;

abstract class ApiTestCase extends TestCase
{
    /**
     *
     * @var type
     */
    protected $httpClient;
    
    /**
     *
     * @var type
     */
    protected $httpClientConfig = [
        'base_uri' => 'http://localhost:8000',
        'debug' => true,
        'http_errors' => false,
        'headers' => [
            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ],
    ];
    
    /**
     *
     */
    const HTTP_LOCALHOST_TEST = 'localhost:8000';
    
    /**
     *
     */
    public function setup()
    {
        parent::setup();
        
        try {
            $this->httpClient = new Client($this->httpClientConfig);
            $this->httpClient->request('GET', '/');
        } catch (ConnectException $connectException) {
            $this->startLocalWebService();
        } catch (Exception $exception) {
            throw $exception;
        }
    }
    
    /**
     *
     * @param array $options
     * @return array
     */
    protected function postApi(array $options)
    {
        $response = $this->httpClient->request('POST', $options['uri'], [
            'json' => $options['json'],
        ]);
        
        $body            = $response->getBody();
        $contentResponse = json_decode($body->getContents());
        $statusCode      = $response->getStatusCode();
        
        return [
            'body'            => $body,
            'contentResponse' => $contentResponse,
            'statusCode'      => $statusCode,
        ];
    }

    /**
     *
     * @param array $options
     * @return array
     */
    
    /**
     *
     * @param array $options
     * @return type
     */
    protected function putApi(array $options)
    {
        $response = $this->httpClient->request('PUT', $options['uri'], [
            'json' => $options['json'],
        ]);
        
        $body            = $response->getBody();
        $contentResponse = json_decode($body->getContents());
        $statusCode      = $response->getStatusCode();
        
        return [
            'body'            => $body,
            'contentResponse' => $contentResponse,
            'statusCode'      => $statusCode,
        ];
    }
    
    /**
     *
     * @param array $options
     * @return array
     */
    protected function deleteApi(array $options)
    {
        if (isset($options['json'])) {
            $response = $this->httpClient->request('DELETE', $options['uri'], [
                'json' => $options['json'],
            ]);
        } else {
            $response = $this->httpClient->request('DELETE', $options['uri']);
        }
        
        $body            = $response->getBody();
        $contentResponse = json_decode($body->getContents());
        $statusCode      = $response->getStatusCode();
        
        return [
            'body'            => $body,
            'contentResponse' => $contentResponse,
            'statusCode'      => $statusCode,
        ];
    }

    /**
     *
     * @param array $options
     * @return array
     */
    protected function getApi(array $options)
    {
        if (isset($options['json'])) {
            $response = $this->httpClient->request('GET', $options['uri'], [
                'json' => $options['json'],
            ]);
        } else {
            $response = $this->httpClient->request('GET', $options['uri']);
        }
        
        $body            = $response->getBody();
        $contentResponse = json_decode($body->getContents());
        $statusCode      = $response->getStatusCode();
        
        return [
            'body'            => $body,
            'contentResponse' => $contentResponse,
            'statusCode'      => $statusCode,
        ];
    }
    
    /**
     *
     */
    protected function startLocalWebService()
    {
        $command = 'php -S ' . self::HTTP_LOCALHOST_TEST . ' -t httpdocs > /dev/null &';
        
        if (strtoupper(PHP_OS) === 'WINNT') {
            $command = 'start_local_webservice.bat';
        }
        
        exec($command);
    }
}
