<?php
/**
 * Module config
 */
use Skel\Form\DefaultForm;
use Skel\Form\DefaultFormFilter;
use Skel\Model\DefaultModel;
use Skel\Service\DefaultService;

return [
//    'asset_bundle' => array(
//        'assets' => array(
//            'less' => array('@zfRootPath/vendor/twbs/bootstrap/less/bootstrap.less')
//        )
//    ),
    'controllers'     => [
        'invokables' => [
            'Skel\Controller\Default' => 'Skel\Controller\DefaultController'
        ],
    ],
    'service_manager' => [
        'factories' => [
            // ----------------- Factories Forms ---------------------------- //

            'Skel\Form\DefaultForm' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new DefaultForm($options);
            },
            'Skel\Form\DefaultFormFilter' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new DefaultFormFilter($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Skel\Model\DefaultModel' => function ($serviceManager) {
                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new DefaultModel($options);
            },

            // -------------- Factories Services ---------------------------- //

            'Skel\Service\DefaultService' => function ($serviceManager) {
                $options = [];

                return new DefaultService($options);
            },

        ],
    ],
    'router'          => [
        'routes' => [
            'home' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/[page/:page]',
                    'defaults' => [
                        'controller' => 'Skel\Controller\Default',
                        'action'     => 'index',
                        'module'     => 'skel',
                    ],
                ],
            ],
            'skel' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/skel',
                    'defaults' => [
                        'controller'    => 'Default',
                        'action'        => 'index',
                        '__NAMESPACE__' => 'Skel\Controller',
                        'module'        => 'skel'
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'default' => [
                        'type'         => 'Segment',
                        'options'      => [
                            'route'       => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults'    => [],
                        ],
                        'child_routes' => [ //permite mandar dados pela url
                            'wildcard' => [
                                'type' => 'Wildcard'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager'    => [
        'display_not_found_reason' => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'display_exceptions'       => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'partials/paginator/control' => __DIR__ . '/../../Core/view/partials/paginator/control.phtml',
            'layout/layout'              => __DIR__ . '/../../Core/view/layout/layout.phtml',
            'layout/layout_email'        => __DIR__ . '/../../Core/view/layout/layout_email.phtml',
            'error/404'                  => __DIR__ . '/../../Core/view/error/404.phtml',
            'error/index'                => __DIR__ . '/../../Core/view/error/index.phtml',
        ],
        'template_path_stack'      => [
            'skel' => __DIR__ . '/../view',
        ],
    ],
];
