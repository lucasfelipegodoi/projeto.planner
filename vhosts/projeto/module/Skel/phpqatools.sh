#!/bin/bash

echo "============================================  PHP Code Beautifier and Fixer ==========================================="
../../vendor/bin/phpcbf --standard=PSR2 --ignore=tests/build/ .