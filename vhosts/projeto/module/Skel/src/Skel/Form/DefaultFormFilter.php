<?php
/**
 * Form
 */
namespace Skel\Form;

use Core\Form\BaseFormFilter;

/**
 * Formulário para filtrar itens
 *
 * @name    DefaultFormFilter
 * @package Skel\Form
 * @author  Author <e-mail>
 */
class DefaultFormFilter extends BaseFormFilter
{

    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('id', 'skel-default-index');
        $this->setAttribute('class', 'form-inline');

        $this->addText([
            'name'  => 'description',
            'label' => 'Description',
            'column-size' => 'sm-4',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Description of item',
        ]);

        $this->addText([
            'name'  => 'teste',
            'label' => 'Teste',
            'column-size' => 'sm-2',
            'label_attributes' => [
                'class' => 'col-sm-1'
            ],
            'help-block' => 'Description of item',
        ]);
        
        $this->addSubmit([
            'value' => 'Buscar',
            'class' => 'btn btn-primary',
        ]);
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
