<?php

/**
 * Controller
 */

namespace Skel\Controller;

use \Exception;
use Core\Model\EntityException;
use Core\Controller\ActionController;
use Skel\Form\DefaultForm;
use Skel\Form\DefaultFormFilter;
use Skel\Service\DefaultService;
use Zend\View\Model\ViewModel;

/**
 * Controller Default
 *
 * @name    DefaultController
 * @package Skel\Controller
 * @author  Author <e-mail>
 */
class DefaultController extends ActionController
{
    /**
     * Método para excluir um item
     *
     * @name   deleteAction
     * @access public
     * @return void
     */
    public function deleteAction()
    {
        $defaultService = $this->getService(DefaultService::class);
        $id = $this->params()->fromRoute('id', null);

        if ($defaultService->deleteById($id)) {
            $this->addSuccessMessage('Exclusão realizada com sucesso!');
        } else {
            $this->addErrorMessage('Não foi possível executar a exclusão!');
        }

        $this->redirect()->toUrl('/skel/default/index');
    }

    /**
     * Método para gerenciar os itens
     *
     * @name   indexAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $formFilter = $this->getService(DefaultFormFilter::class);
        $defaultService = $this->getService(DefaultService::class);
        $params = $this->getParamsFromRoute();

        $viewModel->setVariables([
            'formFilter' => $formFilter->bind($params['parameters']),
            'paginator' => $defaultService->paginate($params),
        ]);

        return $viewModel;
    }

    /**
     * Método para salvar um item
     *
     * @name   saveAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function saveAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(DefaultService::class);
        $form = $this->getService(DefaultForm::class);
        $id = $this->params()->fromRoute('id', null);

        $viewModel->setVariables([
            'form' => $form,
        ]);
        
        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {
                    $this->addSuccessMessage('Ação realizada com sucesso!');
                    $this->redirect()->toUrl('/skel/default/index');
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/skel/default/save');
        }

        return $viewModel;
    }

    /**
     * Método para visualizar um item
     *
     * @name   viewAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function viewAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(DefaultService::class);
        $id = $this->params()->fromRoute('id', null);

        try {
            $viewModel->setVariables([
                'item' => $defaultService->getItemById($id),
            ]);

            $viewModel->setTerminal(true);

            return $viewModel;
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/skel/default/index');
        }
    }
}
