<?php
namespace Skel;

use Skel\Model\DefaultModel;

/**
 * Classe provedora dos dados de testes
 *
 * @name    DataSourceTest
 * @package Skel
 * @author  Author <e-mail>
 */
class DataSourceTest
{
    /**
     * Exemplo de utilização do método de retorno dos dados válidos para teste
     *
     * @name   getValidData
     * @access public static
     * @return array
     */
    public static function getValidData(array $options)
    {
        $data = [
            DefaultModel::class => [
                [
                    '' => null,
                ],
                [
                    '' => null,
                ],
            ],
            ////////////////////////////////////////////////////////////////////
            
        ];
        
        return $data[$options['model']];
    }

    /**
     * Exemplo de utilização do método de retorno dos dados inválidos para teste
     *
     * @name   getInvalidData
     * @access public static
     * @return array
     */
    public static function getInvalidData(array $options)
    {
        $data = [
            DefaultModel::class => [
                [
                    '' => null,
                ],
                [
                    '' => null,
                ],
            ],
            ////////////////////////////////////////////////////////////////////
        ];
        
        return $data[$options['model']];
    }
}
