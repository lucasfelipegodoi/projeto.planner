<?php
/**
 * Service
 */
namespace Skel\Service;

use Core\Service\Service;
use \Exception;
use Skel\Model\DefaultModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class DefaultService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($data)
    {
        $defaultModel = $this->getModel(DefaultModel::class);

        if ($this->isValid($defaultModel, $data) === false) {
            return false;
        }

        try {
            $this->beginTransaction();
            $this->saveModel($defaultModel);
            $this->commit();

            return $defaultModel;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $defaultModel = $this->getService(DefaultModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($defaultModel);
            $this->commit();

            return $result;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $defaultModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Método para retornar os itens paginados
     *
     * @name         paginate
     *
     * @param  array $parametres
     *
     * @access public
     * @return \Zend\Paginator\Paginator
     */
    public function paginate(array $parameters = [])
    {
        $select  = $this->getSelect();
        $options = [
            'select'  => $select,
            'parameters' => $parameters,
            'models'  => [
                ['model' => $this->getModel(DefaultModel::class), 'alias' => 'd'],
            ],
        ];
        
        $select->from(['d' => DefaultModel::TABLE_NAME])
            ->columns($this->getModelColumns(DefaultModel::class));
        
        return $this->generatePaginator([
            'itemCountPerPage' => self::ITEM_COUNT_PER_PAGE,
            'select'           => $select,
            'sql'              => $this->getTable(DefaultModel::class)->getSql(),
            'page'             => isset($parametres['page']) ? $parametres['page'] : 1,
        ]);
    }

    /**
     * Métodos para retornar todos os itens
     *
     * @name   getAll
     * @access public
     * @return array
     */
    public function getAll()
    {
        $select = $this->getSelect()
            ->from(['d' => DefaultModel::TABLE_NAME])
            ->columns($this->getModelColumns(DefaultModel::class));

        return $this->fetchAll($select);
    }

    /**
     * Método para retornar todos os itens como [chave => valor]
     *
     * @name         fetchPairs
     *
     * @param  array $parametros
     *
     * @access public
     * @return array
     */
    public function fetchPairs(array $parametros = [])
    {
        $defaultModel   = $this->getModel(DefaultModel::class);
        $options = [
            'tableName'   => DefaultModel::TABLE_NAME,
            'index'       => $defaultModel->getPrimaryKeyField(),
            'description' => '', //Trocar pelo campo específico
            'filters'     => $parametros,
        ];

        return parent::fetchPairs($options);
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
