<?php
/**
 * Service
 */
namespace Skel\Parameter;

use Core\Stdlib\Parameters;
use Skel\Model\DefaultModel;

/**
 *
 */
class DefaultParameter extends Parameters
{
    /**
     * Parâmetros de busca do service
     */
    protected $parameters = [
        '' => [
            'type' => self::TYPE_INT,
            'value' => null,
            'search_type' => self::SEARCH_TYPE_IN,
        ],
    ];

    public function __construct(array $values = null)
    {
        parent::__construct($values);
    }
    
      public static function getModelName()
    {
        return DefaultModel::class;
    }
}
