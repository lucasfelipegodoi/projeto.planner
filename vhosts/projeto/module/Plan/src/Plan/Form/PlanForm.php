<?php
/**
 * Form
 */
namespace Plan\Form;

use Core\Form\BaseForm;

/**
 * Formulário para incluir um item
 *
 * @name    DefaultForm
 * @package Skel\Form
 * @author  Author <e-mail>
 */
class PlanForm extends BaseForm
{
    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('id', 'plan-default-save');
        $this->setAttribute('action', '/plan/default/save');


        $this->addHidden([
            'name' => 'ID_Plan',
        ]);

        $this->addText([
            'name'  => 'Nome',
            'label' => 'Nome',
            'placeholder' => '',
            'required' => 'true',
            'data-parsley-required' => 'true',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'help-block' => 'Nome da sua plan',
        ]);


        $this->addTextarea([
            'name'  => 'Descricao',
            'label' => 'Descrição',
            'placeholder' => '',
            'class' => 'TextAreaContent',
            'style' => 'width:100%',
            'label_attributes' => [
                'class' => 'col-sm-2'
            ],
            'required' => 'true',

            'help-block' => 'Conteúdo',
        ]);


        
        $this->addSubmit([
            'class'   => 'btn btn-success',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Salvar',
        ]);

        $this->addButton([
            'name'    => 'cancel',
            'label'   => 'Cancelar',
            'class'   => 'btn btn-warning',
            'column-size' => 'sm-2 col-sm-offset-1',
            'onclick' => "goTo('/skel/default/index')",
        ]);
    }
    
    /**
     *
     * @name setData
     * @param array $data
     */
    public function setData($data)
    {
        parent::setData($data);
    }
    
    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
