<?php
/**
 * Form
 */
namespace Plan\Form;

use Core\Form\BaseForm;

/**
 * Formulário para incluir um item
 *
 * @name    DefaultForm
 * @package Skel\Form
 * @author  Author <e-mail>
 */
class CompartilharForm extends BaseForm
{
    /**
     * Construtor do formulário
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);

        $this->setAttribute('id', 'plan-default-save');
        $this->setAttribute('action', '/plan/default/compartilhar');

        $this->addHidden([
            'name' => 'fk_id_plan',
        ]);


          $this->addHidden([
                'name' => 'email_id',
            ]);

        
        $this->addSubmit([
            'class'   => 'btn btn-success',
            'column-size' => 'sm-1 col-sm-offset-1',
            'label' => 'Salvar',
        ]);
    }
    
    /**
     *
     * @name setData
     * @param array $data
     */
    public function setData($data)
    {
        parent::setData($data);
    }

    public function turnOffValidation()
    {
        parent::setValidated(true);
    }
    
    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
