<?php

/**
 * Controller
 */

namespace Plan\Controller;

use Admin\Service\UsuarioService;
use Core\Service\AutenticacaoService;
use \Exception;
use Core\Model\EntityException;
use Core\Controller\ActionController;
use Plan\Form\CompartilharForm;
use Plan\Form\PlanForm;
use Plan\Model\TipoAcessoModel;
use Plan\Service\AcessoService;
use Plan\Service\PinService;
use Plan\Service\PlanService;
use User\Service\UserService;
use Zend\View\Model\ViewModel;

use Zend\View\View;
use ZF\Apigility\Documentation\JsonModel;

/**
 * Controller Default
 *
 * @name    DefaultController
 * @package Skel\Controller
 * @author  Author <e-mail>
 */
class DefaultController extends ActionController
{
    /**
     * Método para excluir um item
     *
     * @name   deleteAction
     * @access public
     * @return void
     */
  

    /**
     * Método para gerenciar os itens
     *
     * @name   indexAction
     * @access public
     * @return Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $planService = $this->getService(PlanService::class);
        $params = $this->getParamsFromRoute();
        $plansDoUsuario = $planService->getPlansDoUsuario();
        $this->layout()->setVariable('plan',$plansDoUsuario);
        $this->layout()->setVariable('userLogado',$this->getService(AutenticacaoService::class)->getUserData());

        $viewModel->setVariables([
            'plan' => $plansDoUsuario,
        ]);

        return $viewModel;
    }

    public function saveAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(PlanService::class);
        $form = $this->getService(PlanForm::class);
        $id = $this->params()->fromRoute('plan', null);

        $viewModel->setVariables([
            'form' => $form,
        ]);

        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {

                    $this->addSuccessMessage('Ação realizada com sucesso!');

                    $this->redirect()->toUrl('/plan/default/index');
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/plan/default/index');
        }

        $viewModel->setTerminal(true);

        return $viewModel;
    }

    public function viewAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(PlanService::class);

        $id = $this->params()->fromRoute('plan', null);
        $planService = $this->getService(PlanService::class);
        $plansDoUsuario = $planService->getPlansDoUsuario();
        $this->layout()->setVariable('plan',$plansDoUsuario);
        $this->layout()->setVariable('planAtual',$id);
        $this->layout()->setVariable('userLogado',$this->getService(AutenticacaoService::class)->getUserData());

        $pinService = $this->getService(PinService::class);
        $pinsAdicionas = $pinService->renderPinsDeUmaPlan(["fk_id_plan" => $id]);




        try {
            $viewModel->setVariables([
                'item' => $defaultService->getItemById($id),
                'pinsAdicionadas' => $pinsAdicionas,
                'usuarios' => $defaultService->getUsersDaPlan($id),
                'userLogado' => $this->getService(AutenticacaoService::class)->getUserData()
            ]);

            return $viewModel;

        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/plan/default/index');
        }
    }

    public function deletePlanAction()
    {
        try {
            $pinService = $this->getService(PlanService::class);
            $plan = $this->params()->fromRoute('plan', null);

            if ($pinService->deleteById($plan)) {
                $this->addSuccessMessage('Exclusão realizada com sucesso!');
            } else {
                $this->addErrorMessage('Não foi possível executar a exclusão!');
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }

        $this->redirect()->toUrl('/plan/default/index');
    }

    public function deletePlanUserAction()
    {
        try {
            $pinService = $this->getService(AcessoService::class);
            $plan = $this->params()->fromRoute('plan', null);
            $id   = $this->params()->fromRoute('id', null);

            if ($pinService->deleteItem($id)) {
                $this->addSuccessMessage('Exclusão realizada com sucesso!');
            } else {
                $this->addErrorMessage('Não foi possível executar a exclusão!');
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }


        $this->redirect()->toUrl('/plan/default/index');
    }

    public function getAllUsuariosAction()
    {
        $userService = $this->getService(UserService::class);
        $userStr = $this->params()->fromRoute('userStr', null);

        $result = new \Zend\View\Model\JsonModel($userService->getAllUsuarios($userStr));

        return( $result );


    }

    public function compartilharAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(PlanService::class);
        $form = $this->getService(CompartilharForm::class);
        $plan = $this->params()->fromRoute('plan', null);

        $viewModel->setVariables([
            'form' => $form,
            'plan' => $plan,
        ]);

        try {

            if ($this->getRequest()->isPost()) {

                $form->setData($this->getRequest()->getPost());

                $form->turnOffValidation();

                $data = $form->data;

                $defaultService->savePlanParaUsuario([
                    "id_user" => $data["email_id"] ,
                    "id_plan" => $data["fk_id_plan"],
                    "tipo_acesso" => TipoAcessoModel::TIPO_ACESSO_ADMIN,
                ]);

                $this->addSuccessMessage('Plan compartilhada com sucesso!');

                $this->redirect()->toUrl('/plan/default/index');

            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/plan/default/index');
        }

        $viewModel->setTerminal(true);

        return $viewModel;
    }

}
