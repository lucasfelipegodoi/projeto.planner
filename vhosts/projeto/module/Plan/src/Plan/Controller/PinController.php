<?php

/**
 * Controller
 */

namespace Plan\Controller;

use \Exception;
use Core\Model\EntityException;
use Core\Controller\ActionController;
use Plan\Form\PinListaForm;
use Plan\Form\PinNotaForm;
use Plan\Form\PlanForm;
use Plan\Model\ItensListaModel;
use Plan\Model\PlanModel;
use Plan\Service\PinListaService;
use Plan\Service\PinNotaService;
use Plan\Service\PinService;
use Plan\Service\PlanService;
use Zend\View\Model\ViewModel;
use Zend\View\View;

/**
 * Controller Default
 *
 * @name    DefaultController
 * @package Skel\Controller
 * @author  Author <e-mail>
 */
class PinController extends ActionController
{

    public function saveNotaAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(PinNotaService::class);
        $form = $this->getService(PinNotaForm::class);
        $plan = $this->params()->fromRoute('plan', null);
        $id = $this->params()->fromRoute('id', null);

        $viewModel->setVariables([
            'form' => $form,
            'plan' => $plan,
        ]);

        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                }
            }

            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {
                    $this->addSuccessMessage('Ação realizada com sucesso!');
                    $this->redirect()->toUrl('/plan/default/view/plan/'.$form->getData()["fk_id_plan"]);
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/plan/default/view/plan/'.$form->getData()["fk_id_plan"]);
        }

        $viewModel->setTerminal(true);

        return $viewModel;
    }

    public function saveListaAction()
    {
        $viewModel = $this->getService(ViewModel::class);
        $defaultService = $this->getService(PinListaService::class);
        $form = $this->getService(PinListaForm::class);
        $plan = $this->params()->fromRoute('plan', null);
        $id = $this->params()->fromRoute('id', 0);
        $itens = $defaultService->getItensLista($id);
        $viewModel->setVariables([
            'form' => $form,
            'plan' => $plan,
        ]);

        try {
            if ($this->getRequest()->isPost() === false) {
                if ($id) {
                    $form->bind($defaultService->getItemById($id));
                       ;
                }
            }

            $viewModel->setVariables([
                'itens' => $itens,
            ]);


            if ($this->getRequest()->isPost()) {
                $form->setData($this->getRequest()->getPost());

                if ($defaultService->save($form)) {
                    $this->addSuccessMessage('Ação realizada com sucesso!');
                    $this->redirect()->toUrl('/plan/default/view/plan/'.$form->getData()["fk_id_plan"]);
                }
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
            $this->redirect()->toUrl('/plan/default/view/plan/'.$form->getData()["fk_id_plan"]);
        }

        $viewModel->setTerminal(true);

        return $viewModel;
    }

    public function updateItemListaAction()
    {
        try {
            $pinService = $this->getService(PinListaService::class);
            $id = $this->params()->fromRoute('id', 0);
            $checked = $this->params()->fromRoute('checked', 0);

            $pinService->checkUncheckItemLista(["itemID" => $id, "flCheck" => $checked]);

            $result = new JsonModel(array(
                'success'=>true,
            ));

            return( $result );

        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }
    }


    public function deletePinAction()
    {
        try {
            $pinService = $this->getService(PinService::class);
            $id = $this->params()->fromRoute('id', 0);
            $plan = $this->params()->fromRoute('plan', null);

            if ($pinService->deletePinDaPlan(["ID_PlanxPin" => $id])) {
                $this->addSuccessMessage('Exclusão realizada com sucesso!');
            } else {
                $this->addErrorMessage('Não foi possível executar a exclusão!');
            }
        } catch (Exception $exception) {
            $this->addExceptionMessage($exception);
        }

        $this->redirect()->toUrl('/plan/default/view/plan/'.$plan);

    }

}
