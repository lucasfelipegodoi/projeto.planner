<?php
/**
 * Model
 */
namespace Plan\Model;

use Core\Model\Entity;
use Zend\InputFilter\InputFilter;

/**
 * Classe de representação de um modelo
 *
 * @name    DefaultModel
 * @package Skel\Model
 * @author  Author <e-mail>
 */
class DefaultModel extends Entity
{

    /**
     * Name of table
     *
     * @var string
     */
    protected $tableName = self::TABLE_NAME;

    /**
     * Primary key of table
     *
     * @var string
     */
    protected $primaryKeyField = '';
    
    /**
     * Name of table
     *
     * @var static
     */
    const TABLE_NAME = '';

    /**
     * Construtor do modelo
     *
     * @name         __construct
     *
     * @param  mixed $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
        parent::__construct($options);
    }

    /**
     * Método para setar os dados no modelo
     *
     * @name         setData
     *
     * @param  mixed $data
     *
     * @access public
     * @return void
     */
    public function setData($data)
    {
        $this->executeDependentRoutines($data);
        parent::setData($data);
    }

    /**
     * Configura os filters e validators do modelo
     *
     * @name   getInputFilter
     *
     * @param array $data
     * @access public
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter($data = [])
    {
        if ($this->inputFilter instanceof InputFilter) {
            return $this->inputFilter;
        }

        $this->createInputFilterFactory()
                ->addInputFilterPrimaryKey()
                ->addAllInputFilterForeignKey()
                ->addInputFilterToInsertAndUpdate();

        $this->addInputFilterMediumText([
            'name' => '',
            'required' => true,
        ]);
        
        return $this->inputFilter;
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
    
    /**
     * @name executeDependentRoutines
     * @param array|object $data
     */
    protected function executeDependentRoutines($data)
    {
        $data = $this->normalizeDataSource($data);
        $this->getInputFilter($data);
        $isUpdate = isset($data[$this->primaryKeyField]) && $data[$this->primaryKeyField];
    
        if ($isUpdate === true) {
            $this->inputFilter->get($this->primaryKeyField)->setRequired(true);
        }
    }
}
