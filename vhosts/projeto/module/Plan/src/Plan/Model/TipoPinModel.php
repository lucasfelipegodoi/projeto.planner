<?php
/**
 * Model
 */
namespace Plan\Model;

use Core\Model\Entity;
use Zend\InputFilter\InputFilter;

/**
 * Classe de representação de um modelo
 *
 * @name    PacienteModel
 * @package Paciente\Model
 * @author  Lucas Godoi <e-mail>
 */
class TipoPinModel extends Entity
{

    /**
     * Name of table
     *
     * @var string
     */
    protected $tableName = self::TABLE_NAME;

    /**
     * Primary key of table
     *
     * @var string
     */
    protected $primaryKeyField = 'ID_Tipo_Pin';
    
    /**
     *
     * @var int
     */
    protected $ID_Tipo_Pin;
    protected $Nome;

    const TIPO_PIN_NOTA = 1;

    const TIPO_PIN_LISTA = 2;

    /**
     *
     * @var string
     */

    /**
     * Name of table
     *
     * @var static
     */
    const TABLE_NAME = 'tipo_pin';

    /**
     * Construtor do modelo
     *
     * @name         __construct
     *
     * @param  mixed $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para setar os dados no modelo
     *
     * @name         setData
     *
     * @param  mixed $data
     *
     * @access public
     * @return void
     */
    public function setData($data)
    {
        parent::setData($data);
        
        if ($this->isUpdate()) {
            $this->inputFilter->get($this->primaryKeyField)->setRequired(true);
        }
        
        parent::setData($data);
    }

    /**
     * Configura os filters e validators do modelo
     *
     * @name   getInputFilter
     * @access public
     * @return \Zend\InputFilter\InputFilter
     */
    public function getInputFilter()
    {
        if ($this->inputFilter instanceof InputFilter) {
            return $this->inputFilter;
        }

        $this->createInputFilterFactory();

        $this->inputFilterAdd([
            'name'       => $this->primaryKeyField,
            'required'   => false,
            'filters'    => [
                ['name' => 'ToInt'],
            ]
        ]);

        $this->inputFilterAdd([
            'name'       => 'Nome',
            'required'   => true,
            'filters'    => [
                ['name' => 'StripTags'],
                ['name' => 'StringTrim'],
            ],
            'validators' => [
                $this->getStringLengthValidator([
                    'min' => 4,
                    'max' => 80,
                ]),

            ],
        ]);



        return $this->inputFilter;
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
