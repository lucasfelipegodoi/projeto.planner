<?php
/**
 * Service
 */
namespace Plan\Service;

use Core\Service\AutenticacaoService;
use Core\Service\Service;
use \Exception;
use Plan\Model\AcessoModel;
use Plan\Model\PlanModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class AcessoService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($defaultModel)
    {
        try {
            $this->beginTransaction();
            $this->saveModel($defaultModel);
            $this->commit();
            return $defaultModel;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }


    public function deleteItem($id)
    {
        $defaultModel = $this->getService(AcessoModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($defaultModel);
            $this->commit();

            return true;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }


    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
