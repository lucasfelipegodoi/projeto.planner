<?php
/**
 * Service
 */
namespace Plan\Service;

use Core\Service\AutenticacaoService;
use Core\Service\Service;
use \Exception;
use Plan\Model\NotaModel;
use Plan\Model\TipoPinModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class PinNotaService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($data)
    {
        $defaultModel = $this->getModel(NotaModel::class);


        try {
            if ($this->isValid($defaultModel, $data) === false) {

                $message = "Não foi possível salvar o Pin";
               throw new \Hoa\Core\Exception\Exception($message);
            }

            $this->beginTransaction();

            $novaNota = $defaultModel->isInsert();

            $this->saveModel($defaultModel);

            if($novaNota) {
                $pinService = $this->getService(PinService::class);

                $pinService->referenciaPinAUmaPlan(["FK_ID_Tipo_Pin" => TipoPinModel::TIPO_PIN_NOTA,
                    "FK_ID_Plan" => $data->getData()["fk_id_plan"],
                    "Fk_ID_Pin" => $defaultModel->ID_Nota
                ]);
            }
            $this->commit();
            return $defaultModel;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $defaultModel = $this->getService(DefaultModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($defaultModel);
            $this->commit();

            return $result;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $defaultModel = $this->getModel(NotaModel::class);
        $defaultModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function renderPin(array $option)
    {
        $pin = $option['pin'];

        return '<div class="col-md-4">
          <div class="box box-default">
            <div class="box-header with-border">
              <h3 class="box-title">'.$pin->Nome.'</h3>
              <div class="box-tools pull-right">
              <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-sort-down"></i></button>
                  <ul class="dropdown-menu" role="menu">
                        <li><a href="#myModal" data-toggle="modal" onclick="renderViewOnModal(\'/plan/pin/save-nota/plan/'.$option['fk_id_plan'].'/id/'.$pin->ID_Nota.'\')" >Editar</a></li>
                        <li><a href="/plan/pin/delete-pin/id/'.$option['ID_PlanxPin'].'/plan/'.$option['fk_id_plan'].'" data-toggle="modal"  >Excluir</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
              '.$pin->Conteudo.'
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>';
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
