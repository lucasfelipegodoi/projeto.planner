<?php
/**
 * Service
 */
namespace Plan\Service;

use Core\Service\AutenticacaoService;
use Core\Service\Service;
use \Exception;
use Plan\Model\ItensListaModel;
use Plan\Model\ListaModel;
use Plan\Model\NotaModel;
use Plan\Model\TipoPinModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class PinListaService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    /**
     * Método para salvar um item
     *
     * @name   save
     *
     * @param  $data
     *
     * @access public
     * @return Core\Model\Entity
     * @throws \Exception
     */
    public function save($data)
    {
        $defaultModel = $this->getModel(ListaModel::class);


        try {
            if ($this->isValid($defaultModel, $data) === false) {

                $message = "Não foi possível salvar o Pin";
               throw new \Hoa\Core\Exception\Exception($message);
            }

            $this->beginTransaction();

            $novaNota = $defaultModel->isInsert();

            $this->saveModel($defaultModel);

            $itens = explode("¢", $data->getData()["relacaoLista"]);

            if($novaNota == false)
            {
                $itensAntigos = $this->getItensLista($defaultModel->ID_Lista);

                foreach($itensAntigos as $itemAntigo) {
                    $this->deleteModel(['fullNamespaceModel' => ItensListaModel::class, 'where' => $itemAntigo]);
                }
            }



            $base = $this->getService(ItensListaModel::class);

            foreach($itens as $item)
            {
                if($item == "") continue;

                $n = clone $base;
                $n->Descricao = $item;
                $n->FK_ID_Lista = $defaultModel->ID_Lista;
                $n->Deletado = 0;
                $n->checked = 0;

                $this->saveModel($n);
            }

            if($novaNota) {
                $pinService = $this->getService(PinService::class);

                $pinService->referenciaPinAUmaPlan(["FK_ID_Tipo_Pin" => TipoPinModel::TIPO_PIN_LISTA,
                    "FK_ID_Plan" => $data->getData()["fk_id_plan"],
                    "Fk_ID_Pin" => $defaultModel->ID_Lista
                ]);
            }


            $this->commit();
            return $defaultModel;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para excluir um item
     *
     * @name   delete
     *
     * @param  $id
     *
     * @access public
     * @return bool
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $defaultModel = $this->getService(DefaultModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            $this->beginTransaction();
            $result = $this->deleteModel($defaultModel);
            $this->commit();

            return $result;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    /**
     * Método para retornar um item
     *
     * @name   getItemById
     *
     * @param  $id
     *
     * @access public
     * @return Core\Model\Entity|Exception
     */
    public function getItemById($id)
    {
        $defaultModel = $this->getModel(ListaModel::class);
        $defaultModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function getItemLista($id)
    {
        $defaultModel = $this->getModel(ItensListaModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }

    }

    public function checkUncheckItemLista(array $options)
    {

        $item = $this->getItemLista($options["itemID"]);
        $item->checked = $options["flCheck"];
        $item->ID_itens_lista = $options["itemID"];

        $this->saveModel($item);
    }

    public function getItensLista($fkID)
    {
        $select = $this->getSelect();
        $select->from(ItensListaModel::TABLE_NAME)
            ->where(["FK_ID_Lista" => $fkID]);

        return $this->fetchAll($select);
    }

    public function renderPin(array $option)
    {
        $pin = $option['pin'];

        $html =  '<div class="col-md-4">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">'.$pin->Nome.'</h3>
              <div class="box-tools pull-right">
              <div class="btn-group">
                  <button type="button" class="btn btn-box-tool dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <i class="fa fa-sort-down"></i></button>
                  <ul class="dropdown-menu" role="menu">
                        <li><a href="#myModal" data-toggle="modal" onclick="renderViewOnModal(\'/plan/pin/save-lista/plan/'.$option['fk_id_plan'].'/id/'.$pin->ID_Lista.'\')" >Editar</a></li>
                        <li><a href="/plan/pin/delete-pin/id/'.$option['ID_PlanxPin'].'/plan/'.$option['fk_id_plan'].'" data-toggle="modal"  >Excluir</a></li>
                  </ul>
                </div>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
                  <div class="col-lg-12">
                            <ul class="todo-list">';
            foreach($option["pinItens"] as $item) {
              $html .=  '<li '.($item["checked"]==1 ? "class=\"done\"":"").'>
                            <input type="checkbox" '.($item["checked"]==1 ? "checked":"").' value="'.$item["ID_Itens_Lista"].'">
                            <span class="text">'.$item["Descricao"].'</span>
                         </li>';
            }
            $html .='</ul>
                  </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>';

        return $html;
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
