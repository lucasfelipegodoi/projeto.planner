<?php
/**
 * Service
 */
namespace Plan\Service;

use Core\Service\AutenticacaoService;
use Core\Service\Service;
use \Exception;
use Plan\Model\AcessoModel;
use Plan\Model\PlanModel;
use Plan\Model\TipoAcessoModel;
use User\Model\UserModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class PlanService extends Service
{
    public function __construct(array $options = [])
    {
    }

    public function save($data)
    {
        $defaultModel = $this->getModel(PlanModel::class);


        try {
            if ($this->isValid($defaultModel, $data) === false) {

                $message = "Não foi possível salvar o Plan";
               throw new \Hoa\Core\Exception\Exception($message);
            }

            $defaultModel->DT_Registro = date('Y-m-d H:i:s');
            $defaultModel->Deletado = 0;

            $defaultModel->Fk_id_usuario = $this->getService(AutenticacaoService::class)->getUserID();
            $this->beginTransaction();
            $this->saveModel($defaultModel);

            if($defaultModel->isInsert())
            {
                $this->savePlanParaUsuario([
                   "id_user" =>  $defaultModel->Fk_id_usuario,
                    "id_plan" => $defaultModel->ID_Plan,
                    "tipo_acesso" => TipoAcessoModel::TIPO_ACESSO_ADMIN,
                ]);

            }

            $this->commit();
            return $defaultModel;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    public function deleteById($id)
    {
        $defaultModel = $this->getItemById($id);
        $defaultModel->Deletado = 1;

        try {
            $this->beginTransaction();
            $result = $this->saveModel($defaultModel);
            $this->commit();
            return true;
        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }

    public function getItemById($id)
    {
        $defaultModel = $this->getModel(PlanModel::class);
        $defaultModel->setPrimaryKeyValue($id);
        
        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    public function savePlanParaUsuario(array $options)
    {

        if(isset($options["id_user"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de id_user é obrigatório");
        }

        if(isset($options["id_plan"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de id_plan é obrigatório");
        }

        if(isset($options["tipo_acesso"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de tipo_acesso é obrigatório");
        }

        $acesso = $this->getService(AcessoModel::class);
        $acesso->FK_ID_Usuario = $options["id_user"];
        $acesso->FK_ID_Plan = $options["id_plan"];
        $acesso->FK_ID_Tipo_Acesso = $options["tipo_acesso"];

        $this->getService(AcessoService::class)->save($acesso);
    }


    public function getPlansDoUsuario()
    {
        $select = $this->getSelect()
            ->from(['d' => AcessoModel::TABLE_NAME])
            ->join([ 'p' =>PlanModel::TABLE_NAME],'ID_Plan = Fk_id_plan',$this->getModelColumns(PlanModel::class))
            ->where(['d.fk_id_usuario' => $this->getService(AutenticacaoService::class)->getUserID(),
                'p.Deletado' => 0]);

        return $this->fetchAll($select);
    }

    public function getUsersDaPlan($idPlan)
    {

        $select = $this->getSelect();
        $select->from(['a' => AcessoModel::TABLE_NAME])
            ->join([ 'u' =>UserModel::TABLE_NAME],'u.ID_Usuario = a.FK_ID_Usuario',$this->getModelColumns(UserModel::class))
            ->columns(["ID_Acesso"])
            ->where(['a.FK_ID_Plan' => $idPlan]);

        return $this->fetchAll($select);
    }

    public function getClassName()
    {
        return self::class;
    }
}
