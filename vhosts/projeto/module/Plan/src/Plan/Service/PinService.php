<?php
/**
 * Service
 */
namespace Plan\Service;

use Core\Service\AutenticacaoService;
use Core\Service\Service;
use \Exception;
use Plan\Model\NotaModel;
use Plan\Model\PlanxPinModel;
use Plan\Model\TipoPinModel;

/**
 * Classes com as regras de négocio
 *
 * @name       DefaultService
 * @package    Skel\Service
 * @author     Author <e-mail>
 */
class PinService extends Service
{
    /**
     * Construtor da classe
     *
     * @name         __construct
     *
     * @param  array $options = array()
     *
     * @access public
     * @return void
     */
    public function __construct(array $options = [])
    {
    }

    public function referenciaPinAUmaPlan(array $option)
    {
        if(isset($option["FK_ID_Tipo_Pin"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de FK_ID_Tipo_Pin é obrigatório");
        }
        if(isset($option["FK_ID_Plan"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de FK_ID_Tipo_Pin é obrigatório");
        }

        if(isset($option["Fk_ID_Pin"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de Fk_ID_Pin é obrigatório");
        }

        $pinxplan = $this->getService(PlanxPinModel::class);
        $pinxplan->FK_ID_Tipo_Pin = $option["FK_ID_Tipo_Pin"];
        $pinxplan->FK_ID_Plan = $option["FK_ID_Plan"];
        $pinxplan->Fk_ID_Pin = $option["Fk_ID_Pin"];
        $pinxplan->Deletado = 0;

        try {
            $this->beginTransaction();
            $this->saveModel($pinxplan);
            $this->commit();
            return $pinxplan;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }

    }

    public function getPinsDeUmaPlan(array $options)
    {
        if(isset($options["fk_id_plan"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de fk_id_plan é obrigatório");
        }

        $select = $this->getSelect();
        $select->from(["p" => PlanxPinModel::TABLE_NAME])
            ->where(["p.FK_ID_Plan" => $options["fk_id_plan"],
                    "p.Deletado" => 0]);

        return $this->fetchAll($select);
    }


    public function renderPinsDeUmaPlan(array $options)
    {
        if(isset($options["fk_id_plan"]) == false)
        {
            throw new \Hoa\Core\Exception\Exception("O valor de fk_id_plan é obrigatório");
        }

        $allPins = $this->getPinsDeUmaPlan($options);

        $htmlPins = array();

        foreach($allPins as $pin)
        {
            if($pin["FK_ID_Tipo_Pin"] == TipoPinModel::TIPO_PIN_NOTA)
            {
                $pinNotaService = $this->getService(PinNotaService::class);

                $nota = $pinNotaService->getItemById($pin["Fk_ID_Pin"]);

                $htmlPins[] = $pinNotaService->renderPin(["pin"=>$nota,
                                                    "fk_id_plan"=>$options["fk_id_plan"],
                                                    "ID_PlanxPin" => $pin["ID_PlanxPin"]]);
            }

            if($pin["FK_ID_Tipo_Pin"] == TipoPinModel::TIPO_PIN_LISTA)
            {
                $pinListService = $this->getService(PinListaService::class);
                $list = $pinListService->getItemById($pin["Fk_ID_Pin"]);
                $itensList = $pinListService->getItensLista($pin["Fk_ID_Pin"]);

                $htmlPins[] = $pinListService->renderPin(["pin"=>$list,"pinItens" => $itensList,
                    "fk_id_plan"=>$options["fk_id_plan"],
                    "ID_PlanxPin" => $pin["ID_PlanxPin"]]);
            }
        }

        return $htmlPins;
    }

    public function deletePinDaPlan(array $options)
    {

        try {

            $this->beginTransaction();
            $pinxplan = $this->getItemById($options["ID_PlanxPin"]);
            $pinxplan->Deletado = 1;
            $this->saveModel($pinxplan);
            $this->commit();

            return true;

        } catch (Exception $exception) {
            $this->rollback();
            throw $exception;
        }
    }
    public function getItemById($id)
    {
        $defaultModel = $this->getModel(PlanxPinModel::class);
        $defaultModel->setPrimaryKeyValue($id);

        try {
            return $this->get($defaultModel);
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Método para retornar o name space de um objeto
     *
     * @name   getClassName
     * @access public
     * @return string
     */
    public function getClassName()
    {
        return self::class;
    }
}
