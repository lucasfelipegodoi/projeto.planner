<?php
/**
 * Module
 */

namespace Plan;

use Core\Service\AutenticacaoService;
use Zend\Mvc\ModuleRouteListener;

/**
 * Configurações do módulo
 *
 * @name    Module
 * @package Skel
 * @author  Author <e-mail>
 */
class Module
{

    /**
     * Configurações de boot do módulo
     *
     * @name         onBootstrap
     *
     * @param  array $event
     *
     * @access public
     * @return void
     */
    public function onBootstrap($event)
    {
        $event->getApplication()->getServiceManager()->get('translator');
        $eventManager        = $event->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

    }

    /**
     * Retorna o array de configurações do módulo
     *
     * @name   getConfig
     * @access public
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * Retorna o autoloader config do módulo
     *
     * @name   getAutoloaderConfig
     * @access public
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
