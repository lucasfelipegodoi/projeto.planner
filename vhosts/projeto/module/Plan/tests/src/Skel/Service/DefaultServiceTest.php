<?php
/**
 * Service
 */
namespace Skel\Service;

use Core\Test\ServiceTestCase;
use Skel\DataSourceTest;
use Skel\Model\DefaultModel;
use Zend\Paginator\Paginator;

/**
 * Testes do service
 *
 * @group   Service
 *
 * @name    DefaultServiceTest
 * @package Skel\Service
 * @author  Author <e-mail>
 */
class DefaultServiceTest extends ServiceTestCase
{

    /**
     * Namespace completo do Model
     *
     * @var string
     */
    //protected $modelFQDN = DefaultModel::class;

    /**
     * @var
     */
    protected $defaultService;

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->defaultService = $this->getService(DefaultService::class);
        $this->executeDependentRoutines();
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Teste para verficar se todos os itens são retornados
     *
     * @name   testGetAll
     * @access public
     * @return void
     */
    public function testGetAll()
    {
        $parametros = DataSourceTest::getValidData(['model' => DefaultModel::class])[0];
        
        $totalItens = $this->executeSave();
        
        $this->assertEquals($totalItens, count($this->defaultService->getAll()));
        
    }

    /**
     * Teste para verficar a inclusão/alteração de um item inválido
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testSaveInvalid
     * @access public
     * @return void
     */
    public function testSaveInvalid()
    {
        $result = $this->defaultService->save([
            'dataSource' => DataSourceTest::getInvalidData(['model' => DefaultModel::class])[0],
        ]);

        $this->assertFalse($result);
    }

    /**
     * Teste para verficar a exclusão de um item
     *
     * @name   testDeleteById
     * @access public
     * @return void
     */
    public function testDeleteById()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $result = $this->defaultService->deleteById(1);

        $this->assertTrue($result);
    }

    /**
     * /**
     * Teste para verficar o retorno de um item
     *
     * @name   testGetItemById
     * @access public
     * @return void
     */
    public function testGetItemById()
    {
        $numberOfItems = 1;
        $this->executeSave($numberOfItems);
        $item = $this->defaultService->getItemById(1);

        $this->assertEquals(1, $item->getPrimaryKeyValue());
    }

    /**
     * Teste para verficar a paginação dos dados
     *
     * @name   testPaginate
     * @access public
     * @return void
     */
    public function testPaginate()
    {
        $this->executeSave();
        $paginator = $this->defaultService->paginate();

        $this->assertInstanceOf(Paginator::class, $paginator);
    }

    /**
     * Teste para verficar o método [chave => valor]
     *
     * @name
     * @access public
     * @return void
     */
    public function testFetchPairs()
    {
        $totalItens = $this->executeSave();
        $pairs      = $this->defaultService->fetchPairs();

        $this->assertEquals($totalItens, count($pairs));
    }

    /**
     *
     * @name   executeSave
     *
     * @param int $numberOfItems Número de registros que serão inseridos
     * @access private
     * @return int
     */
    private function executeSave($numberOfItems = null)
    {
        $itens      = DataSourceTest::getValidData(['model' => DefaultModel::class]);
        $savedItens = $numberOfItems === null ? count($itens) : $numberOfItems;
        
        foreach ($itens as $item) {
            if ($savedItens == 0) {
                break;
            }
            
            $this->defaultService->save([
                'dataSource' => $item,
            ]);
            
            $savedItens--;
        }

        return count($itens);
    }
    
    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        //Coloque aqui as rotinas que deverão ser executadas antes dos testes iniciarem
        //Por exemplo: $this->migrationService->runSeed('NiveisDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
