<?php
/**
 * Model
 */
namespace Skel\Model;

use Core\Test\ModelTestCase;
use Skel\DataSourceTest;
use Skel\Model\DefaultModel;
use Zend\InputFilter\InputFilterInterface;

/**
 * Testes do model
 *
 * @group   Model
 *
 * @name    DefaultModelTest
 * @package Skel\Model
 * @author  Author <e-mail>
 */
class DefaultModelTest extends ModelTestCase
{

    /**
     * Namespace completo do Model
     *
     * @var string
     */
    protected $modelFQDN = DefaultModel::class;

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        parent::setup();
        $this->executeDependentRoutines();
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }


    /**
     * Retorna os inputs filters do model
     *
     * @name   testGetInputFilter
     * @access public
     * @return void
     */
    public function testGetInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $inputFilter  = $defaultModel->getInputFilter();

        $this->assertInstanceOf(InputFilterInterface::class, $inputFilter);
    }

    /**
     * Testa se os input filters são válidos
     *
     * @name   testValidInputFilter
     * @access  public
     * @return void
     */
    public function testValidInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $inputFilter  = $defaultModel->getInputFilter();
        
        $this->assertTrue($inputFilter->has($defaultModel->getPrimaryKeyField()));
        $this->assertTrue($inputFilter->has('description'));
    }

    /**
     * Verifica se um input filter inválido lança uma exceção.
     *
     * @expectedException Core\Model\EntityException
     *
     * @name   testInvalidInputFilter
     * @access public
     * @return void
     */
    public function testInvalidInputFilter()
    {
        $defaultModel = $this->getModel(DefaultModel::class);
        $defaultModel->setData(DataSourceTest::getInvalidData(['model' => DefaultModel::class])[0]);
    }
    
    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        if ($this->mockShouldBeUsedForModel) {
            return 1;
        }
                
        //Coloque aqui as rotinas que deverão ser executadas antes dos testes iniciarem
    }
}
