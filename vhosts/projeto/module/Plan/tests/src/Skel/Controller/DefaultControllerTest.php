<?php
/**
 * Controller
 */
namespace Skel\Controller;

use Core\Test\ControllerTestCase;
use Skel\DataSourceTest;
use Skel\Model\DefaultModel;
use Zend\View\Model\ViewModel;

/**
 * Testes do controller
 *
 * @group   Controller
 *
 * @name    DefaultControllerTest
 * @package Skel\Controller
 * @author  Author <e-mail>
 */
class DefaultControllerTest extends ControllerTestCase
{

    /**
     * Namespace completa do Controller
     *
     * @var string
     */
    protected $controllerFQDN = DefaultController::class;

    /**
     * Nome da rota padrão definida no arquivo module.config
     *
     * @var string
     */
    protected $controllerRoute = 'skel';

    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->executeDependentRoutines();
    }

    /**
     * Rotinas que serão executadas ao final do teste
     *
     * @name   tearDown
     * @access public
     * @return void
     */
    public function tearDown()
    {
        parent::tearDown();
    }

    /**
     * Teste da action de exclusão
     *
     * @name   testDeleteAction
     * @access public
     * @return void
     */
    public function testDeleteAction()
    {
        $this->executeSaveAction();
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'delete',
                'id'     => 1,
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(200, $result['response']->getStatusCode());
    }

    /**
     * Teste da action de gerenciamento
     *
     * @name   testIndexAction
     * @access public
     * @return void
     */
    public function testIndexAction()
    {
        $numberOfItems = count(DataSourceTest::getValidData(['model' => DefaultModel::class]));
        $this->executeSaveAction($numberOfItems);
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'index',
            ],
        ]);

        $viewModel = $this->controller->dispatch($this->request, $this->response);
        $result    = $this->controller->getResponse();
        $paginator = $viewModel->getVariable('paginator');

        $this->assertEquals($numberOfItems, $paginator->getTotalItemCount());
        $this->assertEquals(200, $result->getStatusCode());
    }

    /**
     * Teste da action de inserção e atualização
     *
     * @name   testSaveAction
     * @access public
     * @return void
     */
    public function testSaveAction()
    {
        $result = $this->executeSaveAction();

        $this->assertEquals(302, $result['response']->getStatusCode());
        $this->assertEquals('Location: /skel/default/index', $result['headers']->get('Location'));
    }
    
       /**
     *
     *
     * @name   testSaveUpdateAction
     * @access public
     * @return void
     */
    public function testSaveUpdateAction()
    {
        $this->executeSaveAction();
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'save',
                'id'     => 1,
           
            ],
        ]);

        $result = $this->getResponse();

        $this->assertEquals(200, $result['response']->getStatusCode());
    }
    


    /**
     * Teste da action de visualização
     *
     * @name   testViewAction
     * @access public
     * @return void
     */
    public function testViewAction()
    {
        $this->executeSaveAction();
        $this->setRouteMatch([
            'method' => 'get',
            'params' => [
                'action' => 'view',
                'id'     => 1,
            ],
        ]);

        $viewModel = $this->controller->dispatch($this->request, $this->response);
        $result    = $this->controller->getResponse();

        $this->assertInstanceOf(ViewModel::class, $viewModel);
        
        if ($viewModel instanceof ViewModel) {
            $item      = $viewModel->getVariable('item');
            $this->assertEquals(1, $item->getPrimaryKeyValue());
            $this->assertEquals(200, $result->getStatusCode());
        }
    }

    /**
     * Método padrão para executar a ação que salvar o ite,
     *
     * @name      executeSaveAction
     *
     * @param int $numberOfItems = 1
     *
     * @return array|null
     */
    private function executeSaveAction($numberOfItems = 1)
    {
        $result = null;
        $data   = DataSourceTest::getValidData(['model' => DefaultModel::class]);

        for ($count = 0; $count < $numberOfItems; $count++) {
            $this->setRouteMatch([
                'method' => 'post',
                'params' => [
                    'action' => 'save',
                ],
            ]);

            $post = $this->request->getPost();

            foreach ($data[$count] as $key => $value) {
                $post->set($key, $value);
            }

            $result = $this->getResponse();
        }

        return $result;
    }
    
    /**
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        //Coloque aqui as rotinas que deverão ser executadas antes dos testes iniciarem
        //Por exemplo: $this->migrationService->runSeed('NiveisDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
