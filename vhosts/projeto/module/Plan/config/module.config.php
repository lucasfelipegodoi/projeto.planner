<?php

use Plan\Model\PlanModel;
use Plan\Form\PlanForm;
use Plan\Service\PlanService;
use Plan\Model\AcessoModel;
use Plan\Model\AnexoModel;
use Plan\Model\CalendarioModel;
use Plan\Model\Evento_CalendarioModel;
use Plan\Model\InteracaoModel;
use Plan\Model\ItensListaModel;
use Plan\Model\ListaModel;
use Plan\Model\NotaModel;
use Plan\Model\PlanxPinModel;
use Plan\Model\StatusModel;
use Plan\Model\TarefaModel;
use Plan\Model\TipoAcessoModel;
use Plan\Model\TipoPinModel;
use Plan\Service\AcessoService;
use Plan\Form\PinNotaForm;
use Plan\Form\PinListaForm;
use Plan\Form\CompartilharForm;
use Plan\Service\PinNotaService;
use Plan\Service\PinListaService;
use Plan\Service\PinService;



return [
    'controllers'     => [
        'invokables' => [
            'Plan\Controller\Default' => 'Plan\Controller\DefaultController',
            'Plan\Controller\Pin' => 'Plan\Controller\PinController'
        ],
    ],
    'service_manager' => [
        'factories' => [
            'Plan\Model\AcessoModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new AcessoModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\AnexoModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new AnexoModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\CalendarioModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new CalendarioModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\Evento_CalendarioModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new Evento_CalendarioModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\InteacaoModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new InteracaoModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\ItensListaModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new ItensListaModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\ListaModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new ListaModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\NotaModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new NotaModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\PlanxPinModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PlanxPinModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\StatusModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new StatusModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\TarefaModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new TarefaModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\TipoAcessoModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new TipoAcessoModel($options);
            },

            // ---------------- Factories Models ---------------------------- //

            'Plan\Model\TipoPinModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new TipoPinModel($options);
            },
            'Plan\Model\PlanModel' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PlanModel($options);
            },

            /***FORMS **/
                'Plan\Form\PlanForm' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PlanForm($options);
            },
                'Plan\Form\PinNotaForm' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PinNotaForm($options);
            },

            'Plan\Form\PinListaForm' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PinListaForm($options);
            },

            'Plan\Form\CompartilharForm' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new CompartilharForm($options);
            },


            /*** SERVICES **/
            'Plan\Service\PlanService' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PlanService($options);
            },

            'Plan\Service\AcessoService' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new AcessoService($options);
            },

            'Plan\Service\PinNotaService' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PinNotaService($options);
            },

            'Plan\Service\PinListaService' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PinListaService($options);
            },
            'Plan\Service\PinService' => function ($serviceManager) {

                $options = [
                    'serviceManager' => $serviceManager
                ];

                return new PinService($options);
            },


        ],
    ],
    'router'          => [
        'routes' => [
            'home' => [
                'type'    => 'Segment',
                'options' => [
                    'route'    => '/[page/:page]',
                    'defaults' => [
                        'controller' => 'Plan\Controller\Default',
                        'action'     => 'index',
                        'module'     => 'plan',
                    ],
                ],
            ],
            'plan' => [
                'type'          => 'Literal',
                'options'       => [
                    'route'    => '/plan',
                    'defaults' => [
                        'controller'    => 'Default',
                        'action'        => 'index',
                        '__NAMESPACE__' => 'Plan\Controller',
                        'module'        => 'plan'
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'default' => [
                        'type'         => 'Segment',
                        'options'      => [
                            'route'       => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ],
                            'defaults'    => [],
                        ],
                        'child_routes' => [ //permite mandar dados pela url
                            'wildcard' => [
                                'type' => 'Wildcard'
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],
    'view_manager'    => [
        'display_not_found_reason' => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'display_exceptions'       => defined('APPLICATION_ENV') ? APPLICATION_ENV == 'development' : false,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'             => [
            'partials/paginator/control' => __DIR__ . '/../../Core/view/partials/paginator/control.phtml',
            'layout/layout'              => __DIR__ . '/../../Core/view/layout/layout.phtml',
            'layout/layout_email'        => __DIR__ . '/../../Core/view/layout/layout_email.phtml',
            'error/404'                  => __DIR__ . '/../../Core/view/error/404.phtml',
            'error/index'                => __DIR__ . '/../../Core/view/error/index.phtml',
        ],
        'template_path_stack'      => [
            'plan' => __DIR__ . '/../view',
        ],
    ],
];
