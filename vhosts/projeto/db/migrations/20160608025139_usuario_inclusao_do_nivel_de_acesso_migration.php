<?php
use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class UsuarioInclusaoDoNivelDeAcessoMigration extends AbstractMigration
{
    private $tableName = 'tb_usuarios';
    
    private $tableNameForeign = 'tb_niveis_de_acesso';
    
    public function up()
    {
        $foreignKeyOptions = ['delete' => 'CASCADE', 'update' => 'NO_ACTION'];
        $usuarios = $this->table($this->tableName);
        $usuarios->addColumn('fk_id_nivel_de_acesso', MysqlAdapter::PHINX_TYPE_INTEGER, ['null' => false, 'after' => 'pk_id_usuario'])
                ->addForeignKey('fk_id_nivel_de_acesso', $this->tableNameForeign, 'pk_id_nivel_de_acesso', $foreignKeyOptions)
                ->save();
    }
    
    public function down()
    {
        $this->table($this->tableName)->dropForeignKey('fk_id_nivel_de_acesso');        
    }
}
