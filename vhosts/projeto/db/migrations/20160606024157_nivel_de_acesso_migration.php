<?php
use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class NivelDeAcessoMigration extends AbstractMigration
{
    private $tableName = 'tb_niveis_de_acesso';
  
    public function up()
    {
        $niveisDeAcesso = $this->table($this->tableName, ['id' => 'pk_id_nivel_de_acesso']);
        $niveisDeAcesso->addColumn('nivel_de_acesso', MysqlAdapter::PHINX_TYPE_STRING, ['limit' => 255, 'null' => false])
                ->addIndex(['nivel_de_acesso'], ['unique' => true])
                ->save();
    }
    
    public function down()
    {
        $this->dropTable($this->tableName);
    }
}
