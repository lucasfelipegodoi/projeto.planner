<?php
use Phinx\Migration\AbstractMigration;
use Phinx\Db\Adapter\MysqlAdapter;

class UsuarioMigration extends AbstractMigration
{
    private $tableName = 'tb_usuarios';
    
    public function up() 
    {
       $usuario = $this->table($this->tableName, ['id' => 'pk_id_usuario']);
       $usuario->addColumn('nome', MysqlAdapter::PHINX_TYPE_STRING, ['limit' => 255, 'null' => false])
               ->addColumn('login', MysqlAdapter::PHINX_TYPE_STRING, ['limit' => 50, 'null' => false])
               ->addColumn('email', MysqlAdapter::PHINX_TYPE_STRING, ['limit' => 255, 'null' => false])
               ->addColumn('senha_de_acesso', MysqlAdapter::PHINX_TYPE_STRING, ['limit' => 128, 'null' => false])
               ->addColumn('esta_ativo', MysqlAdapter::PHINX_TYPE_BOOLEAN, ['default'=> 1, 'null' => false])
               ->addColumn('cadastrado_em', MysqlAdapter::PHINX_TYPE_TIMESTAMP, ['default' => 'CURRENT_TIMESTAMP', 'null' => false])
               ->addColumn('alterado_em', MysqlAdapter::PHINX_TYPE_TIMESTAMP, ['default' => null, 'null' => true])
               ->addIndex(['login'], ['unique' => true])
               ->addIndex(['email'], ['unique' => true])
               ->save();        
    }

    public function down() 
    {
        $this->dropTable($this->tableName);
    }
}