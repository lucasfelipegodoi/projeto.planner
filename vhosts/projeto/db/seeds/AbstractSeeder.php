<?php

use Phinx\Seed\AbstractSeed;

/**
 * Description of AbstractSeed
 *
 */
class AbstractSeeder extends AbstractSeed
{
    /**
     *
     * @var type 
     */
    protected $tableName = null;
    
    /**
     *
     * @var type 
     */
    private $environment = 'development';  

    /**
     * Initialize method.
     *
     * @return void
     */
    public function run()
    {
        $this->defineEnvironment();
        
        if($this->hasRows() == true) {
            exit;
        }
    }    
    
    /**
     * 
     * @return boolean
     */
    public function hasRows()
    {
        if ($this->tableName === null) {
            return false;
        }
        
        $sql = 'SELECT 1 AS total FROM ' . $this->tableName;
        
        return $this->fetchRow($sql) == true;
    }
    
    /**
     * 
     * @param type $seed
     * @param type $environment
     */
    final protected function runSeed($seed)
    {
        $path    = realpath(__DIR__ . '/../../');
        $command = $path . '/vendor/bin/phinx seed:run -e ' . $this->environment . ' -s ' . $seed;
        shell_exec($command);
    }
    
    /**
     * 
     * @name defineEnvironment
     * @return void
     */
    final protected function defineEnvironment()
    {
        $this->environment = $this->adapter->getOption('environment');
    }     
}
