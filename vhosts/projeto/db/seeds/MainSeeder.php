<?php

class MainSeeder extends AbstractSeeder
{
    /**
     * 
     * @name run
     * @access public
     * @return void
     */
    public function run()
    {
        parent::run();
		
		$this->runSeed('NivelDeAcessoSeeder');
        $this->runSeed('UsuarioSeeder');
    }
}
