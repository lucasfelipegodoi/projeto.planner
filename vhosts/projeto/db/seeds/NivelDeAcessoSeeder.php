<?php

class NivelDeAcessoSeeder extends AbstractSeeder
{
    /**
     *
     * @var string 
     */
    protected $tableName = 'tb_niveis_de_acesso';
            
    /**
     * 
     * @name run
     * @access public
     * @return void
     */
    public function run()
    {
        parent::run();
        
        $dados = [
            [
                'pk_id_nivel_de_acesso' => 1,
                'nivel_de_acesso' => 'Administrador',
            ],
        ];
        
        $this->table($this->tableName)->insert($dados)->save();
    }
}
