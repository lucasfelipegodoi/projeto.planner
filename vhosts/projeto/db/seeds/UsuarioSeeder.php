<?php

class UsuarioSeeder extends AbstractSeeder
{
    /**
     *
     * @var string
     */
    protected $tableName = 'tb_usuarios';
    
    /**
     * 
     * @name run
     * @access public
     * @return void
     */      
    public function run()
    {
        parent::run();

        $this->executeBefore();

        $dados = [
            'pk_id_usuario'         => 1,
            'fk_id_nivel_de_acesso' => 1,
            'nome'                  => 'Admin',
            'esta_ativo'            =>  1,      
            'login'                 => 'admin',
            'email'                 => 'email@teste.com',
            'senha_de_acesso'       => hash('sha512', '3Tyu12'),
        ];

        $this->table($this->tableName)->insert($dados)->save();
    }

    /**
     * 
     * @name executeBefore
     * @access private
     * @return void
     */    
    private function executeBefore()
    {
        $this->runSeed('NivelDeAcessoSeeder');
    }
}
