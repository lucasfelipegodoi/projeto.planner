<?php
/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */

/**
 * Ambientes
 *
 * production  Produção
 * staging     Homologaçao
 * development Desenvolvimento
 * 
 */
chdir(dirname(__DIR__));
define('DOCUMENT_ROOT', $_SERVER['DOCUMENT_ROOT']);
date_default_timezone_set('America/Sao_Paulo');
setlocale(LC_MONETARY, 'pt_BR');

if (file_exists(__DIR__ . '/../' . '/config/development.config.php')) {
    define('APPLICATION_ENV', 'development');
    error_reporting(E_ALL);
    ini_set('display_startup_errors', true);
    ini_set('display_errors', true);
    ini_set('track_errors', true);
} else {
    define('APPLICATION_ENV', 'development');
    error_reporting(E_ALL);
    ini_set('display_startup_errors', true);
    ini_set('display_errors', true);
    ini_set('track_errors', true);
}

// Setup autoloading
include 'init_autoloader.php';

if (! defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(__DIR__ . '/../'));
}

$appConfig = include APPLICATION_PATH . '/config/application.config.php';

if (file_exists(APPLICATION_PATH . '/config/development.config.php')) {
    $appConfig = Zend\Stdlib\ArrayUtils::merge($appConfig, include APPLICATION_PATH . '/config/development.config.php');
}

if ($_SERVER['HTTP_HOST'] === 'localhost:8000') {
    $appConfig = Zend\Stdlib\ArrayUtils::merge($appConfig, include APPLICATION_PATH . '/config/test.config.php');
}

// Run the application!
Zend\Mvc\Application::init($appConfig)->run();

