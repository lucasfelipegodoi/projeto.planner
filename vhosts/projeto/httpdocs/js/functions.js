$(document).ready(function () {
    runInit();
});

function runInit()
{
    //Incluir aqui as rotinas globais da aplicação
    $(document).ajaxStart(function() { Pace.restart(); }); 
    $("#myModal").modal('hide');
  
    $(".select2").select2({
        placeholder: 'Escolha uma opção',
        allowClear: false
    });
    
    $(".select2_allow_clear").select2({
        placeholder: 'Escolha uma opção',
        allowClear: true
    });
    $('.TextAreaContent').wysihtml5();

    $(".real").mask('0.000.000.000,00', {reverse: true});
    $('.cep').mask('00000-000', {reverse: true});
    $(".data").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        yearRange: '1990:2100',
        dayNames: ['Domingo','Segunda','Terça','Quarta','Quinta','Sexta','Sábado'],
        dayNamesMin: ['D','S','T','Q','Q','S','S','D'],
        dayNamesShort: ['Dom','Seg','Ter','Qua','Qui','Sex','Sáb','Dom'],
        monthNames: ['Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro'],
        monthNamesShort: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez'],
        nextText: 'Próximo',
        prevText: 'Anterior'
    });
    $(".data").inputmask("dd/mm/yyyy", {"placeholder": ""});
    $('.time').inputmask("hh:mm:ss", { insertMode: false, showMaskOnHover: false});
}


/**
 * @name  renderViewOnModal
 * @param url
 */
function renderViewOnModal(url) {
    $("#myModal").empty();
    $("#myModal").load(url);
}

/**
 *
 * @name  confirmModal
 * @param message
 * @param url
 */
function confirmModal(message, url) {
    bootbox.confirm({
        title: 'Atenção!',
        message: message,
        buttons: {
            'cancel': {
                label: 'Cancelar',
                className: 'btn-primary pull-left'
            },
            'confirm': {
                label: 'Confirmar',
                className: 'btn-danger pull-right'
            }
        },
        callback: function (confirm) {

            if (confirm) {
                location.href = url;
            }
        }
    });
}

/**
 *
 * @name  resetForm
 * @param form
 */
function resetForm(form) 
{   
    var elements = form.elements;
    
    for(i = 0; i < elements.length; i++) {
    
        field_type = elements[i].type.toLowerCase();

        switch(field_type) {

            case "text": 
            case "password": 
            case "textarea":
            case "hidden":	

                elements[i].value = ""; 
                break;

            case "radio":
            case "checkbox":
                
                if (elements[i].checked) {
                    elements[i].checked = false; 
                }
                
                break;

            case "select-one":
            case "select-multi":
                
                elements[i].selectedIndex = -1;
                break;

            default: 
                break;
        }
    }
}


/**
 *
 * @name  goTo
 * @param url
 */
function goTo(url) {
    location.href = url;
}