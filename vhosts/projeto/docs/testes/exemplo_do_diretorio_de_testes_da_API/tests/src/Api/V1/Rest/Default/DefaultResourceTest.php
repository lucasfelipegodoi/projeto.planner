<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace Api\V1\Rest;

use Core\Test\ApiTestCase;
use Skel\DataSourceTest;
use Skel\Model\DefaultModel;

/**
 * @group   ApiResource
 *
 */
class DefaultResourceTest extends ApiTestCase
{
    /**
     * Endereço do serviço da API
     * Por exemplo: /api/v1/municipio
     * @var string
     */
    protected $uri = '';
    
    /**
     * Rotinas que serão executadas antes do início do teste
     *
     * @name   setup
     * @access public
     * @return void
     */
    public function setup()
    {
        $this->createDataBase           = true;
        $this->createDataBaseUsingPhinx = true;
        parent::setup();
        $this->executeDependentRoutines();
    }
    
    /**
     * 
     *
     * @name   testValidGet
     * @access public
     * @return void
     */    
    public function testValidGet()
    {
        $this->executePost();

        $get = $this->getApi([
            'uri'  => $this->uri . '/1',
        ]);
        
        $this->assertEquals(200, $get['statusCode']);
    }

    /**
     * 
     *
     * @name   testValidPost
     * @access public
     * @return void
     */    
    public function testValidPost()
    {
        $post = $this->executePost();
        
        $this->assertEquals(201, $post['statusCode']);
    }
    
    /**
     * 
     *
     * @name   testValidPut
     * @access public
     * @return void
     */    
    public function testValidPut()
    {
        $this->executePost();
        
        $put = $this->putApi([
            'uri'  => $this->uri . '/1',
            'json' => [
                
            ],
        ]);
        
        $this->assertEquals(200, $put['statusCode']);
    }

    /**
     * 
     *
     * @name   testValidDelete
     * @access public
     * @return void
     */    
    public function testValidDelete()
    {
        $this->executePost();
        
        $delete = $this->deleteApi([
            'uri'  => $this->uri . '/1',
        ]);
        
        $this->assertEquals(204, $delete['statusCode']);
        $this->assertNull($delete['contentResponse']);
    }

    /**
     * 
     *
     * @name   executePost
     * @access private
     * @return array
     */    
    private function executePost()
    {
        $itens = DataSourceTest::getValidData(['model' => DefaultModel::class]);
        
        return $this->postApi([
            'uri'  => $this->uri,
            'json' => $itens[0],
        ]);
    }
    
    /**
     * 
     * 
     * @name executeDependentRoutines
     * @return void
     */
    private function executeDependentRoutines()
    {
        //Coloque aqui as rotinas que deverão ser executadas antes dos testes iniciarem
        //Por exemplo: $this->migrationService->runSeed('NiveisDeAcessoSeeder', self::DEFAULT_ENVIRONMENT);
    }
}
