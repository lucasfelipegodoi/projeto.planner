<?php
return array(
    'ignoreThoseModules' => array(
        'ZF\Apigility',
        'ZF\Apigility\Provider',
        'ZF\Apigility\Documentation',
        'AssetManager',
        'ZF\ApiProblem',
        'ZF\Configuration',
        'ZF\MvcAuth',
        'ZF\OAuth2',
        'ZF\Hal',
        'ZF\ContentNegotiation',
        'ZF\ContentValidation',
        'ZF\Rest',
        'ZF\Rpc',
        'ZF\Versioning',
        'ZF\DevelopmentMode',
        'TwbBundle',
        'AssetsBundle',
        'Neilime\MobileDetect',        
        'AcMailer',
    ),
    'mockShouldBeUsedForModel' => true,
    'environment_config' => array(
        'execute_real_email_sending' => false,
    ),        
    'db' => array(
        'driver'         => 'PDO_MYSQL',
        'database'       => '',
        'hostname'       => 'localhost',
        'port'           => 8889,
        'username'       => 'jenkins.ci',
        'password'       => '',
        'driver_options' => array(
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::ATTR_ERRMODE            =>PDO::ERRMODE_EXCEPTION,
        ),
    ),
    'db_information_schema' => array(
        'driver'   => 'PDO_MYSQL',
        'database' => 'information_schema',
        'hostname' => 'localhost',
        'port'     => 8889,
        'username' => 'jenkins.ci',
        'password' => '',
        'driver_options' => array(
            1002 => 'SET NAMES \'UTF8\'',
            3    => 2,
        ),
        'adapters' => array(),
    ),  
    'acl' => array(
        'roles' => array(
            'admin' => NULL,
        ),
        'resources' => array(
            'Admin\Controller\Autenticacao.sair-do-sistema',
            'Admin\Controller\Autenticacao.index', 
            'Admin\Controller\Index.index', 
            'Admin\Controller\Autenticacao.forbidden',
            'Admin\Controller\Usuario.index',
            'Admin\Controller\Usuario.save',
            'Admin\Controller\Usuario.view',
            'Admin\Controller\Usuario.delete',
       ),
        'privilege' => array(
            'admin' => array(
                'allow' => array(
                    'Admin\Controller\Autenticacao.sair-do-sistema',
                    'Admin\Controller\Autenticacao.index',
                    'Admin\Controller\Index.index',
                    'Admin\Controller\Usuario.index',
                    'Admin\Controller\Usuario.save',
                    'Admin\Controller\Usuario.view',
                    'Admin\Controller\Usuario.delete',
                ),
                'deny' => array(
                ),
            ),
	),  
    ),
    'module_listener_options' => [
        'config_glob_paths' => [
            'config/{,*.}{test.config}.php',
        ],
    ],    
);
