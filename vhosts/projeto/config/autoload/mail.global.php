<?php
/**
 *
 */
return [
    'acmailer_options' => [
        'default' => [
            'message_options' => [
                'from' => 'planner.sac@gmail.com',//Informar o e-mail do remetente
                'body' => [
                    'charset' => 'utf-8',
                ],
            ],
            'smtp_options' => [
                'host' => 'smtp.gmail.com',//Informar o servidor SMTP
                'port' => 587,
                'connection_class' => 'login',
                'connection_config' => [
                    'username' => 'planner.sac@gmail.com',//Informar o login do servidor SMTP
                    'password' => 'planner123',//Informar a senha do servidor SMTP
                    'ssl' => 'tls',
                ],
            ],
            'file_options' => [],
            'mail_adapter' => 'Zend\\Mail\\Transport\\Smtp',
        ],
    ],
];