<?php
return array(
    'environment_config' => array(
        'execute_real_email_sending' => true,
    ),
    'service_manager' => array(
        'factories' => array(
            'Zend\\Db\\Adapter\\Adapter' => 'Zend\\Db\\Adapter\\AdapterServiceFactory',
        ),
    ),
    'db' => array(
        'driver'   => 'PDO_MYSQL',
        'database' => '',
        'hostname' => 'localhost',
        'port'     => 3306,
        'username' => 'root',
        'password' => '',
        'driver_options' => array(
            1002 => 'SET NAMES \'UTF8\'',
            3    => 2,
        ),
        'adapters' => array(),
    ),
    'db_information_schema' => array(
        'driver'   => 'PDO_MYSQL',
        'database' => 'information_schema',
        'hostname' => 'localhost',
        'port'     => 3306,
        'username' => 'root',
        'password' => '',
        'driver_options' => array(
            1002 => 'SET NAMES \'UTF8\'',
            3    => 2,
        ),
        'adapters' => array(),
    ),    
    'acl' => array(
        'roles' => array(
            'admin' => NULL,
            'user' => NULL,
        ),
        'resources' => array(
            'Plan\Controller\Default.index',
            'Plan\Controller\Default.save',
            'Plan\Controller\Default.view',
            'Plan\Controller\Default.compartilhar',
            'Plan\Controller\Pin.save-nota',
            'Plan\Controller\Pin.save-lista',
            'Plan\Controller\Pin.update-item-lista',
            'Plan\Controller\Pin.delete-pin',
            'Plan\Controller\Default.delete-plan-user'
        ),
        'privilege' => array(
            'admin' => array(
                'allow' => array(
                    'Plan\Controller\Default.index',
                    'Plan\Controller\Default.save',
                    'Plan\Controller\Default.view',
                    'Plan\Controller\Default.get-all-usuarios',
                    'Plan\Controller\Default.delete-plan',
                    'Plan\Controller\Pin.save-nota',
                    'Plan\Controller\Pin.save-lista',
                    'Plan\Controller\Pin.update-item-lista',
                    'Plan\Controller\Pin.delete-pin',
                    'Plan\Controller\Default.compartilhar',
                    'Plan\Controller\Default.delete-plan-user'
                ),
                'deny' => array(
                    
                ),
            ),
            'user' => array(
                'allow' => array(
                ),
                'deny' => array(
                ),
            ),

        ),
    ),    
);
