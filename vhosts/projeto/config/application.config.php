<?php

use Zend\Authentication\Adapter\DbTable as AuthenticationAdapterDbTable;
use Zend\Session\Container as SessionContainer;
use Zend\View\Model\ViewModel;
use Zend\Authentication\AuthenticationService as ZendAuthenticationService;
use Zend\Permissions\Acl\Acl;
use Core\Service\AutenticacaoService;

return [
    'modules' => [
        'ZF\DevelopmentMode',
        'ZF\Apigility',
        'ZF\Apigility\Provider',
        'ZF\Apigility\Documentation',
        'AssetManager',
        'ZF\ApiProblem',
        'ZF\Configuration',
        'ZF\MvcAuth',
        'ZF\OAuth2',
        'ZF\Hal',
        'ZF\ContentNegotiation',
        'ZF\ContentValidation',
        'ZF\Rest',
        'ZF\Rpc',
        'ZF\Versioning',
        //'TwbBundle',
        //'AssetsBundle',
        'Neilime\MobileDetect',
        'AcMailer',
        'Core',
        'Admin',
        'User',
        'Plan',
        
    ],
    'service_manager' => [
        'factories' => [
            'translator' => 'Zend\I18n\Translator\TranslatorServiceFactory',
            'Zend\Session\Container' => function($serviceManager) {

                return new SessionContainer(AutenticacaoService::AUTHENTICATION_SESSION);
            },
            'Zend\View\Model\ViewModel' => function ($serviceManager) {

                return new ViewModel();
            },
            'Zend\Authentication\Adapter\DbTable' => function ($serviceManager) {

                return new AuthenticationAdapterDbTable(
                        $serviceManager->get('DbAdapter')
                );
            },
            'Zend\Authentication\AuthenticationService' => function($serviceManager) {
                return new ZendAuthenticationService();
            },
            'Zend\Permissions\Acl\Acl' => function($serviceManager) {
                return new Acl();
            },      
        ],
    ],
    'module_listener_options' => [
        'config_glob_paths' => [
            'config/autoload/{,*.}{global,local}.php',
        ],
        'module_paths' => [
            './module',
            './vendor',
        ],
    ],
];
